/* eslint-disable @typescript-eslint/no-var-requires */
// production config
const { resolve } = require('path')
const merge = require('webpack-merge')
const Dotenv = require('dotenv-webpack')

console.log('dotenv', Dotenv)

const commonConfig = require('./common')

module.exports = merge(commonConfig, {
    mode: 'production',
    entry: './index.tsx',
    output: {
        filename: 'js/bundle.[hash].min.js',
        path: resolve(__dirname, '../../dist'),
        publicPath: '/'
    },
    devtool: 'source-map',
    plugins: [new Dotenv()]
})
