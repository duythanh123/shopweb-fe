/* eslint-disable @typescript-eslint/no-var-requires */
const express = require('express')
const app = express()
const path = require('path')
const http = require('http')
const favicon = require('serve-favicon')

app.use(express.static(path.join(__dirname, 'dist')))
app.use(favicon(path.join(__dirname, 'dist', 'favicon.ico')))
app.get('/*', function(req, res) {
    res.sendFile(path.join(__dirname + '/dist/index.html'))
})

const httpServer = http.createServer(app)
const PORT = process.env.PORT || 3000
httpServer.listen(PORT)
console.log('> Ready on http://localhost:' + PORT)
