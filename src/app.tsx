import React from 'react'
import { Layout } from 'antd'
import { Language } from './models/language'
import language from './i18n/lang-en.json'
import Routing from './routing'
import AppHeader from './components/layout/header'
import AppFooter from './components/layout/footer'
import 'antd/lib/row/style/css'
import 'antd/lib/col/style/css'
import '../src/styles/main.scss'
import '../src/styles/responsive.scss'
import '../src/styles/button.scss'
import '../src/styles/manager.scss'
import '../src/styles/public.scss'

import './app.css'
import SiderDom from './components/manager/sider'
require('./static/favicon.ico')

const { Content } = Layout
const lang: Language = language

const App = () => {
    if (location.pathname.includes('/login') || location.pathname.includes('/group/image')) {
        return (
            <Content>
                <Routing lang={lang} />
            </Content>
        )
    } else if (location.pathname.includes('/public')) {
        return (
            <Content>
                <Routing lang={lang} />
            </Content>
        )
    } else {
        return (
            <div>
                <Layout>
                    <AppHeader lang={lang} />
                    <Layout>
                        <SiderDom lang={lang}></SiderDom>
                        <Layout>
                            <Content className='layout'>
                                <Routing lang={lang} />
                            </Content>
                        </Layout>
                    </Layout>
                    <AppFooter lang={lang} />
                </Layout>
            </div>
        )
    }
}

export default App
