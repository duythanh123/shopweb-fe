import React from 'react'
import { Modal, Table } from 'antd'
import { Language } from '../../models/language'
import { WareHouseInOutModel } from '../../models/warehouseInOut.model'
import { formatter } from '../../common/common'

export interface SeeProps {
    lang?: Language
    visible?: boolean
    warehouse?: WareHouseInOutModel
    CloseModal?: Function
}
const columns = [
    {
        title: 'Tên NVL',
        dataIndex: 'name',
        key: 'name'
    },
    {
        title: 'Mã NVL',
        dataIndex: 'code',
        key: 'code'
    },
    {
        title: 'Mô tả NVL',
        dataIndex: 'description',
        key: 'description'
    },
    {
        title: 'Số lượng',
        dataIndex: 'amount',
        key: 'amount'
    },
    {
        title: 'Giá tiền',
        dataIndex: 'price',
        key: 'price',
        render: (_: string, record: WareHouseInOutModel): JSX.Element => {
            return <div>{formatter.format(record.price)}</div>
        }
    }
]

const See = (props: SeeProps) => {
    return (
        <div>
            <Modal
                visible={props.visible}
                title='Chi tiết'
                destroyOnClose={true}
                onCancel={() => props.CloseModal()}
                footer={null}>
                <Table
                    columns={columns}
                    rowKey={'id'}
                    dataSource={props.warehouse?.items}
                    pagination={false}
                    size='small'
                />
            </Modal>
        </div>
    )
}
export default See
