import React, { useRef, useState, useEffect, useContext } from 'react'
import { Card, Form, Input, Select, Button, Modal, InputNumber, Row, Col } from 'antd'
import { Language } from '../../models/language'
import { WareHouseModel } from '../../models/warehouse.model'
import { showNotification, formatter } from '../../common/common'
import warehouseService from '../../services/warehouse-service'
import { WareHouseInOutModel } from '../../models/warehouseInOut.model'
import warehouseInOutService from '../../services/warehouseInOut-service'
import { TreeSelect, Table } from 'antd'

import { TreeNode } from 'antd/lib/tree-select'

const layout = {
    labelCol: {
        span: 8
    },
    wrapperCol: {
        span: 16
    }
}
const validateMessages = {
    required: '${label} không được trống!',
    types: {
        email: '${label} không phải là địa chỉ email!'
    }
}
export interface AddProps {
    lang?: Language
    visible?: boolean
    closeModal?: any
    onCreate?: Function
    editable?: boolean
}

interface WarehouseListSelect {
    id?: string
    name?: string
    code?: string
    description?: string
    price?: number
    amount?: number
}
interface AddState {
    warehouseList: WareHouseModel[]
    warehouseListSelect: WarehouseListSelect[]
    warehouseInOutList: WareHouseInOutModel[]
    value: string[]
    loading: boolean
}
const InWareHouse = (props: AddProps) => {
    const [form] = Form.useForm()
    const { lang } = props
    const initalState: AddState = {
        loading: true,
        value: [],
        warehouseList: [],
        warehouseListSelect: [],
        warehouseInOutList: []
    }

    const [state, setState] = useState(initalState)
    const fetchData = async () => {
        try {
            const result = await warehouseService.getWareHouseList()
            setState(s => ({ ...s, warehouseList: result?.result, loading: false }))
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
        try {
            const result = await warehouseInOutService.getWareHouseInOutList()
            setState(s => ({
                ...s,
                warehouseInOutList: result?.result,
                loading: false,
                value: [],
                warehouseListSelect: []
            }))
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    useEffect(() => {
        if (props) fetchData()
    }, [props])

    const onChange = value => {
        setState(s => ({ ...s, value }))
    }
    const addItemsToTable = () => {
        const { warehouseList, value } = state
        const warehouseListSelectCopy = []
        value?.map(itemId => {
            warehouseList.map(item => {
                if (item.id === itemId)
                    warehouseListSelectCopy.push({
                        id: item.id,
                        name: item.name,
                        code: item.code,
                        description: item.description,
                        price: item.price,
                        amount: 1
                    })
            })
        })
        setState(s => ({ ...s, warehouseListSelect: warehouseListSelectCopy }))
    }
    function onChangenumber(e, record: WarehouseListSelect) {
        let { warehouseListSelect } = state
        const n: number = warehouseListSelect.indexOf(record)
        record.amount = e
        warehouseListSelect.splice(n, 1, record)
        warehouseListSelect = warehouseListSelect.slice()
        setState(s => ({ ...s, warehouseListSelect }))
    }

    const columns = [
        {
            title: 'Tên NVL',
            dataIndex: 'name',
            key: 'name'
        },
        {
            title: 'Mã NVL',
            dataIndex: 'code',
            key: 'code'
        },
        {
            title: 'Mô tả NVL',
            dataIndex: 'description',
            key: 'description'
        },
        {
            title: 'Số lượng',
            dataIndex: 'amount',
            key: 'amount',
            render: (_: string, record: WarehouseListSelect): JSX.Element => {
                return (
                    <InputNumber
                        size='large'
                        min={1}
                        max={100000}
                        defaultValue={record.amount}
                        onChange={e => onChangenumber(e, record)}
                    />
                )
            }
        },
        {
            title: 'Giá tiền',
            dataIndex: 'price',
            key: 'price',
            render: (_: string, record: WareHouseInOutModel): JSX.Element => {
                return <div>{formatter.format(record.price)}</div>
            }
        }
    ]

    return (
        <Modal
            visible={props.visible}
            title='Phiếu nhập kho'
            destroyOnClose={true}
            okText='Tạo'
            cancelText='Hủy'
            onCancel={props.closeModal}
            onOk={() => {
                form.validateFields()
                    .then(values => {
                        form.resetFields()
                        let priced = 0
                        state.warehouseListSelect.map(item => {
                            priced += item.amount * item.price
                        })
                        props.onCreate(values, priced, state.warehouseListSelect)
                    })
                    .catch(info => {
                        console.log('Validate Failed:', info)
                    })
            }}>
            <Form
                {...layout}
                form={form}
                name='nest-add'
                validateMessages={validateMessages}
                initialValues={{ role: 2 }}>
                <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                    <Col className='search-row' xs={24} sm={12} md={12} lg={12}>
                        <Form.Item
                            label='Loại'
                            name='type'
                            rules={[
                                {
                                    required: true
                                }
                            ]}>
                            <Select placeholder='Nhập/Xuất'>
                                <Select.Option value={1} key={1}>
                                    Nhập
                                </Select.Option>
                                <Select.Option value={2} key={2}>
                                    Xuất
                                </Select.Option>
                            </Select>
                        </Form.Item>
                    </Col>
                    <Col className='search-row' xs={24} sm={12} md={12} lg={12}>
                        <Form.Item
                            label='NCC'
                            name='supplier'
                            rules={[
                                {
                                    required: true
                                }
                            ]}>
                            <Select placeholder='Nhà cung cấp'>
                                {state.warehouseInOutList.map(item => (
                                    <Select.Option value={item.supplier} key={item.id}>
                                        {item.supplier}
                                    </Select.Option>
                                ))}
                            </Select>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                    <Col className='search-row' xs={24} sm={12} md={12} lg={12}>
                        <Form.Item
                            label='Mã phiếu'
                            name='code'
                            rules={[
                                {
                                    required: true
                                }
                            ]}>
                            <Input placeholder='Điền mã phiếu' />
                        </Form.Item>
                    </Col>
                    <Col className='search-row' xs={24} sm={12} md={12} lg={12}>
                        <Form.Item name='description' label='Ghi chú'>
                            <Input />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                    <Col className='search-row' xs={30} sm={16} md={16} lg={16}>
                        <Form.Item name='MH' label='Mặt hàng'>
                            <TreeSelect
                                className='col-6'
                                showSearch
                                style={{ width: 250 }}
                                value={state.value}
                                dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                                placeholder='Vui lòng chọn'
                                allowClear
                                treeDefaultExpandAll
                                treeCheckable
                                onChange={onChange}>
                                {state.warehouseList?.map((item, key) => (
                                    <TreeNode value={item.id} key={item.id} title={item.name} />
                                ))}
                            </TreeSelect>
                        </Form.Item>
                    </Col>
                    <Col className='search-row' xs={16} sm={8} md={8} lg={8}>
                        <Button className='col-6' style={{ marginLeft: 50 }} type='primary' onClick={addItemsToTable}>
                            Thêm
                        </Button>
                    </Col>
                </Row>
            </Form>
            <Card>
                <Table
                    columns={columns}
                    dataSource={state.warehouseListSelect}
                    rowKey='code'
                    pagination={false}
                    size='small'
                />
            </Card>
        </Modal>
    )
}

export default InWareHouse
