import React, { Fragment, useState, useEffect } from 'react'
import { Layout, Card, Button, Table, Row, Col, Modal, DatePicker, Breadcrumb, Input } from 'antd'
import '../../styles/persons.scss'
import { Language } from '../../models/language'
import { WareHouseInOutModel } from '../../models/warehouseInOut.model'
import warehouseInOutService from '../../services/warehouseInOut-service'
import { showNotification, formatMoment, formatter } from '../../common/common'
import SEE from './see'
import In from './in'
import HeaderDom from '../../components/manager/header'
import { LoadingPage } from '../../jsx/loading'
import { VerticalAlignMiddleOutlined } from '@ant-design/icons'
import { formatDate } from '../../common/date-util'
import { pagination } from '../../common/constant'
import BreadcrumbItem from 'antd/lib/breadcrumb/BreadcrumbItem'
const { RangePicker } = DatePicker

interface WareHouseInOutProps {
    lang: Language
}

interface WareHouseInOutState {
    warehouseList: WareHouseInOutModel[]
    loading: boolean
    warehouse: WareHouseInOutModel
    modalShowing?: string
    checkUpdate?: boolean
    disabled?: boolean
    loadTable?: boolean
    current: number
    pageSize: number
    total: number
    dateSearch: number[]
}

const WareHouseInOutPage = (props: WareHouseInOutProps) => {
    const { lang } = props
    const { Content } = Layout
    const initalState: WareHouseInOutState = {
        loading: true,
        dateSearch: [],
        warehouseList: [],
        modalShowing: '',
        checkUpdate: false,
        disabled: true,
        warehouse: null,
        loadTable: true,
        current: pagination.current,
        pageSize: pagination.pageSize,
        total: pagination.total
    }

    const [state, setState] = useState(initalState)
    const fetchData = async () => {
        console.log(state.dateSearch)
        if (state.dateSearch?.length === 0) {
            try {
                const result = await warehouseInOutService.getWareHouseInOutList(state.current - 1, state.pageSize)
                setState(s => ({
                    ...s,
                    warehouseList: result.result,
                    loadTable: false,
                    loading: false,
                    total: result?.count
                }))
            } catch (error) {
                showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
            }
        } else {
            try {
                const result = await warehouseInOutService.getWarehouseListByDate(
                    state.dateSearch,
                    state.current - 1,
                    state.pageSize
                )
                console.log('dateSearch==>', state.dateSearch)
                console.log('result==>', result)
                setState(s => ({
                    ...s,
                    warehouseList: result.result,
                    loadTable: false,
                    loading: false,
                    total: result?.count
                }))
            } catch (error) {
                console.log('nn', state.dateSearch)
            }
        }
    }
    useEffect(() => {
        if (state.loadTable) {
            fetchData()
        }
    }, [state])

    const handleChange = async pagination => {
        setState(s => ({
            ...s,
            current: pagination.current,
            pageSize: pagination.pageSize,
            total: pagination.total,
            loadTable: true
        }))
    }

    const closeModal = () => {
        setState(s => ({ ...s, modalShowing: '', disabled: true, checkUpdate: false }))
    }
    const showModal = (type: string, warehouse?: WareHouseInOutModel) => {
        setState(s => ({ ...s, modalShowing: type, warehouse: warehouse }))
    }

    const deleteWareHouse = async (warehouse: WareHouseInOutModel) => {
        try {
            const result = await warehouseInOutService.deleteWareHouse(warehouse.id)
            console.log(result)
            if (result.status == 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.delete)
                setState(s => ({ ...s, modalShowing: '', loadTable: true }))
            } else {
                showNotification(lang.notification.error, result.data?.message)
            }
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.delete)
        }
    }
    const onCreate = async (values, priced, warehouseListSelect) => {
        const body = {
            items: warehouseListSelect,
            code: values.code,
            description: values.description,
            type: values.type,
            price: priced,
            supplier: values.supplier
        }
        try {
            const result = await warehouseInOutService.createWareHouseInOut(body)
            if (result.status == 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.create)
                setState(s => ({ ...s, modalShowing: '', loadTable: true }))
            } else {
                showNotification(lang.notification.error, result.data?.message)
            }
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.create)
        }
    }
    const onChange = value => {
        const dateSearch: number[] = []
        value?.map(date => {
            dateSearch.push(formatMoment(date))
        })
        setState(s => ({ ...s, dateSearch: dateSearch }))
    }
    const onClickShow = async () => {
        setState(s => ({ ...s, loadTable: true, current: pagination.current, pageSize: pagination.pageSize }))
    }
    const columns = [
        {
            title: 'Ngày tạo',
            dataIndex: 'dateCreate',
            key: 'dateCreate',
            render: (_: string, record: WareHouseInOutModel): JSX.Element => {
                return <div>{formatDate(record.createDate)}</div>
            }
        },
        {
            title: 'Loại',
            dataIndex: 'type',
            key: 'type',
            render: (_: string, record: WareHouseInOutModel): JSX.Element => {
                if (record.type === 1) return <div>Nhập</div>
                else if (record.type === 2) return <div>Xuất</div>
            }
        },
        {
            title: 'Mã Phiếu',
            dataIndex: 'code',
            key: 'code'
        },
        {
            title: 'Mô tả NVL',
            dataIndex: 'description',
            key: 'description'
        },
        {
            title: 'Giá tiền',
            dataIndex: 'price',
            key: 'price',
            render: (_: string, record: WareHouseInOutModel): JSX.Element => {
                return <div>{formatter.format(record.price)}</div>
            }
        },
        {
            title: 'Nhà cung cấp',
            dataIndex: 'supplier',
            key: 'supplier'
        },

        {
            title: 'Chi tiết',
            dataIndex: 'detal',
            key: 'detil',
            render: (_: string, record: WareHouseInOutModel): JSX.Element => {
                return (
                    <div>
                        <Button type='primary' onClick={() => showModal('edit-warehouse', record)}>
                            Chi tiết
                        </Button>
                    </div>
                )
            }
        },
        {
            title: 'Xóa',
            dataIndex: 'delete',
            key: 'delete',
            render: (_: string, record: WareHouseInOutModel): JSX.Element => {
                return (
                    <Button type='primary' onClick={() => showModal('delete-warehouse', record)}>
                        Xóa
                    </Button>
                )
            }
        }
    ]
    return (
        <Fragment>
            <HeaderDom itemMenu={6} />
            <Content
                className='site-layout-background'
                style={{
                    padding: 24,
                    margin: 0,
                    minHeight: 280
                }}>
                <div>
                    {state.loading ? (
                        <LoadingPage />
                    ) : (
                        <Fragment>
                            <Breadcrumb>
                                <BreadcrumbItem>Quản lý kho</BreadcrumbItem>
                                <BreadcrumbItem>Nhập Xuất kho</BreadcrumbItem>
                            </Breadcrumb>
                            <br />
                            <Row gutter={{ xs: 4, sm: 8, md: 12, lg: 16 }}>
                                <Col className='gutter-row' xs={24} sm={12} md={12} lg={12}>
                                    <Button
                                        icon={<VerticalAlignMiddleOutlined />}
                                        type='primary'
                                        onClick={() => showModal('in-warehouse')}>
                                        Nhập xuất kho
                                    </Button>
                                </Col>
                                <Col className='gutter-row' xs={24} sm={12} md={12} lg={12}>
                                    <div className='input-search'>
                                        <Input.Group compact>
                                            <RangePicker onChange={onChange} />
                                            <Button type='primary' onClick={() => onClickShow()}>
                                                Xem
                                            </Button>
                                        </Input.Group>
                                    </div>
                                </Col>
                            </Row>
                            <div>
                                <Card
                                    title='Danh sách kho'
                                    headStyle={{ background: '#70bf70', fontSize: 20, fontWeight: 'bold' }}>
                                    <Table
                                        columns={columns}
                                        loading={state.loadTable}
                                        rowKey='id'
                                        dataSource={state.warehouseList}
                                        pagination={{
                                            pageSize: state.pageSize,
                                            current: state.current,
                                            total: state.total
                                        }}
                                        onChange={handleChange}
                                        size='small'
                                    />
                                </Card>
                            </div>

                            <SEE
                                lang={props.lang}
                                visible={state.modalShowing === 'edit-warehouse'}
                                warehouse={state.warehouse}
                                CloseModal={closeModal}
                            />

                            <In
                                lang={props.lang}
                                visible={state.modalShowing === 'in-warehouse'}
                                closeModal={closeModal}
                                onCreate={onCreate}
                            />
                            <Modal
                                title='Xóa kho'
                                visible={state.modalShowing === 'delete-warehouse'}
                                destroyOnClose={true}
                                onCancel={() => closeModal()}
                                onOk={() => deleteWareHouse(state.warehouse)}
                                okText='Xóa'
                                cancelText='Hủy'>
                                Xóa phiếu xuất/nhập kho này?
                            </Modal>
                        </Fragment>
                    )}
                </div>
            </Content>
        </Fragment>
    )
}

export default WareHouseInOutPage
