import React, { Component, Fragment, useState, useEffect } from 'react'
import { Layout, Space, Card, Form, Input, DatePicker, Select, Switch, Row, Col, Button } from 'antd'
import { Language } from '../../models/language'

import { showNotification, getChangeItems, UserInf, formatMoment } from '../../common/common'
import moment from 'moment'
import userAction from '../../redux/actions/user'
import { useDispatch } from 'react-redux'
import { CategoryModel } from '../../models/category.model'
import categoryService from '../../services/category-service '

const { Header, Content } = Layout
const layout = {
    labelCol: {
        span: 8
    },
    wrapperCol: {
        span: 16
    }
}
const validateMessages = {
    required: '${label} không được trống!'
}
export interface CatProps {
    lang?: Language
    cat?: CategoryModel
    checkUpdate?: boolean
    closeModal?: Function
    ableButton?: Function
    updateCat?: Function
}
interface CatState {
    loading: boolean
    newCat: CategoryModel
}
const EditCat = (props: CatProps) => {
    const [form] = Form.useForm()
    const { lang } = props
    const initalState: CatState = {
        loading: true,
        newCat: null
    }
    const dispatch = useDispatch()
    const [state, setState] = useState(initalState)

    useEffect(() => {
        setState(s => ({ ...s, newCat: Object.assign({}, props.cat), loading: false }))
    }, [])

    useEffect(() => {
        if (props.checkUpdate) {
            updateCat()
        }
    }, [props.checkUpdate])

    const changeItem = (value: string | number, key: string) => {
        const { newCat } = state
        newCat[key] = value
        props.ableButton()
        setState(s => ({ ...s, newCat: Object.assign({}, newCat) }))
    }

    const updateCat = async () => {
        const { newCat } = state
        const param: CategoryModel = getChangeItems(props.cat, newCat)
        try {
            const response = await categoryService.editCat(props.cat?.id, param)
            if (response.status == 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.edit)
                props.closeModal()
                props.updateCat(response.data)
                dispatch({ type: userAction.updateUser, payload: response.data })
            } else {
                showNotification(lang.notification.error, response.data?.message)
            }
        } catch (error) {
            showNotification(lang.notification.error, lang.message_error.update_fail)
        }
    }

    const { newCat } = state
    const initForm = {
        name: props.cat.name,
        status: props.cat.status
    }
    return (
        <Fragment>
            <Form {...layout} name='nest-messages' form={form} onFinish={updateCat} initialValues={initForm}>
                <Form.Item
                    name='name'
                    label='Tên danh mục'
                    rules={[
                        {
                            required: true
                        }
                    ]}>
                    <Input style={{ width: 250 }} onChange={e => changeItem(e.target.value, 'name')} />
                </Form.Item>

                <Form.Item name='status' label='Mô tả'>
                    <Input style={{ width: 250 }} onChange={e => changeItem(e.target.value, 'status')} />
                </Form.Item>
            </Form>
        </Fragment>
    )
}

export default EditCat
