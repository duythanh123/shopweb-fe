import React, { Fragment, useState, useEffect } from 'react'
import { Layout, Card, Button, Table, Row, Col, Input, Modal, Breadcrumb } from 'antd'
import '../../styles/persons.scss'
import { Language } from '../../models/language'
import { showNotification, UserInf } from '../../common/common'
import Edit from './edit'
import Add from './add'
import HeaderDom from '../../components/manager/header'
import { LoadingPage } from '../../jsx/loading'
import { getCookie } from '../../common/cookie'
import { CategoryModel } from '../../models/category.model'
import categoryService from '../../services/category-service '
import BreadcrumbItem from 'antd/lib/breadcrumb/BreadcrumbItem'

interface CatProps {
    lang: Language
}
interface CatState {
    catList: CategoryModel[]
    loading: boolean
    cat: CategoryModel
    modalShowing?: string
    checkUpdate?: boolean
    disabled?: boolean
    loadTable: boolean
}

const CatPage = (props: CatProps) => {
    const { lang } = props
    const { Header, Content } = Layout
    const initalState: CatState = {
        loading: true,
        catList: [],
        modalShowing: '',
        checkUpdate: false,
        disabled: true,
        cat: null,
        loadTable: true
    }

    const [state, setState] = useState(initalState)
    const fetchData = async () => {
        let catList: CategoryModel[] = []
        try {
            catList = await categoryService.getCategoryList()
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
        setState(s => ({ ...s, catList, loading: false, loadTable: false }))
    }
    useEffect(() => {
        if (state.loadTable) {
            fetchData()
        }
    }, [state])
    const closeModal = () => {
        setState(s => ({ ...s, modalShowing: '', disabled: true, checkUpdate: false }))
    }
    const showModal = (type: string, cat?: CategoryModel) => {
        setState(s => ({ ...s, modalShowing: type, cat: cat }))
    }
    const isUpdate = () => {
        setState(s => ({ ...s, checkUpdate: true }))
    }
    const updateCat = (cat: CategoryModel) => {
        setState(s => ({ ...s, loadTable: true }))
    }
    const ableButton = () => {
        setState(s => ({ ...s, disabled: false }))
    }

    const deleteCat = async (cat: CategoryModel) => {
        try {
            const result = await categoryService.deleteCat(cat.id)
            if (result.status == 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.delete)

                setState(s => ({ ...s, modalShowing: '', loadTable: true }))
            } else {
                showNotification(lang.notification.error, result.data?.message)
            }
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    const onCreate = async (values: CategoryModel) => {
        try {
            const result = await categoryService.createCat(values)
            if (result.status == 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.create)
                setState(s => ({ ...s, loadTable: true, modalShowing: '' }))
            } else {
                showNotification(lang.notification.error, result.data?.message)
            }
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    const columns = [
        {
            title: 'Tên danh mục',
            dataIndex: 'name',
            key: 'name'
        },
        {
            title: 'Mô tả',
            dataIndex: 'status',
            key: 'status'
        },

        {
            title: 'Cập nhật',
            dataIndex: 'update',
            key: 'update',
            render: (_: string, record: CategoryModel): JSX.Element => {
                return (
                    <div>
                        <Button type='primary' onClick={() => showModal('edit-cat', record)}>
                            Cập nhật
                        </Button>
                    </div>
                )
            }
        },
        {
            title: 'Xóa',
            dataIndex: 'delete',
            key: 'delete',
            render: (_: string, record: CategoryModel): JSX.Element => {
                if (record.id === getCookie(UserInf).id)
                    return (
                        <Button type='primary' disabled>
                            Xóa
                        </Button>
                    )
                else
                    return (
                        <Button type='primary' onClick={() => showModal('delete-cat', record)}>
                            Xóa
                        </Button>
                    )
            }
        }
    ]
    return (
        <Fragment>
            <HeaderDom itemMenu={7} />

            <Content
                className='site-layout-background'
                style={{
                    padding: 24,
                    margin: 0,
                    minHeight: 280
                }}>
                <div>
                    {state.loading ? (
                        <LoadingPage />
                    ) : (
                        <Fragment>
                            <div>
                                <Breadcrumb>
                                    <BreadcrumbItem>Quản lý danh mục</BreadcrumbItem>
                                    <BreadcrumbItem>Nhóm món</BreadcrumbItem>
                                </Breadcrumb>
                                <Row gutter={{ xs: 4, sm: 8, md: 12, lg: 16 }}>
                                    <Col
                                        className='gutter-row'
                                        xs={24}
                                        sm={24}
                                        md={24}
                                        lg={24}
                                        style={{ textAlign: 'right' }}>
                                        <Button type='primary' onClick={() => showModal('add-cat')}>
                                            Thêm danh mục
                                        </Button>
                                    </Col>
                                </Row>
                            </div>
                            <div>
                                <Card
                                    title='Danh sách danh mục'
                                    headStyle={{ background: '#70bf70', fontSize: 20, fontWeight: 'bold' }}>
                                    <Table
                                        columns={columns}
                                        rowKey='id'
                                        dataSource={state.catList}
                                        loading={state.loadTable}
                                        size='small'
                                    />
                                </Card>
                            </div>
                            <Modal
                                title='Cập nhật thông tin danh mục'
                                visible={state.modalShowing === 'edit-cat'}
                                destroyOnClose={true}
                                onCancel={() => closeModal()}
                                onOk={() => isUpdate()}
                                okText='Cập nhật'
                                cancelText='Hủy'
                                okButtonProps={{ disabled: state.disabled }}>
                                <Edit
                                    cat={state.cat}
                                    lang={props.lang}
                                    checkUpdate={state.checkUpdate}
                                    closeModal={closeModal}
                                    ableButton={ableButton}
                                    updateCat={updateCat}
                                />
                            </Modal>
                            <Add
                                lang={props.lang}
                                visible={state.modalShowing === 'add-cat'}
                                closeModal={closeModal}
                                onCreate={onCreate}
                            />
                            <Modal
                                title='Xóa nhân viên'
                                visible={state.modalShowing === 'delete-cat'}
                                destroyOnClose={true}
                                onCancel={() => closeModal()}
                                onOk={() => deleteCat(state.cat)}
                                okText='Xóa'
                                cancelText='Hủy'>
                                Xác nhận xóa danh mục này?
                            </Modal>
                        </Fragment>
                    )}
                </div>
            </Content>
        </Fragment>
    )
}

export default CatPage
