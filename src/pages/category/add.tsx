import React, { Component, Fragment, useState, useEffect } from 'react'
import { Layout, Space, Card, Form, Input, DatePicker, Select, Switch, Row, Col, Button, Modal } from 'antd'
import { Link } from 'react-router-dom'
import { Language } from '../../models/language'

const layout = {
    labelCol: {
        span: 8
    },
    wrapperCol: {
        span: 16
    }
}
const validateMessages = {
    required: '${label} không được trống!'
}
export interface AddProps {
    lang?: Language
    visible?: boolean
    closeModal?: any
    onCreate?: Function
}

const AddCat = (props: AddProps) => {
    const [form] = Form.useForm()
    const { lang } = props

    return (
        <Modal
            visible={props.visible}
            title='Thêm danh mục'
            destroyOnClose={true}
            okText='Thêm'
            cancelText='Hủy'
            onCancel={props.closeModal}
            onOk={() => {
                form.validateFields()
                    .then(values => {
                        form.resetFields()
                        props.onCreate(values)
                    })
                    .catch(info => {
                        console.log('Validate Failed:', info)
                    })
            }}>
            <Form
                {...layout}
                form={form}
                name='nest-add'
                validateMessages={validateMessages}
                initialValues={{ role: 2 }}>
                <Form.Item
                    name='name'
                    label='Danh mục'
                    rules={[
                        {
                            required: true
                        }
                    ]}>
                    <Input style={{ width: 250 }} />
                </Form.Item>

                <Form.Item name='status' label='Mô tả'>
                    <Input style={{ width: 250 }} />
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default AddCat
