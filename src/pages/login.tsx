import React, { useState } from 'react'
import { Form, Input, Button, Row, Col, Layout } from 'antd'
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import { setCookie } from '../common/cookie'
import { Token, ExpireKey, ExpireTime, UserKey, UserInf } from '../common/common'
import loginService from '../services/login-service'

const LoginPage = () => {
    const [form] = Form.useForm()
    const { Content } = Layout
    const [state, setState] = useState({
        error: null,
        page: ''
    })

    const onChange = () => {
        setState(s => ({ ...s, error: null }))
    }
    const log = (st: string) => {
        setState(s => ({ ...s, page: st }))
    }
    const login = async () => {
        try {
            const res = await loginService.login(form.getFieldsValue(true))
            console.log('res===>', res)
            if (res.status === 200) {
                setCookie(UserKey, form.getFieldValue('userName'))
                setCookie(UserInf, {
                    id: res.data.id,
                    name: res.data.name,
                    avatar: res.data.avatar,
                    role: res.data.role
                })
                setCookie(Token, res.data.token)
                const date = new Date().setHours(new Date().getHours() + ExpireTime)
                setCookie(ExpireKey, date.valueOf())
                const url = state.page
                location.href = url
            } else {
                setState(s => ({ ...s, error: 'Incorrect userName or password' }))
            }
        } catch (error) {
            setState(s => ({ ...s, error: 'Incorrect userName or password' }))
        }
    }

    return (
        <Layout>
            <Content className='abc'>
                <div className='login'>
                    <Form form={form} onFinish={login} className='login-form'>
                        <Form.Item>
                            <img src='http://res.cloudinary.com/ds5zmvcyj/image/upload/v1593272980/fstuo35zhq4j5jl7sbxj.png' />
                        </Form.Item>
                        <Form.Item
                            name='userName'
                            rules={[{ required: true, message: 'Please input your userName!' }, { max: 16 }]}>
                            <Input
                                prefix={<UserOutlined />}
                                placeholder='Input user name'
                                onKeyPress={e => {
                                    if (!/^[0-9a-zA-Z]/.test(e.key)) event.preventDefault()
                                }}
                                onChange={onChange}
                                maxLength={16}
                            />
                        </Form.Item>
                        <Form.Item
                            name='password'
                            rules={[{ required: true, message: 'Please input your password!' }, { max: 30 }]}>
                            <Input.Password
                                prefix={<LockOutlined />}
                                placeholder='Input password'
                                onChange={onChange}
                                maxLength={30}
                            />
                        </Form.Item>
                        <Form.Item>
                            <Row gutter={32}>
                                <Col span={12}>
                                    <Button
                                        type='primary'
                                        style={{ width: '100%' }}
                                        className='login-form-button'
                                        htmlType='submit'
                                        onClick={() => log('/')}>
                                        Admin
                                    </Button>
                                </Col>
                                <Col span={12}>
                                    <Button
                                        type='primary'
                                        style={{ width: '100%' }}
                                        className='login-form-button'
                                        htmlType='submit'
                                        onClick={() => log('/public')}>
                                        Sale
                                    </Button>
                                </Col>
                            </Row>
                        </Form.Item>
                    </Form>
                </div>
            </Content>
        </Layout>
    )
}

export default LoginPage
