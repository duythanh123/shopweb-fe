import React, { useState, useEffect } from 'react'
import { Form, Table, Input, Popconfirm, Button, Select } from 'antd'
import moment from 'moment'
import { SearchOutlined, EditOutlined, DeleteOutlined, QuestionCircleOutlined } from '@ant-design/icons'
import { ButtonMain, ButtonCancel, ButtonSave, ButtonUpdate } from '../jsx/button'
import { LoadingPage } from '../jsx/loading'
import { showNotification, getChangeItems, formatMoment, objectToArray } from '../common/common'
import { formatDate, compareDate } from '../common/date-util'
import constant from '../components/common/constant'
import { FormItemProps, renderFormField } from '../components/form/form-common'
import userService from '../services/user-service'
import ListPageHeader from '../components/shared/list-page-header'
import { Language } from '../models/language'
import { UserModel } from '../models/user.model'

interface UserState {
    loading: boolean
    userList?: UserModel[]
    editUser?: UserModel
    actionStatus?: string
    currentPage?: number
    filteredInfo?: any
    sortedInfo?: any
}
const { Option } = Select

const UserPage = (props: { lang?: Language }) => {
    const [form] = Form.useForm()
    const { lang } = props
    const initalState: UserState = {
        loading: true,
        userList: [],
        editUser: null,
        actionStatus: '',
        currentPage: 1,
        filteredInfo: null,
        sortedInfo: null
    }

    const [state, setState] = useState(initalState)
    let userList = []
    const fetchData = async () => {
        try {
            userList = await userService.getUserList()
        } catch (error) {
            showNotification(lang.notification.error, lang.item.data, lang.action.load)
        }
        setState(s => ({ ...s, userList, loading: false }))
    }

    useEffect(() => {
        if (state.loading) {
            fetchData()
        }
    }, [])

    // -> Code as Ant Design
    const getColumnSearchProps = (props: { lang: Language }, columnName: string) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div className='search-input'>
                <Input
                    ref={node => {
                        this.searchInput = node
                    }}
                    placeholder={`Search ${columnName}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm)}
                />
                <Button
                    type='primary'
                    onClick={() => handleSearch(selectedKeys, confirm)}
                    icon={<SearchOutlined />}
                    size='small'
                    style={{ width: 90, marginRight: 8 }}>
                    {lang.button.search}
                </Button>
                <Button onClick={() => handleReset(clearFilters)} size='small' style={{ width: 90 }}>
                    {lang.button.reset}
                </Button>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) =>
            record[columnName] &&
            record[columnName]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select())
            }
        }
    })
    const handleSearch = (selectedKeys: any, confirm: any) => {
        confirm()
        setState(s => ({ ...s }))
    }
    const handleChange = (pagination: any, filters: any, sorter: any) => {
        setState(s => ({ ...s, filteredInfo: filters, sortedInfo: sorter }))
    }
    const handleReset = clearFilters => {
        clearFilters()
        setState(s => ({ ...s }))
    }
    const clearAll = () => {
        setState(s => ({ ...s, filteredInfo: null, sortedInfo: null }))
    }
    // -> end code as Ant Design

    const formFields: Array<FormItemProps> = [
        {
            item: 'name',
            tag: 'input',
            rules: [{ required: true, max: 30 }],
            onChange: e => changeItem(e.target.value, 'name')
        },
        {
            item: 'email',
            tag: 'input',
            rules: [{ required: true, max: 30 }, { type: 'email' }],
            onChange: e => changeItem(e.target.value, 'email')
        },
        {
            item: 'birthday',
            tag: 'datepicker',
            rules: [{ required: true, max: 30 }, { type: 'email' }],
            onChange: date => changeItem(formatMoment(date), 'birthday')
        },
        {
            item: 'tel',
            tag: 'inputNumber',
            rules: [{ max: 15 }],
            maxLength: 15,
            onChange: e => changeItem(e.target.value, 'tel')
        }
    ]

    const columns = [
        {
            title: lang.symbol.no,
            dataIndex: '',
            key: '',
            render: (record: any) => state.userList.findIndex(u => u.id === record.id) + 1
        },
        {
            title: lang.user.user_name,
            dataIndex: 'userName',
            key: 'userName',
            filteredValue: state.filteredInfo?.userName || null,
            ...getColumnSearchProps({ lang }, 'userName'),
            render: (userName: string, record: any) =>
                record.id !== '-1' ? (
                    userName
                ) : (
                    <Form.Item
                        name='userName'
                        rules={[
                            { required: true },
                            () => ({
                                async validator(rule, value) {
                                    const result = await userService.checkUserName(value)
                                    if (result.status === 200) {
                                        return Promise.resolve()
                                    }
                                    return Promise.reject('UserName exists')
                                }
                            })
                        ]}>
                        <Input
                            placeholder={`Input ${lang.user.user_name}`}
                            defaultValue={state.editUser?.userName}
                            onKeyPress={e => {
                                if (e.key === 'Enter') event.preventDefault()
                            }}
                            onChange={e => changeItem(e.target.value, 'userName')}
                            maxLength={16}></Input>
                    </Form.Item>
                )
        },
        {
            title: lang.user.name,
            dataIndex: 'name',
            key: 'name',
            filteredValue: state.filteredInfo?.name || null,
            ...getColumnSearchProps({ lang }, 'name'),
            render: (name: string, record: any) =>
                state.editUser === null || state.editUser?.id !== record.id
                    ? name
                    : renderFormField(Object.assign(formFields[0], { initialValue: state.editUser?.name }))
        },
        {
            title: lang.user.email,
            dataIndex: 'email',
            key: 'email',
            filteredValue: state.filteredInfo?.email || null,
            ...getColumnSearchProps({ lang }, 'email'),
            render: (email: string, record: any) =>
                state.editUser === null || state.editUser?.id !== record.id
                    ? email
                    : renderFormField(Object.assign(formFields[1], { initialValue: state.editUser?.email }))
        },
        {
            title: lang.user.birthday,
            dataIndex: 'birthday',
            key: 'birthday',
            className: 'box-birthday',
            sorter: (a, b) => compareDate(a.birthday, b.birthday),
            sortOrder: state.sortedInfo?.columnKey === 'birthday' && state.sortedInfo?.order,
            render: (birthday: string, record: any) =>
                state.editUser === null || state.editUser?.id !== record.id
                    ? formatDate(birthday)
                    : renderFormField(
                          Object.assign(formFields[2], {
                              initialValue: state.editUser?.birthday ? moment(state.editUser.birthday) : null
                          })
                      )
        },
        {
            title: lang.user.tel,
            dataIndex: 'tel',
            key: 'tel',
            filteredValue: state.filteredInfo?.tel || null,
            ...getColumnSearchProps({ lang }, 'tel'),
            render: (tel: string, record: any) =>
                state.editUser === null || state.editUser?.id !== record.id
                    ? tel
                    : renderFormField(Object.assign(formFields[3], { initialValue: state.editUser?.tel }))
        },
        {
            title: lang.user.role,
            dataIndex: 'role',
            key: 'role',
            filters: objectToArray(constant.userRole),
            filteredValue: state.filteredInfo?.role || null,
            onFilter: (value, record) => record.role == value,
            render: (role: number, record: any) =>
                state.editUser === null || state.editUser?.id !== record.id ? (
                    constant.userRole[role]
                ) : (
                    <Form.Item name='role' rules={[{ required: true }]}>
                        <Select
                            placeholder={`Select ${lang.user.role}`}
                            labelInValue
                            onChange={e => changeItem(e['value'], 'role')}>
                            <Option value={2}>User</Option>
                            <Option value={1}>Admin</Option>
                        </Select>
                    </Form.Item>
                )
        },
        {
            title: lang.user.create_date,
            dataIndex: 'createDate',
            key: 'createDate',
            sorter: (a, b) => a.createDate - b.createDate,
            sortOrder: state.sortedInfo?.columnKey === 'createDate' && state.sortedInfo?.order,
            render: (createDate: string) => formatDate(createDate)
        },
        {
            className: 'width-sm',
            render: (record: any) =>
                state.editUser !== null ? (
                    ''
                ) : (
                    <Popconfirm
                        placement='leftBottom'
                        title={props?.lang.notification.success}
                        icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                        onConfirm={() => deleteUser(record.id)}>
                        <DeleteOutlined />
                    </Popconfirm>
                )
        },
        {
            className: 'width-sm',
            render: (record: any) => {
                switch (state.actionStatus) {
                    case 'editing':
                        return (
                            record.id === state.editUser?.id && (
                                <div id='action-edit'>
                                    <ButtonUpdate
                                        disabled={JSON.stringify(state.editUser) === JSON.stringify(record)}
                                    />
                                    <ButtonCancel onClick={handleCancel} />
                                </div>
                            )
                        )
                    case 'adding':
                        return (
                            record.id === '-1' && (
                                <div id='action-edit'>
                                    <ButtonSave disabled={JSON.stringify(state.editUser) === JSON.stringify(record)} />
                                    <ButtonCancel onClick={handleCancel} />
                                </div>
                            )
                        )
                    default:
                        return <EditOutlined onClick={() => showEdit(record.id)} />
                }
            }
        }
    ]

    function changeItem(value: any, key: string) {
        const { editUser } = state
        editUser[key] = value
        setState(s => ({ ...s, editUser: Object.assign({}, editUser) }))
    }

    const showEdit = (id: string) => {
        const { userList } = state
        const editUser = userList.find(u => u.id === id)
        form.setFieldsValue(
            Object.assign({}, editUser, {
                role: {
                    key: editUser?.role,
                    label: constant.userRole[editUser?.role]
                }
            })
        )
        setState(s => ({ ...s, actionStatus: 'editing', editUser: Object.assign({}, editUser) }))
    }

    const handleCancel = () => {
        const { userList } = state
        if (state.actionStatus === 'adding') userList.shift()
        form.resetFields()
        setState(s => ({ ...s, userList: [...userList], editUser: null, actionStatus: '' }))
    }

    const showAdd = async () => {
        clearAll()

        const { userList } = state
        setState(s => ({
            ...s,
            userList: [{ id: '-1' }, ...userList],
            editUser: { id: '-1' },
            actionStatus: 'adding',
            currentPage: 1
        }))
    }
    const changePage = (page: number) => {
        if (state.actionStatus === '') setState(s => ({ ...s, currentPage: page }))
    }

    const onFinish = () => {
        const { actionStatus, editUser } = state
        if (editUser.tel === '') editUser.tel = null
        if (!moment(editUser.birthday).isValid()) editUser.birthday = null
        if (actionStatus === 'adding') addUser()
        if (actionStatus === 'editing') updateUser()
    }

    const addUser = async () => {
        const { userList, editUser } = state
        const param = Object.assign({}, editUser)
        delete param.id

        try {
            const response = await userService.createUser(param)
            if (response.status == 200) {
                userList.splice(0, 1, response.data)
                showNotification(lang.notification.success, '', lang.item.user, lang.action.add)
            } else {
                showNotification(lang.notification.error, response.data?.message)
                userList.shift()
            }
        } catch (error) {
            showNotification(lang.notification.error, lang.message_error.add_fail)
            userList.shift()
        } finally {
            form.resetFields()
            setState(s => ({ ...s, userList: [...userList], editUser: null, actionStatus: '' }))
        }
    }

    const updateUser = async () => {
        const { userList, editUser } = state
        const index = userList.findIndex(u => u.id === editUser.id)
        const param = getChangeItems(userList[index], editUser)

        try {
            const response = await userService.editUser(editUser.id, param)
            if (response.status == 200) {
                userList.splice(index, 1, response.data)
                showNotification(lang.notification.success, '', lang.item.user, lang.action.edit)
            } else {
                showNotification(lang.notification.error, response.data?.message)
            }
        } catch (error) {
            showNotification(lang.notification.error, lang.message_error.update_fail)
        } finally {
            form.resetFields()
            setState(s => ({ ...s, userList: [...userList], editUser: null, actionStatus: '' }))
        }
    }

    const deleteUser = async (id: string) => {
        let { userList } = state
        try {
            const response = await userService.deleteUser(id)
            if (response.status == 200) {
                userList = userList.filter(x => x.id !== id)
                showNotification(lang.notification.success, '', lang.item.user, lang.action.delete)
            } else {
                showNotification(lang.notification.error, response.data?.message)
            }
        } catch (error) {
            showNotification(lang.notification.error, lang.message_error.delete_fail)
        } finally {
            setState(s => ({ ...s, userList: [...userList], editUser: null, actionStatus: '' }))
        }
    }

    return (
        <div className='container-fluid user-list'>
            {state.actionStatus === '' && <ButtonMain onClick={showAdd} text={lang.button.add} />}
            <ListPageHeader titlePage={lang.user.title_list} />
            {state.loading ? (
                <LoadingPage />
            ) : (
                <Form form={form} onFinish={onFinish} className='table-user'>
                    <Table
                        columns={columns}
                        dataSource={state.userList}
                        pagination={{ current: state.currentPage, onChange: changePage }}
                        onChange={handleChange}
                    />
                </Form>
            )}
        </div>
    )
}

export default UserPage
