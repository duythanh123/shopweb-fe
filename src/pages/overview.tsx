import React, { Fragment, useState, useEffect } from 'react'
import { Line } from '@ant-design/charts'
import HeaderDom from '../components/manager/header'
import { Layout, Breadcrumb, Row, Col, DatePicker, Button, Card } from 'antd'
import BreadcrumbItem from 'antd/lib/breadcrumb/BreadcrumbItem'
import { LoadingPage } from '../jsx/loading'
import { showNotification, formatMoment, isArrayEmpty } from '../common/common'
import billService from '../services/bill-service'
import { Language } from '../models/language'
import { formatDate } from '../common/date-util'

interface OverviewProps {
    lang?: Language
}
const Overview = (props: OverviewProps) => {
    const { Content } = Layout
    const now = new Date()
    const tenDayAgo = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 10)

    const [state, setState] = useState({
        loading: true,
        data: [],
        dateSearch: []
    })
    const fetchData = async () => {
        try {
            const data = []
            const dateSearch: number[] = [formatMoment(tenDayAgo), formatMoment(now)]
            const list = await billService.getBillListByDate(dateSearch)
            if (!isArrayEmpty(list?.result)) {
                list.result?.map(bill => {
                    const search = data.findIndex(d => d?.date == formatDate(bill?.createDate))
                    if (search === -1) {
                        data.push({ date: formatDate(bill?.createDate), price: bill?.totalPrice })
                    } else {
                        data[search] = {
                            date: formatDate(bill?.createDate),
                            price: data[search]?.price + bill?.totalPrice
                        }
                    }
                })
            }
            setState(s => ({ ...s, data: data, loading: false }))
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    useEffect(() => {
        fetchData()
    }, [])

    const onChange = value => {
        const dateSearch: number[] = []
        value?.map(date => {
            dateSearch.push(formatMoment(date))
        })
        setState(s => ({ ...s, dateSearch: dateSearch }))
    }

    const showBill = async () => {
        const { dateSearch } = state
        if (!isArrayEmpty(dateSearch)) {
            try {
                const data = []
                const list = await billService.getBillListByDate(dateSearch)
                if (!isArrayEmpty(list?.result)) {
                    list.result?.map(bill => {
                        const search = data.findIndex(d => d?.date == formatDate(bill?.createDate))
                        if (search === -1) {
                            data.push({ date: formatDate(bill?.createDate), price: bill?.totalPrice })
                        } else {
                            data[search] = {
                                date: formatDate(bill?.createDate),
                                price: data[search]?.price + bill?.totalPrice
                            }
                        }
                    })
                }
                setState(s => ({ ...s, data: data, loading: false }))
            } catch (error) {
                showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
            }
        }
    }

    const { data } = state
    const config = {
        data,
        title: {
            visible: false,
            text: 'Biểu Đồ Doanh Thu'
        },
        xField: 'date',
        yField: 'price',
        point: {
            visible: true,
            size: 4,
            shape: 'diamond',
            style: {
                fill: 'white',
                stroke: '#2593fc',
                lineWidth: 2
            }
        }
    }
    const { RangePicker } = DatePicker
    return (
        <Fragment>
            <HeaderDom itemMenu={9} />
            <Content
                className='site-layout-background'
                style={{
                    padding: 24,
                    margin: 0,
                    minHeight: 280
                }}>
                <div>
                    {state.loading ? (
                        <LoadingPage />
                    ) : (
                        <Fragment>
                            <Breadcrumb>
                                <BreadcrumbItem>Tổng quan</BreadcrumbItem>
                            </Breadcrumb>
                            <Row gutter={{ xs: 4, sm: 8, md: 12, lg: 16 }}>
                                <Col
                                    className='gutter-row'
                                    xs={24}
                                    sm={24}
                                    md={24}
                                    lg={24}
                                    style={{ textAlign: 'right' }}>
                                    <RangePicker onChange={onChange} />
                                    <Button type='primary' onClick={showBill}>
                                        Xem
                                    </Button>
                                </Col>
                            </Row>
                            <div>
                                <Card
                                    title='Doanh thu'
                                    headStyle={{ background: '#70bf70', fontSize: 20, fontWeight: 'bold' }}>
                                    <Line {...config} />
                                </Card>
                            </div>
                        </Fragment>
                    )}
                </div>
            </Content>
        </Fragment>
    )
}
export default Overview
