import { Card, Col, Row, Table, Button, Input, Avatar, Select } from 'antd'
import Meta from 'antd/lib/card/Meta'
import React, { useState, useEffect } from 'react'
import { TableModel } from '../../models/table.model'
import tableService from '../../services/table-service'
import { showNotification, formatter } from '../../common/common'
import { Language } from '../../models/language'
import productService from '../../services/product-service'
import { productModel } from '../../models/product.model'
import OrderRigth from '../../components/public/orderFooter'
import { cartModel } from '../../models/cart.model'
import CartService from '../../services/cart-service'
import { MinusOutlined, PlusOutlined, UserOutlined } from '@ant-design/icons'
import { billModel } from '../../models/bill.model'
import BillService from '../../services/bill-service'
import { CategoryModel } from '../../models/src_models_category.model'
import categoryService from '../../services/src_services_category-service '
import '../../styles/card.scss'

interface MenuProps {
    lang: Language
    table: TableModel
    checkTable: Function
}
interface cartT {
    productId: string
    amount: number
}
interface MenuState {
    menuList: productModel[]
    loading: boolean
    cartList: cartModel[]
    cart: cartModel
    bill: billModel
    listCategory: CategoryModel[]
    category: string
    count: number
    page: number
}
const menu = (props: MenuProps) => {
    const initalState: MenuState = {
        menuList: [],
        loading: true,
        cartList: [],
        cart: null,
        bill: null,
        listCategory: [],
        category: '',
        count: 0,
        page: 0
    }
    const [state, setState] = useState(initalState)
    const fetchData = async () => {
        let menuList, listCategory, cartList
        try {
            const result = await tableService.getTableDetail(props.table.id)
            if (result.carts) cartList = result.carts
            else cartList = []
            listCategory = await categoryService.getCategoryList()
            if (state.category === '' || state.category === 'all') {
                menuList = await productService.getProductList()
            } else {
                menuList = await productService.getProductDetailWithArea(state.category, 0, 12)
            }
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
        setState(s => ({
            ...s,
            loading: false,
            cartList: cartList,
            listCategory: listCategory,
            menuList: menuList?.result,
            count: menuList?.count,
            page: 1
        }))
    }

    useEffect(() => {
        if (state.loading) {
            fetchData()
        }
    }, [state.category])
    const create = async () => {
        let cartL: cartT[] = []
        state.cartList.map(item => {
            let cartItem: cartT = {
                productId: item.product.id,
                amount: item.amount
            }
            cartL.push(cartItem)
        })
        try {
            const result = await CartService.createCart(props.table?.id, { carts: cartL })
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.create)
        }
        props.checkTable()
    }

    const columns = [
        {
            title: 'STT',
            dataIndex: 'STT',
            key: 'STT',
            width: '5%',
            render: (_: string, record: cartModel): JSX.Element => {
                return <div>{state.cartList?.indexOf(record) + 1}</div>
            }
        },
        {
            title: 'Món',
            dataIndex: 'name',
            key: 'name',
            width: '40%',
            render: (_: string, record: cartModel): JSX.Element => {
                return <div>{record.product?.name}</div>
            }
        },
        {
            title: 'Số lượng',
            dataIndex: 'amount',
            key: 'amount',
            width: '45%',
            render: (_: string, record: cartModel): JSX.Element => {
                return (
                    <div>
                        <Button
                            type='primary'
                            icon={<MinusOutlined />}
                            shape='circle'
                            size={'small'}
                            onClick={() => Minus(record)}
                        />

                        <Input
                            size={'small'}
                            value={record.amount}
                            style={{ width: 35, textAlign: 'center' }}
                            maxLength={3}
                            onKeyPress={e => {
                                if (!/^[0-9]/.test(e.key)) event.preventDefault()
                            }}
                            onChange={e => ChangeAmount(e, record)}
                        />
                        <Button
                            type='primary'
                            icon={<PlusOutlined />}
                            shape='circle'
                            size={'small'}
                            onClick={() => Plus(record)}
                        />
                    </div>
                )
            }
        },
        {
            title: 'Tiền',
            dataIndex: 'price',
            key: 'price',
            width: '10%',
            render: (_: string, record: cartModel): JSX.Element => {
                return <div>{formatter.format(record.product?.price)}</div>
            }
        }
    ]
    const CreateCard = (item: productModel) => {
        let { cartList } = state
        let check = true
        cartList?.map(e => {
            if (e.product?.id === item.id) {
                check = false
                e.amount++
                cartList = cartList.slice()
                setState(s => ({ ...s, cartList }))
            }
        })
        if (check) {
            const card: cartModel = {
                product: item,
                amount: 1
            }
            cartList?.push(card)
            cartList = cartList.slice()
            setState(s => ({ ...s, cartList }))
        }
    }
    const Minus = (cart: cartModel) => {
        let { cartList } = state
        if (cart.amount - 1 === 0) {
            cartList = cartList.filter(item => item.product.id !== cart.product.id)
            setState(s => ({ ...s, cartList }))
        } else {
            cartList[cartList.indexOf(cart)].amount--
            cartList = cartList.slice()
            setState(s => ({ ...s, cartList }))
        }
    }
    const Plus = (cart: cartModel) => {
        let { cartList } = state
        cartList[cartList.indexOf(cart)].amount++
        cartList = cartList.slice()
        setState(s => ({ ...s, cartList }))
    }
    const ChangeAmount = async (e, cart: cartModel) => {
        let { cartList } = state
        cartList[cartList.indexOf(cart)].amount =
            e.target.value === '0' || e.target.value === '' ? 1 : parseInt(e.target.value)
        cartList = cartList.slice()
        setState(s => ({ ...s, cartList }))
    }

    const createBill = async (total: number) => {
        const cartL: cartT[] = []
        state.cartList.map(item => {
            const cartItem: cartT = {
                productId: item.product.id,
                amount: item.amount
            }
            cartL.push(cartItem)
        })
        try {
            const result = await CartService.createCart(props.table?.id, { carts: cartL })
            if (result.status === 200) {
                const bill: billModel = {
                    totalPrice: total,
                    tableId: props.table.id
                }
                try {
                    await BillService.createBill(bill)
                } catch (error) {
                    showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.create)
                }
            }
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.create)
        }
        destroy()
    }

    const destroy = async () => {
        try {
            await CartService.deleteAllCart(props.table.id)
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.delete)
        }
        setState(s => ({ ...s, loading: true, cartList: [] }))
        props.checkTable()
    }
    const onChange = async value => {
        setState(s => ({ ...s, category: value, loading: true }))
    }

    const scrollToBottom = async () => {
        const { count, menuList, page, category } = state
        const elmnt = document.getElementById('scroll-show')
        let productList
        if (elmnt.scrollTop > 200 && count > menuList.length - 1) {
            try {
                if (category === '' || category === 'all') productList = await productService.getProductList(page, 12)
                else productList = await productService.getProductDetailWithArea(category, page, 12)
                setState(s => ({
                    ...s,
                    menuList: menuList.concat(productList?.result),
                    count: productList?.count,
                    page: page + 1
                }))
            } catch (error) {
                console.log('123132313hahah')
                // showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
            }
        }
    }

    return (
        <div>
            <Row justify='end' gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                <Col xs={12} sm={12} md={12} lg={12}>
                    <h4>Bàn {props.table.name}</h4>
                </Col>
                <Col xs={12} sm={12} md={12} lg={12}>
                    <h4>
                        Menu :{'   '}
                        <Select defaultValue='all' onChange={onChange} style={{ width: 150 }}>
                            <Select.Option value='all'>Tất cả</Select.Option>
                            {state.listCategory?.map(item => (
                                <Select.Option value={item.id} key={item.id}>
                                    {item.name}
                                </Select.Option>
                            ))}
                        </Select>
                    </h4>
                </Col>
            </Row>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                <Col
                    className='gutter-row'
                    id='scroll-show'
                    xs={24}
                    sm={24}
                    md={24}
                    lg={16}
                    onScroll={scrollToBottom}
                    style={{
                        border: '1px solid black',
                        height: '500px',
                        overflow: 'scroll'
                    }}>
                    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        {state.menuList?.map((item, index) => (
                            <Col className='gutter-row' xs={24} sm={12} md={8} lg={6} key={index}>
                                <Card
                                    hoverable
                                    bordered={false}
                                    style={{ textAlign: 'center', borderRadius: 15 }}
                                    onClick={() => CreateCard(item)}
                                    cover={
                                        <Avatar
                                            shape={'square'}
                                            style={{ width: '100%', height: 150, borderRadius: 15 }}
                                            icon={<UserOutlined />}
                                            src={item.picture}
                                        />
                                    }>
                                    <Meta title={item.name} />
                                </Card>
                            </Col>
                        ))}
                    </Row>
                </Col>
                <Col className='gutter-row' xs={24} sm={24} md={12} lg={8}>
                    <div className='bill-pay'>
                        <Card>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Col className='search-row' xs={24} sm={24} md={24} lg={24}>
                                    <div className='table-pay'>
                                        <div>
                                            <Table
                                                columns={columns}
                                                rowKey={'id'}
                                                dataSource={state.cartList}
                                                pagination={false}
                                                size='small'
                                            />
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                            <OrderRigth
                                cartList={state.cartList}
                                lang={props.lang}
                                createBill={createBill}
                                close={create}
                                destroy={destroy}
                            />
                        </Card>
                    </div>
                </Col>
            </Row>
        </div>
    )
}
export default menu
