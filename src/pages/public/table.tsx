import { Card, Col, Row } from 'antd'
import React, { useState, useEffect } from 'react'
import { TableModel } from '../../models/table.model'
import tableService from '../../services/table-service'
import { showNotification } from '../../common/common'
import { Language } from '../../models/language'
import MenuTable from './menu'
import '../../styles/card.scss'

interface TableProps {
    areaId?: string
    lang: Language
}
interface TableState {
    tableList: TableModel[]
    loading: boolean
    check: boolean
    table: TableModel
    backgroundColor: string
}
const table = (props: TableProps) => {
    const initalState: TableState = {
        tableList: [],
        loading: false,
        check: true,
        table: null,
        backgroundColor: 'green'
    }
    const [state, setState] = useState(initalState)
    const fetchData = async () => {
        try {
            const tableList = await tableService.getTableByIdArea(props?.areaId)
            setState(s => ({ ...s, tableList: tableList, loading: false, check: true }))
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    useEffect(() => {
        if (props.areaId) {
            fetchData()
        }
    }, [props])
    useEffect(() => {
        if (state.loading) {
            fetchData()
        }
    }, [state])
    const ShowMenu = (item?: TableModel) => {
        setState(s => ({ ...s, table: item, check: false }))
    }
    const CloseMenu = () => {
        setState(s => ({ ...s, loading: true, check: false }))
    }
    return (
        <div>
            <div className='area-table'>
                {state.check ? (
                    <Row gutter={{ xs: 4, sm: 8, md: 12, lg: 16 }}>
                        {state.tableList?.map((item, key) => {
                            return (
                                <Col className='gutter-row' xs={12} sm={6} md={6} lg={4} key={key}>
                                    <Card
                                        className='card'
                                        bodyStyle={{
                                            borderRadius: 15,
                                            backgroundColor: item.carts
                                                ? item.carts?.length !== 0
                                                    ? '#FF6666'
                                                    : '#66CC66'
                                                : '#66CC66'
                                        }}
                                        style={{
                                            borderRadius: '50%',
                                            textAlign: 'center',
                                            fontSize: 26
                                        }}
                                        hoverable
                                        onClick={() => ShowMenu(item)}>
                                        <b>{item.name}</b>
                                    </Card>
                                </Col>
                            )
                        })}
                    </Row>
                ) : (
                    <MenuTable lang={props.lang} table={state.table} checkTable={CloseMenu} />
                )}
            </div>
        </div>
    )
}
export default table
