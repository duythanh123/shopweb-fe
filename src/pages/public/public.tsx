import { Language } from '../../models/language'
import { Layout, Menu, Dropdown, PageHeader, Select } from 'antd'
import React, { useState, useEffect } from 'react'
import { UserOutlined, HomeOutlined, LogoutOutlined } from '@ant-design/icons'
import { removeCookie, getCookie } from '../../common/cookie'
import '../../styles/public.scss'
import { AreaModel } from '../../models/area.model'
import areaService from '../../services/area-service'
import { showNotification, UserKey, Token, ExpireKey, UserInf } from '../../common/common'
import TableShow from './table'
import { useSelector } from 'react-redux'
import { RootState } from '../../redux'

interface publicState {
    areaList: AreaModel[]
    collapsed: boolean
    loading: boolean
    idArea: string
}

const publicPage = (props: { lang: Language }) => {
    const { Content } = Layout
    const initalState: publicState = {
        areaList: [],
        collapsed: false,
        loading: true,
        idArea: null
    }
    const [state, setState] = useState(initalState)
    const fetchData = async () => {
        try {
            const areaList = await areaService.getAreaList()
            setState(s => ({ ...s, areaList: areaList, idArea: areaList[0]?.id, loading: false }))
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    useEffect(() => {
        if (state.loading) {
            fetchData()
        }
    }, [])
    const showTable = (index: number) => {
        setState(s => ({ ...s, idArea: state.areaList[index].id }))
    }

    const [user, setUser] = useState(getCookie(UserInf))
    const userReducer = useSelector<RootState, RootState.User>(state => state.user)

    useEffect(() => {
        if (userReducer.loading) {
            setUser(userReducer.user)
        }
    }, [userReducer])
    const onLogoutClick = () => {
        const pathname = window.location.pathname
        removeCookie(UserKey)
        removeCookie(Token)
        removeCookie(ExpireKey)
        removeCookie(UserInf)
        location.href = `/login?url=${pathname}`
    }
    const onClickManager = () => {
        location.href = `/`
    }
    const back = () => {
        setState(s => ({ ...s, idArea: state.idArea }))
    }
    const menu = (
        <Menu>
            <Menu.Item key='1' icon={<HomeOutlined />} onClickCapture={onClickManager}>
                Quản lý
            </Menu.Item>
            <Menu.Item key='3' icon={<LogoutOutlined />} onClickCapture={onLogoutClick}>
                Đăng xuất
            </Menu.Item>
        </Menu>
    )
    return (
        <Layout className='site-layout'>
            <PageHeader
                className='site-page-header'
                title='MiLLan Coffee'
                extra={[
                    <Select defaultValue={0} style={{ width: 200 }} onChange={showTable}>
                        {state.areaList?.map((item, index) => {
                            return (
                                <Select.Option key={index} value={index}>
                                    <h5>{item.name}</h5>
                                </Select.Option>
                            )
                        })}
                    </Select>,
                    <Dropdown.Button overlay={menu} icon={<UserOutlined />} style={{ width: 150 }}>
                        {user.name}
                    </Dropdown.Button>
                ]}
                onBack={back}></PageHeader>
            <Content
                className='site-layout-background'
                style={{
                    margin: '24px 16px',
                    padding: 24,
                    minHeight: 200
                }}>
                <TableShow areaId={state.idArea} lang={props.lang} />
            </Content>
        </Layout>
    )
}
export default publicPage
