import React, { Fragment, useState, useEffect } from 'react'
import { Row, Col, Breadcrumb, Layout, Modal, Avatar, Button } from 'antd'
import { LoadingPage } from '../../jsx/loading'
import { UserInf, showNotification } from '../../common/common'
import { getCookie } from '../../common/cookie'
import { formatTime } from '../../common/date-util'
import { Language } from '../../models/language'
import '../../styles/persons.scss'
import '../../styles/profile.scss'
import '../../styles/manager.scss'
import storeService from '../../services/store-service'
import { StoreModel } from '../../models/strore.models'
import StoreEditPage from './edit'
import HeaderDom from '../../components/manager/header'
import { UserOutlined } from '@ant-design/icons'
import { UserModel } from '../../models/user.model'
import userService from '../../services/user-service'

interface StoreState {
    loading: boolean
    storeDetail?: StoreModel
    modalShowing?: boolean
    checkUpdateStore?: boolean
    disabled?: boolean
    userDetail?: UserModel
}
const StorePage = (props: StoreProps) => {
    const { lang } = props
    const { Content } = Layout

    const initalState: StoreState = {
        loading: true,
        storeDetail: {},
        userDetail: {}
    }

    const [state, setState] = useState(initalState)

    const fetchData = async () => {
        let storeDetail = {}
        try {
            storeDetail = await storeService.getStoreDetail()
        } catch (error) {
            showNotification(lang.notification.error, lang.item.data, lang.action.load)
        }
        let userDetail = {}
        try {
            userDetail = await userService.getUserDetail(getCookie(UserInf).id)
        } catch (error) {
            showNotification(lang.notification.error, lang.item.data, lang.action.load)
        }
        setState(s => ({ ...s, storeDetail, userDetail, loading: false }))
    }

    useEffect(() => {
        if (state.loading) {
            fetchData()
        }
    }, [state])
    const closeModal = () => {
        setState(s => ({ ...s, modalShowing: false, disabled: true, checkUpdateStore: false }))
    }

    const showModal = () => {
        setState(s => ({ ...s, modalShowing: true }))
    }

    const isUpdate = () => {
        setState(s => ({ ...s, checkUpdateStore: true }))
    }

    const updateStore = (store: StoreModel) => {
        setState(s => ({ ...s, storeDetail: store }))
    }

    const ableButton = () => {
        setState(s => ({ ...s, disabled: false }))
    }
    return (
        <Fragment>
            <HeaderDom itemMenu={4} />
            <Content
                className='site-layout-background'
                style={{
                    padding: 24,
                    margin: 0,
                    minHeight: 280
                }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item>Quản lý cửa hàng</Breadcrumb.Item>
                    <Breadcrumb.Item>Thông tin cửa hàng</Breadcrumb.Item>
                </Breadcrumb>
                <div className='site-layout-background'>
                    <div className='container-fluid profile'>
                        {state.loading ? (
                            <LoadingPage />
                        ) : (
                            <Fragment>
                                <div className='user-profile'>
                                    <Row>
                                        <Col md={24} lg={12}>
                                            {state.userDetail.role === 1 ? (
                                                <Button className={`button blue`} onClick={() => showModal()}>
                                                    {lang.button.edit}
                                                </Button>
                                            ) : (
                                                <Button className={`button blue`} disabled>
                                                    {lang.button.edit}
                                                </Button>
                                            )}
                                            <ul className='info-profile'>
                                                <li>Thông tin cửa hàng</li>
                                                <li>
                                                    <span>
                                                        <i className='far fa-user'></i>
                                                        {lang.store.avatar}:
                                                    </span>
                                                    <Avatar
                                                        shape='square'
                                                        size={100}
                                                        icon={<UserOutlined />}
                                                        src={state.storeDetail.avatar}
                                                    />
                                                </li>
                                                <li>
                                                    <span>
                                                        <i className='far fa-address-book'></i>
                                                        {lang.store.name} :
                                                    </span>
                                                    {state.storeDetail.name}
                                                </li>
                                                <li>
                                                    <span>
                                                        <i className='fas fa-mobile-alt'></i>
                                                        {lang.store.tel} :
                                                    </span>
                                                    {state.storeDetail.tel}
                                                </li>

                                                <li>
                                                    <span>
                                                        <i className='far fa-address-card'></i>
                                                        {lang.store.address}
                                                    </span>
                                                    {state.storeDetail.address}
                                                </li>
                                                <li>
                                                    <span>
                                                        <i className='fas fa-align-justify'></i>
                                                        {lang.store.description}
                                                    </span>
                                                    {state.storeDetail.description}
                                                </li>
                                                <li>
                                                    <span>
                                                        <i className='far fa-clock'></i>
                                                        {lang.store.timeOpen}
                                                    </span>
                                                    {formatTime(state.storeDetail.timeOpen)}
                                                </li>
                                                <li>
                                                    <span>
                                                        <i className='fas fa-clock'></i>
                                                        {lang.store.timeClose}
                                                    </span>
                                                    {formatTime(state.storeDetail.timeClose)}
                                                </li>
                                            </ul>
                                        </Col>
                                    </Row>
                                </div>
                                <Modal
                                    title='Cập nhật thông tin'
                                    visible={state.modalShowing}
                                    destroyOnClose={true}
                                    onCancel={() => closeModal()}
                                    onOk={() => isUpdate()}
                                    okButtonProps={{ disabled: state.disabled }}
                                    className='modal-add-member'>
                                    <StoreEditPage
                                        lang={props.lang}
                                        checkUpdateStore={state.checkUpdateStore}
                                        closeModal={closeModal}
                                        store={state.storeDetail}
                                        ableButton={ableButton}
                                        updateStore={updateStore}
                                    />
                                </Modal>
                            </Fragment>
                        )}
                    </div>
                </div>
            </Content>
        </Fragment>
    )
}

export interface StoreProps {
    lang?: Language
    id?: string
}

export default StorePage
