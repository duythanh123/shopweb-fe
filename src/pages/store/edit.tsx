import React, { useState, useEffect } from 'react'
import { Form, Input, Avatar, TimePicker } from 'antd'
import moment from 'moment'
import { showNotification, getChangeItems } from '../../common/common'
import { Language } from '../../models/language'
import { StoreModel } from '../../models/strore.models'
import storeService from '../../services/store-service'
import uploadService from '../../services/upload-service'
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons'

export interface StoreProps {
    lang?: Language
    store?: StoreModel
    checkUpdateStore?: boolean
    closeModal?: Function
    ableButton?: Function
    updateStore?: Function
}
interface StoreState {
    imageUrl: any
    loading: boolean
    avatarGroup: File
    newStore: StoreModel
}
const StoreEditPage = (props: StoreProps) => {
    const [form] = Form.useForm()
    const { lang } = props

    const initalState: StoreState = {
        loading: true,
        avatarGroup: null,
        newStore: null,
        imageUrl: ''
    }

    const [state, setState] = useState(initalState)

    useEffect(() => {
        setState(s => ({ ...s, newStore: Object.assign({}, props.store), loading: false }))
    }, [])

    useEffect(() => {
        if (props.checkUpdateStore) {
            updateStore()
        }
    }, [props.checkUpdateStore])

    const changeItem = (value, key: string) => {
        const { newStore } = state
        console.log('value===>', value)
        newStore[key] = value
        props.ableButton()
        setState(s => ({ ...s, newStore: Object.assign({}, newStore) }))
    }

    const onChangeAvatar = async e => {
        const avatarGroup = e.target.files[0]
        const showAvatarGroup = e.target.files[0] ? URL.createObjectURL(e.target.files[0]) : null
        setState(s => ({ ...s, avatarGroup: avatarGroup, imageUrl: showAvatarGroup }))
    }

    const uploadButton = (
        <div>
            {state.loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div className='ant-upload-text'>Upload</div>
        </div>
    )

    const updateStore = async () => {
        const { newStore } = state
        const param: StoreModel = getChangeItems(props.store, newStore)
        if (!moment(param.timeOpen).isValid()) param.timeOpen = null
        if (!moment(param.timeClose).isValid()) param.timeClose = null
        if (param.tel === '') param.tel = null
        try {
            if (state.avatarGroup) {
                const result = await uploadService.uploadFile(state.avatarGroup)
                param.avatar = result.data?.url
            }
            const response = await storeService.editStore(props.store.id, param)
            if (response.status == 200) {
                showNotification(lang.notification.success, '', lang.item.store, lang.action.edit)
                props.closeModal()
                props.updateStore(response.data)
            } else {
                showNotification(lang.notification.error, response.data?.message)
            }
        } catch (error) {
            showNotification(lang.notification.error, lang.message_error.update_fail)
        }
    }

    const format = 'HH:mm'
    const initForm = {
        name: props.store.name,
        description: props.store.description,
        tel: props.store.tel,
        address: props.store.address
    }

    return (
        <div className='site-layout-background'>
            <div className='container-fluid edit-profile'>
                <Form form={form} initialValues={initForm}>
                    <Form.Item label='Hình ảnh' name='picture'>
                        {/* <Upload
                            name='avatar'
                            listType='picture-card'
                            className='avatar-uploader'
                            showUploadList={false}
                            action='https://www.mocky.io/v2/5cc8019d300000980a055e76'
                            beforeUpload={beforeUpload}
                            onChange={e => handleChange(e)}>
                            {state.imageUrl ? (
                                <Avatar shape='square' size={100} src={state.imageUrl} />
                            ) : props.store?.avatar ? (
                                <Avatar shape='square' size={100} src={props.store?.avatar} />
                            ) : (
                                uploadButton
                            )}
                        </Upload> */}
                        <div className='avatar'>
                            <div className='box-avatar'>
                                {state.imageUrl ? (
                                    <Avatar shape='square' size={100} src={state.imageUrl} />
                                ) : props.store?.avatar ? (
                                    <Avatar shape='square' size={100} src={props.store?.avatar} />
                                ) : (
                                    uploadButton
                                )}
                            </div>
                            <div className='upload'>
                                <input accept='image/*' type='file' onChange={onChangeAvatar} />
                            </div>
                        </div>
                    </Form.Item>
                    <Form.Item label={lang.store.name} rules={[{ max: 30 }]} name='name'>
                        <Input
                            placeholder={`Input ${lang.store.name}`}
                            onChange={e => changeItem(e.target.value, 'name')}
                            maxLength={30}></Input>
                    </Form.Item>

                    <Form.Item label={lang.store.description} rules={[{ max: 30 }]} name='description'>
                        <Input
                            placeholder={`Input ${lang.store.description}`}
                            onChange={e => changeItem(e.target.value, 'description')}
                            maxLength={30}></Input>
                    </Form.Item>

                    <Form.Item label={lang.store.tel} rules={[{ max: 15 }]} name='tel'>
                        <Input
                            placeholder={`Input ${lang.store.tel}`}
                            onChange={e => changeItem(e.target.value, 'tel')}
                            maxLength={15}
                            onKeyPress={e => {
                                if (!/^[0-9]/.test(e.key)) event.preventDefault()
                            }}></Input>
                    </Form.Item>

                    <Form.Item label={lang.store.address} rules={[{ max: 30 }]} name='address'>
                        <Input
                            placeholder={`Input ${lang.user.address}`}
                            onChange={e => changeItem(e.target.value, 'address')}
                            maxLength={30}></Input>
                    </Form.Item>

                    <Form.Item label={lang.store.timeOpen} name='timeOpen'>
                        <TimePicker
                            defaultValue={moment(props.store?.timeOpen ? moment(props.store?.timeOpen) : '', format)}
                            format={format}
                            onChange={date => changeItem(date, 'timeOpen')}
                        />
                    </Form.Item>

                    <Form.Item label={lang.store.timeClose} name='timeClose'>
                        <TimePicker
                            defaultValue={moment(props.store?.timeClose ? moment(props.store?.timeClose) : '', format)}
                            format={format}
                            onChange={date => changeItem(date, 'timeClose')}
                        />
                    </Form.Item>
                </Form>
            </div>
        </div>
    )
}

export default StoreEditPage
