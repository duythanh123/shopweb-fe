import React, { Fragment, useState, useEffect } from 'react'
import { Layout, Card, Button, Table, Row, Col, Modal, DatePicker, Breadcrumb } from 'antd'
import '../../styles/persons.scss'
import { Language } from '../../models/language'
import { TableModel } from '../../models/table.model'
import HeaderDom from '../../components/manager/header'
import { LoadingPage } from '../../jsx/loading'
import { showNotification, formatMoment, formatter } from '../../common/common'
import { pagination } from '../../common/constant'
import { billModel } from '../../models/bill.model'
import billService from '../../services/bill-service'

import ShowBill from './show'
import BreadcrumbItem from 'antd/lib/breadcrumb/BreadcrumbItem'
import { formatDate } from '../../common/date-util'
const { RangePicker } = DatePicker

interface TableProps {
    lang?: Language
}
interface TableState {
    table: TableModel
    loading: boolean
    list: billModel[]
    dateSearch: number[]
    modalShowing: string
    current: number
    pageSize: number
    total: number
    tableId: string
    bill: billModel
    loadTable: boolean
    showTotal?: boolean
}

const BillPage = (props: TableProps) => {
    const { lang } = props
    const { Content } = Layout
    const initalState: TableState = {
        loading: true,
        tableId: '',
        list: [],
        dateSearch: [],
        bill: null,
        modalShowing: '',
        current: pagination.current,
        pageSize: pagination.pageSize,
        total: pagination.total,
        table: null,
        loadTable: true,
        showTotal: false
    }

    const [state, setState] = useState(initalState)
    const fetchData = async () => {
        if (state.dateSearch?.length === 0) {
            try {
                const list = await billService.getBillList(state.current - 1, state.pageSize)

                setState(s => ({ ...s, list: list?.result, loading: false, loadTable: false, total: list?.count }))
            } catch (error) {
                showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
            }
        } else {
            try {
                const result = await billService.getBillListByDate(state.dateSearch, state.current - 1, state.pageSize)
                setState(s => ({ ...s, list: result?.result, loadTable: false, total: result?.count }))
            } catch (error) {
                console.log('nn', state.dateSearch)
            }
        }
    }
    useEffect(() => {
        if (state.loadTable) {
            fetchData()
        }
    }, [state])

    const onChange = value => {
        const dateSearch: number[] = []
        value?.map(date => {
            dateSearch.push(formatMoment(date))
        })
        setState(s => ({ ...s, dateSearch: dateSearch }))
    }

    const showModal = (type: string, bill?: billModel) => {
        setState(s => ({ ...s, modalShowing: type, bill: bill }))
    }
    const closeModal = () => {
        setState(s => ({ ...s, modalShowing: '' }))
    }
    const deleteBill = async (bill: billModel) => {
        try {
            const result = await billService.deleteBill(bill.id)
            if (result.status == 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.delete)

                setState(s => ({ ...s, modalShowing: '', loadTable: true }))
            } else {
                showNotification(lang.notification.error, result.data?.message)
            }
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    const onClickShow = async () => {
        setState(s => ({ ...s, loadTable: true, current: pagination.current, pageSize: pagination.pageSize }))
    }
    const handleChange = async pagination => {
        setState(s => ({
            ...s,
            current: pagination.current,
            pageSize: pagination.pageSize,
            total: pagination.total,
            loadTable: true
        }))
    }
    const columns = [
        {
            title: 'Mã bàn',
            dataIndex: 'table',
            key: 'table',
            render: (_: string, record: billModel): JSX.Element => {
                return <div>{record.table?.code}</div>
            }
        },
        {
            title: 'Mã bill',
            dataIndex: 'id',
            key: 'id'
        },
        {
            title: 'Ngày tạo',
            dataIndex: 'createDate',
            key: 'createDate',
            render: (_: string, record: billModel): JSX.Element => {
                return <div>{formatDate(record?.createDate)}</div>
            }
        },
        {
            title: 'Doanh thu',
            dataIndex: 'totalPrice',
            key: 'totalPrice',
            render: (_: string, record: billModel): JSX.Element => {
                return <div>{formatter.format(record.totalPrice)}</div>
            }
        },
        {
            title: 'Chi tiết',
            dataIndex: 'show',
            key: 'show',
            render: (_: string, record: billModel): JSX.Element => {
                return (
                    <div>
                        <Button type='primary' onClick={() => showModal('show', record)}>
                            Chi tiết
                        </Button>
                    </div>
                )
            }
        },
        {
            title: 'Xóa',
            dataIndex: 'delete',
            key: 'delete',
            render: (_: string, record: TableModel): JSX.Element => {
                return (
                    <Button type='primary' onClick={() => showModal('delete', record)}>
                        Xóa
                    </Button>
                )
            }
        }
    ]

    return (
        <Fragment>
            <HeaderDom itemMenu={3} />
            <Content
                className='site-layout-background'
                style={{
                    padding: 24,
                    margin: 0,
                    minHeight: 280
                }}>
                <div>
                    {state.loading ? (
                        <LoadingPage />
                    ) : (
                        <Fragment>
                            <Breadcrumb>
                                <BreadcrumbItem>Quản lý doanh thu</BreadcrumbItem>
                                <BreadcrumbItem>Danh sách doanh thu</BreadcrumbItem>
                            </Breadcrumb>
                            <Row gutter={{ xs: 4, sm: 8, md: 12, lg: 16 }}>
                                <Col className='gutter-row' xs={24} sm={12} md={12} lg={12}>
                                    <RangePicker onChange={onChange} />
                                    <Button type='primary' onClick={() => onClickShow()}>
                                        Xem
                                    </Button>
                                </Col>
                                {state.showTotal ? (
                                    <Col className='gutter-row' xs={24} sm={12} md={12} lg={12} id='total'>
                                        <div>
                                            <span>Tổng thu: </span>
                                            <span>
                                                <b>23000Đ</b>
                                            </span>
                                        </div>
                                    </Col>
                                ) : null}
                            </Row>
                            <div>
                                <Card
                                    title='Doanh thu'
                                    headStyle={{ background: '#70bf70', fontSize: 20, fontWeight: 'bold' }}>
                                    <Table
                                        columns={columns}
                                        rowKey='id'
                                        dataSource={state.list}
                                        pagination={{
                                            pageSize: state.pageSize,
                                            current: state.current,
                                            total: state.total
                                        }}
                                        onChange={handleChange}
                                        size='small'
                                        loading={state.loadTable}
                                    />
                                </Card>
                            </div>

                            <ShowBill
                                lang={props.lang}
                                visible={state.modalShowing === 'show'}
                                closeModal={closeModal}
                                bill={state.bill}
                            />
                            <Modal
                                title='Xóa bill '
                                visible={state.modalShowing === 'delete'}
                                destroyOnClose={true}
                                onCancel={() => closeModal()}
                                onOk={() => deleteBill(state.bill)}
                                okText='Xóa'
                                cancelText='Hủy'>
                                Xóa bill này?
                            </Modal>
                        </Fragment>
                    )}
                </div>
            </Content>
        </Fragment>
    )
}

export default BillPage
