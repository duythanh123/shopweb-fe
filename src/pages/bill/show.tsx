import React, { Component, Fragment, useState, useEffect } from 'react'
import { Layout, Space, Card, Form, Input, DatePicker, Select, Switch, Row, Col, Button, Modal, Table } from 'antd'
import { billModel } from '../../models/bill.model'
import { Language } from '../../models/language'

import { cartModel } from '../../models/cart.model'
import { formatTime, formatDay, formatDate } from '../../common/date-util'
import { formatter } from '../../common/common'

const { Header, Content } = Layout
const layout = {
    labelCol: {
        span: 8
    },
    wrapperCol: {
        span: 16
    }
}

export interface AreaProps {
    lang?: Language
    closeModal?: Function
    billList?: billModel[]
    visible?: boolean
    bill?: billModel
}

const ShowBill = (props: AreaProps) => {
    const [form] = Form.useForm()
    const { lang } = props

    const columns = [
        {
            title: 'Sản phẩm',
            dataIndex: 'product',
            key: 'product',
            render: (_: string, record: cartModel): JSX.Element => {
                return <div>{record.product.name}</div>
            }
        },
        {
            title: 'Số lượng',
            dataIndex: 'amount',
            key: 'amount'
        },

        {
            title: 'Giá',
            dataIndex: 'price',
            key: 'price',
            render: (_: string, record: cartModel): JSX.Element => {
                return <div>{formatter.format(record.product.price)}</div>
            }
        }
    ]

    return (
        <div>
            {form.setFieldsValue({
                id: props.bill?.id,
                totalPrice: formatter.format(props.bill?.totalPrice),
                code: props.bill?.table?.code,
                area: props.bill?.table?.area.id,
                name: props.bill?.table?.name,
                time: formatDate(props.bill?.createDate)
            })}
            <Modal
                footer={null}
                visible={props.visible}
                title='Chi tiết Bill'
                destroyOnClose={true}
                onCancel={() => props.closeModal()}>
                <Form {...layout} form={form} name='nest-add'>
                    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        <Col className='search-row' xs={24} sm={12} md={12} lg={12}>
                            <Form.Item label='Mã bill' name='id'>
                                <Input disabled className='ant-form-text'></Input>
                            </Form.Item>
                        </Col>
                        <Col className='search-row' xs={24} sm={12} md={12} lg={12}>
                            <Form.Item label='Doanh thu' name='totalPrice'>
                                <Input disabled className='ant-form-text'></Input>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        <Col className='search-row' xs={24} sm={12} md={12} lg={12}>
                            <Form.Item label='Mã bàn' name='code'>
                                <Input disabled className='ant-form-text'></Input>
                            </Form.Item>
                        </Col>
                        <Col className='search-row' xs={24} sm={12} md={12} lg={12}>
                            <Form.Item label='Khu vực' name='area'>
                                <Input disabled className='ant-form-text'></Input>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        <Col className='search-row' xs={24} sm={12} md={12} lg={12}>
                            <Form.Item label='Tên bàn' name='name'>
                                <Input disabled className='ant-form-text'></Input>
                            </Form.Item>
                        </Col>
                        <Col className='search-row' xs={24} sm={12} md={12} lg={12}>
                            <Form.Item label='Ngày tạo' name='time'>
                                <Input disabled className='ant-form-text'></Input>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Card headStyle={{ background: '#70bf70', fontSize: 20, fontWeight: 'bold' }}>
                        <Table
                            columns={columns}
                            rowKey='id'
                            dataSource={props.bill?.table?.carts}
                            pagination={false}
                            size='small'
                        />
                    </Card>
                </Form>
            </Modal>
        </div>
    )
}

export default ShowBill
