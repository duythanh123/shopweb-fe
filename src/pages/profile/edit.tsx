import React, { useState, useEffect, Fragment } from 'react'
import { Link } from 'react-router-dom'
import { Form, Row, Col, Input, DatePicker, Avatar, Breadcrumb, Layout, Modal } from 'antd'
import moment from 'moment'
import { UserInf, showNotification, getChangeItems, formatMoment } from '../../common/common'
import { setCookie } from '../../common/cookie'
import apiConstant from '../../configurations/api-constants'
import userService from '../../services/user-service'
import { Language } from '../../models/language'
import { UserModel } from '../../models/user.model'
import userAction from '../../redux/actions/user'
import { useDispatch } from 'react-redux'

export interface ProfileProps {
    lang?: Language
    profile?: UserModel
    closeModal?: Function
    updateUser?: Function
    modalShowing?: string
}
interface ProfileState {
    loading: boolean
    newProfile: UserModel
}
const ProfileEditModal = (props: ProfileProps) => {
    const [form] = Form.useForm()
    const { lang } = props

    const initalState: ProfileState = {
        loading: true,
        newProfile: null
    }
    const dispatch = useDispatch()
    const [state, setState] = useState(initalState)

    useEffect(() => {
        setState(s => ({ ...s, newProfile: Object.assign({}, props.profile), loading: false }))
    }, [])

    const changeItem = (value: string | number, key: string) => {
        const { newProfile } = state
        newProfile[key] = value
        setState(s => ({ ...s, newProfile: Object.assign({}, newProfile) }))
    }

    const updateProfile = async () => {
        const { newProfile } = state
        const param: UserModel = getChangeItems(props.profile, newProfile)
        if (param.tel === '') param.tel = null
        if (!moment(param.birthday).isValid()) param.birthday = null
        try {
            const response = await userService.editUser(props.profile.id, param)
            if (response.status == 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.edit)
                setCookie(UserInf, {
                    id: newProfile.id,
                    name: newProfile.name,
                    role: newProfile.role
                })
                props.closeModal()
                props.updateUser(response.data)
                dispatch({ type: userAction.updateUser, payload: response.data })
            } else {
                showNotification(lang.notification.error, response.data?.message)
            }
        } catch (error) {
            showNotification(lang.notification.error, lang.message_error.update_fail)
        }
    }

    const initForm = {
        name: props.profile.name,
        email: props.profile.email,
        birthday: props.profile?.birthday ? moment(props.profile?.birthday) : undefined,
        address: props.profile.address,
        tel: props.profile?.tel
    }
    return (
        <Fragment>
            <Modal
                title='Cập nhật thông tin'
                visible={props.modalShowing === 'update-profile'}
                destroyOnClose={true}
                okText='Cập nhật'
                cancelText='Hủy'
                onCancel={() => props.closeModal()}
                onOk={() => {
                    form.validateFields()
                        .then(() => {
                            form.resetFields()
                            updateProfile()
                        })
                        .catch(info => {
                            console.log('Validate Failed:', info)
                        })
                }}>
                <div className='site-layout-background'>
                    <div className='container-fluid edit-profile'>
                        <Form form={form} initialValues={initForm}>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Col span={8} offset={2}>
                                    <Form.Item label={lang.user.user_name}>
                                        <span className='ant-form-text'>{props.profile?.userName}</span>
                                    </Form.Item>
                                </Col>
                                <Col span={8} offset={2}>
                                    <Form.Item
                                        label={lang.user.name}
                                        rules={[{ required: true }, { max: 30 }]}
                                        name='name'>
                                        <Input
                                            placeholder={`Input ${lang.user.name}`}
                                            onChange={e => changeItem(e.target.value, 'name')}
                                            maxLength={30}></Input>
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Col span={8} offset={2}>
                                    <Form.Item label={lang.user.tel} rules={[{ max: 15 }]} name='tel'>
                                        <Input
                                            placeholder={`Input ${lang.user.tel}`}
                                            onChange={e => changeItem(e.target.value, 'tel')}
                                            maxLength={15}
                                            onKeyPress={e => {
                                                if (!/^[0-9]/.test(e.key)) event.preventDefault()
                                            }}></Input>
                                    </Form.Item>
                                </Col>
                                <Col span={8} offset={2}>
                                    <Form.Item
                                        label={lang.user.email}
                                        rules={[{ required: true }, { type: 'email' }, { max: 30 }]}
                                        name='email'>
                                        <Input
                                            placeholder={`Input ${lang.user.email}`}
                                            onChange={e => changeItem(e.target.value, 'email')}
                                            maxLength={30}></Input>
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                <Col span={8} offset={2}>
                                    <Form.Item label={lang.user.birthday} name='birthday'>
                                        <DatePicker
                                            onChange={date => changeItem(formatMoment(date), 'birthday')}
                                            format='DD/MM/YYYY'
                                        />
                                    </Form.Item>
                                </Col>
                                <Col span={8} offset={2}>
                                    <Form.Item label={lang.user.address} rules={[{ max: 30 }]} name='address'>
                                        <Input
                                            placeholder={`Input ${lang.user.address}`}
                                            onChange={e => changeItem(e.target.value, 'address')}
                                            maxLength={30}></Input>
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Form>
                    </div>
                </div>
            </Modal>
        </Fragment>
    )
}

export default ProfileEditModal
