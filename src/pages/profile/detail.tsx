import React, { Fragment, useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Row, Col, Breadcrumb, Layout, Card, Modal, Button } from 'antd'
import { LoadingPage } from '../../jsx/loading'
import { ButtonMain } from '../../jsx/button'
import { UserInf, showNotification } from '../../common/common'
import { getCookie } from '../../common/cookie'
import { formatDate } from '../../common/date-util'
import userService from '../../services/user-service'
import { Language } from '../../models/language'
import { UserModel } from '../../models/user.model'
import '../../styles/persons.scss'
import defaultAvatar from '../../static/images/avatar-default.png'
import '../../styles/profile.scss'
import '../../styles/manager.scss'
import ProfileEditModal from './edit'
import ChangePasswordPage from './change-password'
import HeaderDom from '../../components/manager/header'

interface ProfileState {
    loading: boolean
    userDetail?: UserModel
    modalShowing?: string
    disabled?: boolean
}
const ProfilePage = (props: ProfileProps) => {
    const { lang } = props
    const { Content } = Layout

    const initalState: ProfileState = {
        loading: true,
        userDetail: {},
        modalShowing: '',
        disabled: true
    }

    const [state, setState] = useState(initalState)

    const fetchData = async () => {
        let userDetail = {}
        try {
            userDetail = await userService.getUserDetail(getCookie(UserInf).id)
        } catch (error) {
            showNotification(lang.notification.error, lang.item.data, lang.action.load)
        }
        setState(s => ({ ...s, userDetail, loading: false }))
    }

    useEffect(() => {
        if (state.loading) {
            fetchData()
        }
    }, [state])

    const closeModal = () => {
        setState(s => ({ ...s, modalShowing: '', disabled: true }))
    }

    const showModal = (type: string) => {
        setState(s => ({ ...s, modalShowing: type }))
    }

    const updateUser = (user: UserModel) => {
        setState(s => ({ ...s, userDetail: user }))
    }

    return (
        <Fragment>
            <HeaderDom itemMenu={1} />
            <Content
                className='site-layout-background'
                style={{
                    padding: 24,
                    margin: 0,
                    minHeight: 280
                }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item>Quản lý tài khoản</Breadcrumb.Item>
                    <Breadcrumb.Item>Thông tin cá nhân</Breadcrumb.Item>
                </Breadcrumb>
                <div className='site-layout-background'>
                    <div className='container-fluid profile'>
                        {state.loading ? (
                            <LoadingPage />
                        ) : (
                            <Fragment>
                                <div>
                                    <Row>
                                        <Col md={24} lg={12}>
                                            <Button
                                                className={`button blue`}
                                                onClick={() => showModal('update-profile')}>
                                                {lang.button.edit}
                                            </Button>
                                            <ul className='info-profile'>
                                                <li>Thông tin tài khoản</li>
                                                <li>
                                                    <span>
                                                        <i className='far fa-user'></i>
                                                        {lang.user.user_name}
                                                    </span>
                                                    {state.userDetail.userName}
                                                </li>
                                                <li>
                                                    <span>
                                                        <i className='far fa-user'></i>
                                                        {lang.user.name} :
                                                    </span>
                                                    {state.userDetail.name}
                                                </li>
                                                <li>
                                                    <span>
                                                        <i className='fas fa-mobile-alt'></i>
                                                        {lang.user.tel} :
                                                    </span>
                                                    {state.userDetail.tel}
                                                </li>
                                                <li>
                                                    <span>
                                                        <i className='fas fa-gift'></i>
                                                        {lang.user.birthday} :
                                                    </span>
                                                    {formatDate(state.userDetail.birthday)}
                                                </li>
                                                <li>
                                                    <span>
                                                        <i className='far fa-envelope'></i>
                                                        {lang.user.email} :
                                                    </span>
                                                    {state.userDetail.email}
                                                </li>
                                                <li>
                                                    <span>
                                                        <i className='fas fa-mobile-alt'></i>
                                                        {lang.user.address} :
                                                    </span>
                                                    {state.userDetail.address}
                                                </li>
                                            </ul>
                                        </Col>
                                    </Row>
                                </div>
                                <a className='link-change-pass' onClick={() => showModal('change-pass')}>
                                    <b>
                                        Thay đổi mật khẩu
                                        <i className='far fa-edit'></i>{' '}
                                    </b>
                                </a>
                                <ProfileEditModal
                                    lang={props.lang}
                                    closeModal={closeModal}
                                    profile={state.userDetail}
                                    updateUser={updateUser}
                                    modalShowing={state.modalShowing}
                                />
                                <Modal
                                    title='Cập nhật mật khẩu'
                                    visible={state.modalShowing === 'change-pass'}
                                    destroyOnClose={true}
                                    footer={null}
                                    onCancel={() => closeModal()}>
                                    <ChangePasswordPage closeModal={closeModal} />
                                </Modal>
                            </Fragment>
                        )}
                    </div>
                </div>
            </Content>
        </Fragment>
    )
}

export interface ProfileProps {
    lang?: Language
    id?: string
}

export default ProfilePage
