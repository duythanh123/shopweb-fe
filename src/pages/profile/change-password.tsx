import React from 'react'
import { LockOutlined } from '@ant-design/icons'
import { Form, Input } from 'antd'
import { ButtonChange, ButtonCancel } from '../../jsx/button'
import loginService from '../../services/login-service'
import { showNotification } from '../../common/common'
import { Language } from '../../models/language'
import '../../styles/form.scss'

export interface ChangePassProps {
    lang?: Language
    closeModal?: Function
}
const ChangePasswordPage = (props: ChangePassProps) => {
    const [form] = Form.useForm()
    const onFinish = async () => {
        try {
            const response = await loginService.changePassword({
                oldPassword: form.getFieldValue('oldPassword'),
                newPassword: form.getFieldValue('newPassword')
            })
            if (response.status === 200) {
                showNotification('success', '', 'Password', ' changed')
                props.closeModal()
            } else {
                showNotification('error', response.data?.message)
                form.resetFields()
            }
        } catch {
            showNotification('error', 'Change password failed.')
            form.resetFields()
        }
    }

    return (
        <div className='site-layout-background'>
            <div className='container-fluid change-pass'>
                <Form form={form} onFinish={onFinish}>
                    <Form.Item
                        name='oldPassword'
                        rules={[{ required: true, message: 'Please input your old password!' }, { max: 30 }]}>
                        <Input.Password prefix={<LockOutlined />} placeholder='Input old password' maxLength={30} />
                    </Form.Item>
                    <Form.Item
                        name='newPassword'
                        rules={[{ required: true, message: 'Please input your new password!' }, { max: 30 }]}>
                        <Input.Password prefix={<LockOutlined />} placeholder='Input new password' maxLength={30} />
                    </Form.Item>
                    <Form.Item
                        name='confirmPassword'
                        dependencies={['newPassword']}
                        rules={[
                            { required: true, message: 'Please confirm your new password!' },
                            { max: 30 },
                            ({ getFieldValue }) => ({
                                validator(rule, value) {
                                    if (!value || getFieldValue('newPassword') === value) {
                                        return Promise.resolve()
                                    }
                                    return Promise.reject('The two passwords that you entered do not match!')
                                }
                            })
                        ]}>
                        <Input.Password prefix={<LockOutlined />} placeholder='Confirm new password' maxLength={30} />
                    </Form.Item>
                    <ButtonChange />
                    <ButtonCancel onClick={() => props.closeModal()} />
                </Form>
            </div>
        </div>
    )
}

export default ChangePasswordPage
