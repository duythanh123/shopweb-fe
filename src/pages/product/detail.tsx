import React, { Fragment, useState, useEffect } from 'react'
import { Layout, Card, Button, Table, Row, Col, Input, Modal, Avatar, Space, Breadcrumb } from 'antd'
import '../../styles/persons.scss'
import { Language } from '../../models/language'
import { productModel } from '../../models/product.model'
import productService from '../../services/product-service'
import Edit from './edit'
import Add from './add'
import HeaderDom from '../../components/manager/header'
import { LoadingPage } from '../../jsx/loading'
import { showNotification, formatter } from '../../common/common'
import { CategoryModel } from '../../models/src_models_category.model'
import categoryService from '../../services/src_services_category-service '
import { UserOutlined, SearchOutlined, FileAddOutlined } from '@ant-design/icons'
import { pagination } from '../../common/constant'
import BreadcrumbItem from 'antd/lib/breadcrumb/BreadcrumbItem'

interface ProductProps {
    lang?: Language
}
interface ProductState {
    loading: boolean
    list: productModel[]
    categoryList: CategoryModel[]
    product: productModel
    modalShowing: string
    searchText?: string
    searchedColumn?: string
    current: number
    pageSize: number
    total: number
    loadTable: boolean
}

const ProductPage = (props: ProductProps) => {
    const { lang } = props
    const { Content } = Layout
    const initalState: ProductState = {
        loading: true,
        list: [],
        categoryList: [],
        product: null,
        modalShowing: '',
        searchText: '',
        searchedColumn: '',
        current: pagination.current,
        pageSize: pagination.pageSize,
        total: pagination.total,
        loadTable: true
    }

    const [state, setState] = useState(initalState)
    const fetchData = async () => {
        let categoryList: CategoryModel[] = []
        if (state.searchText === '') {
            try {
                categoryList = await categoryService.getCategoryList()
                const data = await productService.getProductList(state.current - 1, state.pageSize)
                setState(s => ({
                    ...s,
                    list: data?.result,
                    categoryList,
                    loading: false,
                    total: data?.count,
                    loadTable: false
                }))
            } catch (error) {
                showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
            }
        } else {
            try {
                const result = await productService.getProductDetailWithName(
                    state.searchText,
                    state.current - 1,
                    state.pageSize
                )
                setState(s => ({
                    ...s,
                    list: result.result,
                    total: result.count,
                    loading: false,
                    loadTable: false
                }))
            } catch (error) {
                showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
            }
        }
    }
    useEffect(() => {
        if (state.loadTable) {
            fetchData()
        }
    }, [state])
    const handleChange = async pagination => {
        try {
            const data = await productService.getProductList(pagination.current - 1, pagination.pageSize)
            setState(s => ({
                ...s,
                list: data?.result,
                current: pagination.current,
                pageSize: pagination.pageSize,
                total: pagination.total,
                loadTable: true
            }))
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }

    const showModal = (type: string, product?: productModel) => {
        setState(s => ({ ...s, modalShowing: type, product: product }))
    }
    const closeModal = () => {
        setState(s => ({ ...s, modalShowing: '' }))
    }
    const onCreate = async value => {
        let { modalShowing } = state
        try {
            const result = await productService.createProduct(value)
            if (result.status === 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.create)
                modalShowing = ''
                setState(s => ({ ...s, loadTable: true, modalShowing, current: 1 }))
            } else {
                showNotification(lang.notification.error, result.data?.message)
            }
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
        setState(s => ({ ...s, modalShowing: '' }))
    }
    const onEdit = async value => {
        try {
            const result = await productService.editProduct(state.product.id, value)
            if (result.status === 200) {
                setState(s => ({ ...s, loadTable: true, modalShowing: '' }))
                showNotification(lang.notification.success, '', lang.item.user, lang.action.edit)
            } else {
                showNotification(lang.notification.error, result.data?.message)
            }
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.edit)
        }
    }
    const deleteProduct = async (product: productModel) => {
        try {
            const result = await productService.deleteProduct(product.id)
            if (result.status == 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.delete)
                setState(s => ({ ...s, modalShowing: '', loadTable: true }))
            } else {
                showNotification(lang.notification.error, result.data?.message)
            }
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    //search
    const getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{
                        width: 188,
                        marginBottom: 8,
                        display: 'block'
                    }}
                />
                <Space>
                    <Button
                        type='primary'
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size='small'
                        style={{ width: 90 }}>
                        Search
                    </Button>
                    <Button onClick={() => handleReset(clearFilters)} size='small' style={{ width: 90 }}>
                        Reset
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) =>
            record[dataIndex]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select())
            }
        }
    })

    const handleSearch = async (selectedKeys, confirm, dataIndex) => {
        confirm()
        setState(s => ({
            ...s,
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
            current: pagination.current,
            pageSize: pagination.pageSize,
            loadTable: true
        }))
    }

    const handleReset = clearFilters => {
        clearFilters()
        setState(s => ({ ...s, searchText: '', loadTable: true }))
    }
    const columns = [
        {
            title: 'Hình đại diện',
            dataIndex: 'picture',
            key: 'picture',
            render: (_: string, record: productModel): JSX.Element => {
                return <Avatar shape='square' size={70} icon={<UserOutlined />} src={record.picture} />
            }
        },
        {
            title: 'Tên sản phẩm',
            dataIndex: 'name',
            key: 'name',
            ...getColumnSearchProps('name')
        },
        {
            title: 'Nhóm món',
            dataIndex: 'category',
            key: 'category',
            render: (_: string, record: productModel): JSX.Element => {
                let category: string = null
                state.categoryList.forEach(item => {
                    if (item.id === record.categoryId) category = item.name
                })
                return <div>{category}</div>
            }
        },
        {
            title: 'Giá bán',
            dataIndex: 'price',
            key: 'price',
            render: (_: string, record: productModel): JSX.Element => {
                return <div>{formatter.format(record.price)}</div>
            }
        },
        {
            title: 'Cập nhật',
            dataIndex: 'update',
            key: 'update',
            render: (_: string, record: productModel): JSX.Element => {
                return (
                    <div>
                        <Button type='primary' onClick={() => showModal('edit', record)}>
                            Cập nhật
                        </Button>
                    </div>
                )
            }
        },
        {
            title: 'Xóa',
            dataIndex: 'delete',
            key: 'delete',
            render: (_: string, record: productModel): JSX.Element => {
                return (
                    <Button type='primary' onClick={() => showModal('delete', record)}>
                        Xóa
                    </Button>
                )
            }
        }
    ]

    return (
        <Fragment>
            <HeaderDom itemMenu={8} />
            <Content
                className='site-layout-background'
                style={{
                    padding: 24,
                    margin: 0,
                    minHeight: 280
                }}>
                <div>
                    {state.loading ? (
                        <LoadingPage />
                    ) : (
                        <Fragment>
                            <div>
                                <Breadcrumb>
                                    <BreadcrumbItem>Danh mục</BreadcrumbItem>
                                    <BreadcrumbItem>Đồ uống - món ăn</BreadcrumbItem>
                                </Breadcrumb>
                                <Row gutter={{ xs: 4, sm: 8, md: 12, lg: 16 }}>
                                    <Col
                                        className='gutter-row'
                                        xs={24}
                                        sm={24}
                                        md={24}
                                        lg={24}
                                        style={{ textAlign: 'right' }}>
                                        <Button
                                            type='default'
                                            icon={<FileAddOutlined />}
                                            size='large'
                                            onClick={() => showModal('add')}>
                                            Thêm
                                        </Button>
                                    </Col>
                                </Row>
                            </div>
                            <div>
                                <Card
                                    title='Đồ uống- Món ăn'
                                    headStyle={{
                                        background: '#70bf70',
                                        fontSize: 20,
                                        fontWeight: 'bold'
                                    }}>
                                    <Table
                                        columns={columns}
                                        rowKey='id'
                                        dataSource={state.list}
                                        pagination={{
                                            pageSize: state.pageSize,
                                            current: state.current,
                                            total: state.total
                                        }}
                                        loading={state.loadTable}
                                        onChange={handleChange}
                                        size='small'
                                    />
                                </Card>
                            </div>
                            <Add
                                lang={props.lang}
                                visible={state.modalShowing === 'add'}
                                closeModal={closeModal}
                                onCreate={onCreate}
                                categoryList={state.categoryList}
                            />
                            <Edit
                                lang={props.lang}
                                visible={state.modalShowing === 'edit'}
                                product={state.product}
                                closeModal={closeModal}
                                onEdit={onEdit}
                                categoryList={state.categoryList}
                            />
                            <Modal
                                title='Xóa sản phẩm'
                                visible={state.modalShowing === 'delete'}
                                destroyOnClose={true}
                                onCancel={() => closeModal()}
                                onOk={() => deleteProduct(state.product)}
                                okText='Xóa'
                                cancelText='Hủy'>
                                Xác nhận xóa sản phẩm này?
                            </Modal>
                        </Fragment>
                    )}
                </div>
            </Content>
        </Fragment>
    )
}

export default ProductPage
