import React, { useState } from 'react'
import { Form, Input, Select, Modal, Avatar } from 'antd'
import { Language } from '../../models/language'
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons'
import { CategoryModel } from '../../models/src_models_category.model'
import uploadService from '../../services/upload-service'

const layout = {
    labelCol: {
        span: 8
    },
    wrapperCol: {
        span: 16
    }
}
const validateMessages = {
    required: '${label} không được trống!',
    types: {
        email: '${label} không phải là địa chỉ email!'
    }
}
export interface AddProps {
    lang?: Language
    visible?: boolean
    closeModal?: any
    onCreate?: Function
    categoryList?: CategoryModel[]
}

const Add = (props: AddProps) => {
    const [form] = Form.useForm()
    const { lang } = props
    const [state, setState] = useState({
        loading: false,
        imageUrl: null,
        avatarProduct: null
    })

    const uploadButton = (
        <div>
            {state.loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div className='ant-upload-text'>Upload</div>
        </div>
    )

    const onChangeAvatar = async e => {
        const avatarProduct = e.target.files[0]
        const showAvatarProduct = e.target.files[0] ? URL.createObjectURL(e.target.files[0]) : null
        setState(s => ({ ...s, avatarProduct: avatarProduct, imageUrl: showAvatarProduct }))
    }
    return (
        <div>
            <Modal
                visible={props.visible}
                title='Thêm món'
                destroyOnClose={true}
                okText='Tạo'
                cancelText='Hủy'
                onCancel={props.closeModal}
                onOk={() => {
                    form.validateFields()
                        .then(async values => {
                            if (state.avatarProduct) {
                                const result = await uploadService.uploadFile(state.avatarProduct)
                                values.picture = result.data?.url
                            }
                            form.resetFields()
                            props.onCreate(values)
                            setState(s => ({ ...s, imageUrl: null }))
                        })
                        .catch(info => {
                            console.log(info)
                        })
                }}>
                <Form {...layout} form={form} name='nest-add' validateMessages={validateMessages}>
                    <Form.Item label='Hình ảnh' name='picture'>
                        <div className='avatar'>
                            <div className='box-avatar'>
                                {state.imageUrl ? (
                                    <Avatar shape='square' size={100} src={state.imageUrl} />
                                ) : (
                                    uploadButton
                                )}
                            </div>
                            <div className='upload'>
                                <input accept='image/*' type='file' onChange={onChangeAvatar} />
                            </div>
                        </div>
                    </Form.Item>
                    <Form.Item
                        name='name'
                        label='Tên sản phẩm'
                        rules={[
                            {
                                required: true
                            }
                        ]}>
                        <Input style={{ width: 250 }} />
                    </Form.Item>
                    <Form.Item
                        label='Nhóm món'
                        name='categoryId'
                        rules={[
                            {
                                required: true
                            }
                        ]}>
                        <Select style={{ width: 250 }}>
                            {props.categoryList.map(item => (
                                <Select.Option value={item.id} key={item.id}>
                                    {item.name}
                                </Select.Option>
                            ))}
                        </Select>
                    </Form.Item>
                    <Form.Item
                        label='Giá bán'
                        name='price'
                        rules={[
                            {
                                required: true
                            }
                        ]}>
                        <Input
                            style={{ width: 250 }}
                            onKeyPress={e => {
                                if (!/^[0-9]/.test(e.key)) event.preventDefault()
                            }}
                        />
                    </Form.Item>
                    <Form.Item label='Mô tả' name='description'>
                        <Input.TextArea style={{ width: 250 }} />
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    )
}
export default Add
