import React, { Fragment, useState, useEffect } from 'react'
import { Layout, Card, Button, Table, Row, Col, Input, Modal, Select, Breadcrumb } from 'antd'
import '../../styles/persons.scss'
import { Language } from '../../models/language'
import { TableModel } from '../../models/table.model'
import tableService from '../../services/table-service'
import Add from './add'
import Edit from './edit'
import HeaderDom from '../../components/manager/header'
import { LoadingPage } from '../../jsx/loading'
import { showNotification } from '../../common/common'
import { AreaModel } from '../../models/area.model'
import areaService from '../../services/area-service'
import { pagination } from '../../common/constant'
import BreadcrumbItem from 'antd/lib/breadcrumb/BreadcrumbItem'

interface TableProps {
    lang?: Language
}
interface TableState {
    loading: boolean
    list: TableModel[]
    areaList: AreaModel[]
    table: TableModel
    modalShowing: string
    current: number
    pageSize: number
    total: number
    areaId: string
    loadTable: boolean
}

const TablePage = (props: TableProps) => {
    const { lang } = props
    const { Content } = Layout
    const initalState: TableState = {
        loading: true,
        areaId: '',
        list: [],
        areaList: [],
        table: null,
        modalShowing: '',
        current: pagination.current,
        pageSize: pagination.pageSize,
        total: pagination.total,
        loadTable: true
    }

    const [state, setState] = useState(initalState)
    const fetchData = async () => {
        let list: TableModel[] = []
        let areaList: AreaModel[] = []
        if (state.areaId === '' || state.areaId === 'all') {
            try {
                areaList = await areaService.getAreaList()
            } catch (error) {
                showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
            }
            try {
                list = await tableService.getTableList()
            } catch (error) {
                showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
            }
            setState(s => ({ ...s, list, areaList, loading: false, loadTable: false }))
        } else {
            try {
                const result = await tableService.getTableByIdArea(state.areaId)
                setState(s => ({ ...s, list: result, loading: false, loadTable: false }))
            } catch (error) {
                showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
            }
        }
    }
    useEffect(() => {
        if (state.loadTable) {
            fetchData()
        }
    }, [state])
    const showModal = (type: string, table?: TableModel) => {
        setState(s => ({ ...s, modalShowing: type, table: table }))
    }
    const closeModal = () => {
        setState(s => ({ ...s, modalShowing: '' }))
    }
    const deleteTable = async (table: TableModel) => {
        try {
            const result = await tableService.deleteTable(table.id)
            if (result.status == 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.delete)
                setState(s => ({ ...s, modalShowing: '', loadTable: true }))
            } else {
                showNotification(lang.notification.error, result.data?.message)
            }
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    const onCreate = async value => {
        try {
            const rs = await tableService.createTable(value)
            if (rs.status == 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.create)
                setState(s => ({ ...s, loadTable: true, modalShowing: '' }))
            } else {
                showNotification(lang.notification.error, rs.data?.message)
            }
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    const onEdit = async value => {
        try {
            const rs = await tableService.editTable(state.table.id, value)
            if (rs.status === 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.edit)
                setState(s => ({ ...s, loadTable: true, table: null, modalShowing: '' }))
            } else {
                showNotification(lang.notification.error, rs.data?.message)
            }
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    const changeArea = value => {
        setState(s => ({ ...s, areaId: value }))
    }
    const handleSearch = () => {
        setState(s => ({ ...s, loadTable: true }))
    }

    const columns = [
        {
            title: 'Khu vực',
            dataIndex: 'area',
            key: 'area',
            render: (_: string, record: TableModel): JSX.Element => {
                return <div>{record.area?.name}</div>
            }
        },
        {
            title: 'Tên bàn',
            dataIndex: 'name',
            key: 'name'
        },
        {
            title: 'Mã bàn',
            dataIndex: 'code',
            key: 'code'
        },
        {
            title: 'Trạng thái',
            dataIndex: 'status',
            key: 'status',
            render: (_: string, record: TableModel): JSX.Element => {
                if (record.status) return <div>Trống</div>
                else return <div>Có khách</div>
            }
        },
        {
            title: 'Cập nhật',
            dataIndex: 'update',
            key: 'update',
            render: (_: string, record: TableModel): JSX.Element => {
                return (
                    <div>
                        <Button type='primary' onClick={() => showModal('edit', record)}>
                            Cập nhật
                        </Button>
                    </div>
                )
            }
        },
        {
            title: 'Xóa',
            dataIndex: 'delete',
            key: 'delete',
            render: (_: string, record: TableModel): JSX.Element => {
                return (
                    <Button type='primary' onClick={() => showModal('delete', record)}>
                        Xóa
                    </Button>
                )
            }
        }
    ]

    return (
        <Fragment>
            <HeaderDom itemMenu={5} />
            <Content
                className='site-layout-background'
                style={{
                    padding: 24,
                    margin: 0,
                    minHeight: 280
                }}>
                <div>
                    {state.loading ? (
                        <LoadingPage />
                    ) : (
                        <Fragment>
                            <div>
                                <Breadcrumb>
                                    <BreadcrumbItem>Quản lý phòng bàn</BreadcrumbItem>
                                    <BreadcrumbItem>Phòng bàn</BreadcrumbItem>
                                </Breadcrumb>
                                <Row gutter={{ xs: 4, sm: 8, md: 12, lg: 16 }}>
                                    <Col
                                        className='gutter-row'
                                        xs={12}
                                        sm={12}
                                        md={12}
                                        lg={12}
                                        style={{ textAlign: 'left' }}>
                                        <Button type='primary' onClick={() => showModal('add')}>
                                            Thêm
                                        </Button>
                                    </Col>
                                    <Col
                                        className='gutter-row'
                                        xs={12}
                                        sm={12}
                                        md={12}
                                        lg={12}
                                        style={{ textAlign: 'right' }}>
                                        <Select
                                            style={{ width: 100, textAlign: 'center' }}
                                            defaultValue='all'
                                            onChange={changeArea}>
                                            <Select.Option value='all' key={1}>
                                                Tất cả
                                            </Select.Option>
                                            {state.areaList.map(item => (
                                                <Select.Option value={item.id} key={item.id}>
                                                    {item.name}
                                                </Select.Option>
                                            ))}
                                        </Select>
                                        <Button onClick={handleSearch} type='primary'>
                                            Tìm kiếm
                                        </Button>
                                    </Col>
                                </Row>
                            </div>
                            <div>
                                <Card
                                    title='Phòng bàn'
                                    headStyle={{ background: '#70bf70', fontSize: 20, fontWeight: 'bold' }}>
                                    <Table
                                        columns={columns}
                                        rowKey='id'
                                        dataSource={state.list}
                                        size='small'
                                        loading={state.loadTable}
                                    />
                                </Card>
                            </div>
                            <Edit
                                lang={props.lang}
                                visible={state.modalShowing === 'edit'}
                                closeModal={closeModal}
                                onEdit={onEdit}
                                areaList={state.areaList}
                                table={state.table}
                            />
                            <Add
                                lang={props.lang}
                                visible={state.modalShowing === 'add'}
                                closeModal={closeModal}
                                onCreate={onCreate}
                                areaList={state.areaList}
                            />
                            <Modal
                                title='Xóa phòng bàn'
                                visible={state.modalShowing === 'delete'}
                                destroyOnClose={true}
                                onCancel={() => closeModal()}
                                onOk={() => deleteTable(state.table)}
                                okText='Xóa'
                                cancelText='Hủy'>
                                Xóa phòng bàn này?
                            </Modal>
                        </Fragment>
                    )}
                </div>
            </Content>
        </Fragment>
    )
}

export default TablePage
