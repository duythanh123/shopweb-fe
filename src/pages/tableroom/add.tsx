import React from 'react'
import { Form, Input, Select, Modal } from 'antd'
import { Language } from '../../models/language'

import { AreaModel } from '../../models/area.model'
const layout = {
    labelCol: {
        span: 8
    },
    wrapperCol: {
        span: 16
    }
}
const validateMessages = {
    required: '${label} không được trống!'
}
export interface AddProps {
    lang?: Language
    visible?: boolean
    closeModal?: any
    onCreate?: Function
    areaList?: AreaModel[]
}

const AddTable = (props: AddProps) => {
    const [form] = Form.useForm()
    const { lang } = props

    return (
        <Modal
            visible={props.visible}
            title='Thêm phòng bàn'
            destroyOnClose={true}
            okText='Tạo'
            cancelText='Hủy'
            onCancel={props.closeModal}
            onOk={() => {
                form.validateFields()
                    .then(values => {
                        if (values.status) values.status = true
                        else values.status = false
                        form.resetFields()
                        props.onCreate(values)
                    })
                    .catch(info => {
                        console.log('Validate Failed:', info)
                    })
            }}>
            <Form {...layout} form={form} name='nest-add' validateMessages={validateMessages}>
                <Form.Item
                    label='Khu vực'
                    name='areaId'
                    rules={[
                        {
                            required: true
                        }
                    ]}>
                    <Select style={{ width: 250 }}>
                        {props.areaList.map(item => (
                            <Select.Option value={item.id} key={item.id}>
                                {item.name}
                            </Select.Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item
                    label='Tên bàn'
                    name='name'
                    rules={[
                        {
                            required: true
                        }
                    ]}>
                    <Input style={{ width: 250 }} />
                </Form.Item>
                <Form.Item
                    label='Mã bàn'
                    name='code'
                    rules={[
                        {
                            required: true
                        }
                    ]}>
                    <Input style={{ width: 250 }} />
                </Form.Item>
                <Form.Item label='Trạng thái' name='status'>
                    <Select style={{ width: 250 }}>
                        <Select.Option value={1}>Trống</Select.Option>
                        <Select.Option value={0}>Có khách</Select.Option>
                    </Select>
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default AddTable
