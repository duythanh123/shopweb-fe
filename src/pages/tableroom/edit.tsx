import React, { Component, Fragment, useState, useEffect } from 'react'
import { Layout, Space, Card, Form, Input, DatePicker, Select, Switch, Row, Col, Button, Modal } from 'antd'
import { Language } from '../../models/language'
import { AreaModel } from '../../models/area.model'
import areaService from '../../services/area-service'
import { TableModel } from '../../models/table.model'
import tableService from '../../services/table-service'
import { showNotification, getChangeItems, UserInf, formatMoment } from '../../common/common'
import moment from 'moment'
import userAction from '../../redux/actions/user'
import { useDispatch } from 'react-redux'

const { Header, Content } = Layout
const layout = {
    labelCol: {
        span: 8
    },
    wrapperCol: {
        span: 16
    }
}
const validateMessages = {
    required: '${label} không được trống!'
}
interface AreaProps {
    lang?: Language
    closeModal?: Function
    areaList?: AreaModel[]
    onEdit?: Function
    visible?: boolean
    table: TableModel
}
interface AreaState {
    loading: boolean
    table: TableModel
}
const EditArea = (props: AreaProps) => {
    const [form] = Form.useForm()
    const { lang } = props
    const initalState: AreaState = {
        loading: true,
        table: null
    }
    return (
        <div>
            {form.setFieldsValue({
                areaId: props?.table?.area?.id,
                areaName: props?.table?.area?.name,
                name: props?.table?.name,
                code: props?.table?.code,
                status: props?.table?.status ? 1 : 0
            })}
            <Modal
                visible={props.visible}
                title='Thêm phòng bàn'
                destroyOnClose={true}
                okText='Cập nhật'
                cancelText='Hủy'
                onCancel={() => props.closeModal()}
                onOk={() => {
                    form.validateFields()
                        .then(values => {
                            if (values.status === 1) values.status = true
                            else values.status = false
                            console.log('valuae===>', props.table)
                            form.resetFields()
                            props.onEdit(values)
                        })
                        .catch(info => {
                            console.log('Validate Failed:', info)
                        })
                }}>
                <Form {...layout} form={form} name='nest-add' validateMessages={validateMessages}>
                    <Form.Item
                        label='Khu vực'
                        name='areaId'
                        rules={[
                            {
                                required: true
                            }
                        ]}>
                        <Select style={{ width: 250 }}>
                            {props.areaList.map(item => (
                                <Select.Option value={item.id} key={item.id}>
                                    {item.name}
                                </Select.Option>
                            ))}
                        </Select>
                    </Form.Item>
                    <Form.Item
                        label='Tên bàn'
                        name='name'
                        rules={[
                            {
                                required: true
                            }
                        ]}>
                        <Input style={{ width: 250 }} />
                    </Form.Item>
                    <Form.Item
                        label='Mã bàn'
                        name='code'
                        rules={[
                            {
                                required: true
                            }
                        ]}>
                        <Input style={{ width: 250 }} />
                    </Form.Item>
                    <Form.Item label='Trạng thái' name='status'>
                        <Select style={{ width: 250 }}>
                            <Select.Option value={1}>Trống</Select.Option>
                            <Select.Option value={0}>Có khách</Select.Option>
                        </Select>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    )
}

export default EditArea
