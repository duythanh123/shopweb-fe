import React, { Fragment, useState, useEffect } from 'react'
import { Layout, Card, Button, Table, Row, Col, Input, Modal, Breadcrumb } from 'antd'
import '../../styles/persons.scss'
import { Language } from '../../models/language'
import { WareHouseModel } from '../../models/warehouse.model'
import warehouseService from '../../services/warehouse-service'
import { showNotification, UserInf } from '../../common/common'
import Edit from './edit'
import Add from './add'
import HeaderDom from '../../components/manager/header'
import { LoadingPage } from '../../jsx/loading'
import { getCookie } from '../../common/cookie'
import BreadcrumbItem from 'antd/lib/breadcrumb/BreadcrumbItem'

interface WareHouseProps {
    lang: Language
}
interface WareHouseState {
    warehouseList: WareHouseModel[]
    loading: boolean
    warehouse: WareHouseModel
    modalShowing?: string
    checkUpdate?: boolean
    disabled?: boolean
    loadTable: boolean
}

const WareHouseCategoryPage = (props: WareHouseProps) => {
    const { lang } = props
    const { Header, Content } = Layout
    const initalState: WareHouseState = {
        loading: true,
        warehouseList: [],
        modalShowing: '',
        checkUpdate: false,
        disabled: true,
        warehouse: null,
        loadTable: true
    }

    const [state, setState] = useState(initalState)
    const fetchData = async () => {
        try {
            const result = await warehouseService.getWareHouseList()
            setState(s => ({ ...s, warehouseList: result.result, loading: false, loadTable: false }))
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    useEffect(() => {
        if (state.loadTable) {
            fetchData()
        }
    }, [state])
    const closeModal = () => {
        setState(s => ({ ...s, modalShowing: '', disabled: true, checkUpdate: false }))
    }
    const showModal = (type: string, warehouse?: WareHouseModel) => {
        setState(s => ({ ...s, modalShowing: type, warehouse: warehouse }))
    }
    const isUpdate = () => {
        setState(s => ({ ...s, checkUpdate: true }))
    }
    const updateWareHouse = (warehouse: WareHouseModel) => {
        setState(s => ({ ...s, loadTable: true }))
    }
    const ableButton = () => {
        setState(s => ({ ...s, disabled: false }))
    }

    const deleteWareHouse = async (warehouse: WareHouseModel) => {
        try {
            const result = await warehouseService.deleteWareHouse(warehouse.id)
            if (result.status == 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.delete)
                setState(s => ({ ...s, modalShowing: '', loadTable: true }))
            } else {
                showNotification(lang.notification.error, result.data?.message)
            }
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    const onCreate = async (values: WareHouseModel) => {
        try {
            const result = await warehouseService.createWareHouse(values)
            if (result.status == 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.create)
                setState(s => ({ ...s, loadTable: true, modalShowing: '' }))
            } else {
                showNotification(lang.notification.error, result.data?.message)
            }
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    const columns = [
        {
            title: 'Tên NVL',
            dataIndex: 'name',
            key: 'name'
        },
        {
            title: 'Mã NVL',
            dataIndex: 'code',
            key: 'code'
        },
        {
            title: 'Mô tả NVL',
            dataIndex: 'description',
            key: 'description'
        },

        {
            title: 'Cập nhật',
            dataIndex: 'update',
            key: 'update',
            render: (_: string, record: WareHouseModel): JSX.Element => {
                return (
                    <div>
                        <Button type='primary' onClick={() => showModal('edit-warehouse', record)}>
                            Cập nhật
                        </Button>
                    </div>
                )
            }
        },
        {
            title: 'Xóa',
            dataIndex: 'delete',
            key: 'delete',
            render: (_: string, record: WareHouseModel): JSX.Element => {
                if (record.id === getCookie(UserInf).id)
                    return (
                        <Button type='primary' disabled>
                            Xóa
                        </Button>
                    )
                else
                    return (
                        <Button type='primary' onClick={() => showModal('delete-warehouse', record)}>
                            Xóa
                        </Button>
                    )
            }
        }
    ]
    return (
        <Fragment>
            <HeaderDom itemMenu={6} />
            <Content
                className='site-layout-background'
                style={{
                    padding: 24,
                    margin: 0,
                    minHeight: 280
                }}>
                <div>
                    {state.loading ? (
                        <LoadingPage />
                    ) : (
                        <Fragment>
                            <Breadcrumb>
                                <BreadcrumbItem>Quản lý kho</BreadcrumbItem>
                                <BreadcrumbItem>Danh sách nguyên vật liệu</BreadcrumbItem>
                            </Breadcrumb>
                            <br />
                            <div className='add-warehouses'>
                                <Row gutter={{ xs: 4, sm: 8, md: 12, lg: 16 }}>
                                    <Col className='gutter-row' xs={24} sm={24} md={24} lg={24}>
                                        <Button type='primary' onClick={() => showModal('add-warehouse')}>
                                            Thêm kho
                                        </Button>
                                    </Col>
                                </Row>
                            </div>
                            <div>
                                <Card
                                    title='Danh sách kho'
                                    headStyle={{ background: '#70bf70', fontSize: 20, fontWeight: 'bold' }}>
                                    <Table
                                        columns={columns}
                                        rowKey='id'
                                        dataSource={state.warehouseList}
                                        loading={state.loadTable}
                                        size='small'
                                    />
                                </Card>
                            </div>
                            <Modal
                                title='Cập nhật kho'
                                visible={state.modalShowing === 'edit-warehouse'}
                                destroyOnClose={true}
                                onCancel={() => closeModal()}
                                onOk={() => isUpdate()}
                                okText='Cập nhật'
                                cancelText='Hủy'
                                okButtonProps={{ disabled: state.disabled }}>
                                <Edit
                                    warehouse={state.warehouse}
                                    lang={props.lang}
                                    checkUpdate={state.checkUpdate}
                                    closeModal={closeModal}
                                    ableButton={ableButton}
                                    updateWareHouse={updateWareHouse}
                                />
                            </Modal>
                            <Add
                                lang={props.lang}
                                visible={state.modalShowing === 'add-warehouse'}
                                closeModal={closeModal}
                                onCreate={onCreate}
                            />
                            <Modal
                                title='Xóa kho'
                                visible={state.modalShowing === 'delete-warehouse'}
                                destroyOnClose={true}
                                onCancel={() => closeModal()}
                                onOk={() => deleteWareHouse(state.warehouse)}
                                okText='Xóa'
                                cancelText='Hủy'>
                                Xác kho này?
                            </Modal>
                        </Fragment>
                    )}
                </div>
            </Content>
        </Fragment>
    )
}

export default WareHouseCategoryPage
