import React, { Component, Fragment, useState, useEffect } from 'react'
import { Layout, Space, Card, Form, Input, DatePicker, Select, Switch, Row, Col, Button } from 'antd'
import { Language } from '../../models/language'
import { WareHouseModel } from '../../models/warehouse.model'
import warehouseService from '../../services/warehouse-service'
import { showNotification, getChangeItems, UserInf, formatMoment } from '../../common/common'
import moment from 'moment'
import userAction from '../../redux/actions/user'
import { useDispatch } from 'react-redux'

const { Header, Content } = Layout
const layout = {
    labelCol: {
        span: 8
    },
    wrapperCol: {
        span: 16
    }
}
const validateMessages = {
    required: '${label} không được trống!'
}
export interface WareHouseProps {
    lang?: Language
    warehouse?: WareHouseModel
    checkUpdate?: boolean
    closeModal?: Function
    ableButton?: Function
    updateWareHouse?: Function
}
interface WareHouseState {
    loading: boolean
    newWareHouse: WareHouseModel
}
const EditWareHouse = (props: WareHouseProps) => {
    const [form] = Form.useForm()
    const { lang } = props
    const initalState: WareHouseState = {
        loading: true,
        newWareHouse: null
    }
    const dispatch = useDispatch()
    const [state, setState] = useState(initalState)

    useEffect(() => {
        setState(s => ({ ...s, newWareHouse: Object.assign({}, props.warehouse), loading: false }))
    }, [])

    useEffect(() => {
        if (props.checkUpdate) {
            updateWareHouses()
        }
    }, [props.checkUpdate])

    const changeItem = (value: string | number, key: string) => {
        const { newWareHouse } = state
        newWareHouse[key] = value
        props.ableButton()
        setState(s => ({ ...s, newArea: Object.assign({}, newWareHouse) }))
    }

    const updateWareHouses = async () => {
        const { newWareHouse } = state
        const param: WareHouseModel = getChangeItems(props.warehouse, newWareHouse)

        try {
            const response = await warehouseService.editWareHouse(props.warehouse?.id, param)
            if (response.status == 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.edit)
                props.closeModal()
                props.updateWareHouse(response.data)
                dispatch({ type: userAction.updateUser, payload: response.data })
            } else {
                showNotification(lang.notification.error, response.data?.message)
            }
        } catch (error) {
            showNotification(lang.notification.error, lang.message_error.update_fail)
        }
    }

    const initForm = {
        name: props.warehouse.name,
        code: props.warehouse.code,
        description: props.warehouse.description
    }

    return (
        <Fragment>
            <Form {...layout} name='nest-messages' form={form} onFinish={updateWareHouses} initialValues={initForm}>
                <Form.Item
                    name='name'
                    label='Tên NVL'
                    rules={[
                        {
                            required: true
                        }
                    ]}>
                    <Input style={{ width: 250 }} onChange={e => changeItem(e.target.value, 'name')} />
                </Form.Item>
                <Form.Item
                    name='code'
                    label='Mã NVL'
                    rules={[
                        {
                            required: true
                        }
                    ]}>
                    <Input style={{ width: 250 }} onChange={e => changeItem(e.target.value, 'code')} />
                </Form.Item>

                <Form.Item
                    name='description'
                    label='Trạng thái'
                    rules={[
                        {
                            required: true
                        }
                    ]}>
                    <Input style={{ width: 250 }} onChange={e => changeItem(e.target.value, 'description')} />
                </Form.Item>
            </Form>
        </Fragment>
    )
}

export default EditWareHouse
