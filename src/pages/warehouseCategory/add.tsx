import React, { Component, Fragment, useState, useEffect } from 'react'
import { Layout, Space, Card, Form, Input, DatePicker, Select, Switch, Row, Col, Button, Modal } from 'antd'
import { Link } from 'react-router-dom'
import { Language } from '../../models/language'
import { WareHouseModel } from '../../models/warehouse.model'
import { getChangeItems, showNotification } from '../../common/common'
import warehouseService from '../../services/warehouse-service'

import { useDispatch } from 'react-redux'
const layout = {
    labelCol: {
        span: 8
    },
    wrapperCol: {
        span: 16
    }
}
const validateMessages = {
    required: '${label} không được trống!',
    types: {
        email: '${label} không phải là địa chỉ email!'
    }
}
export interface AddProps {
    lang?: Language
    visible?: boolean
    closeModal?: any
    onCreate?: Function
}

const AddWareHouse = (props: AddProps) => {
    const [form] = Form.useForm()
    const { lang } = props

    return (
        <Modal
            visible={props.visible}
            title='Thêm kho'
            destroyOnClose={true}
            okText='Thêm'
            cancelText='Hủy'
            onCancel={props.closeModal}
            onOk={() => {
                form.validateFields()
                    .then(values => {
                        form.resetFields()
                        props.onCreate(values)
                    })
                    .catch(info => {
                        console.log('Validate Failed:', info)
                    })
            }}>
            <Form
                {...layout}
                form={form}
                name='nest-add'
                validateMessages={validateMessages}
                initialValues={{ role: 2 }}>
                <Form.Item
                    name='name'
                    label='Tên NVL'
                    rules={[
                        {
                            required: true
                        }
                    ]}>
                    <Input style={{ width: 250 }} />
                </Form.Item>
                <Form.Item
                    name='code'
                    label='Mã NVL'
                    rules={[
                        {
                            required: true
                        }
                    ]}>
                    <Input style={{ width: 250 }} />
                </Form.Item>

                <Form.Item
                    label='Mô tả NVL'
                    name='description'
                    rules={[
                        {
                            required: true
                        }
                    ]}>
                    <Input style={{ width: 250 }} />
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default AddWareHouse
