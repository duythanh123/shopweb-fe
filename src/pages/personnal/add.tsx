import React, { useState, useEffect } from 'react'
import { Form, Input, DatePicker, Select, Modal } from 'antd'
import { Language } from '../../models/language'
import { roleModel } from '../../models/role.model'
import roleService from '../../services/role-service'
import { showNotification, UserInf } from '../../common/common'
import { getCookie } from '../../common/cookie'
const layout = {
    labelCol: {
        span: 8
    },
    wrapperCol: {
        span: 16
    }
}
const validateMessages = {
    required: '${label} không được trống!',
    types: {
        email: '${label} không phải là địa chỉ email!'
    }
}
export interface AddProps {
    lang?: Language
    visible?: boolean
    closeModal?: any
    onCreate?: Function
}
interface roleState {
    loading: boolean
    roleList: roleModel[]
}
const Addpersonnal = (props: AddProps) => {
    const [form] = Form.useForm()
    const initalState: roleState = {
        loading: true,
        roleList: []
    }
    const [state, setState] = useState(initalState)
    const fetchData = async () => {
        try {
            const roleList = await roleService.getRoleList()
            setState(s => ({ ...s, roleList: roleList, loading: false }))
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    useEffect(() => {
        if (state.loading) {
            fetchData()
        }
    }, [state])

    return (
        <Modal
            visible={props.visible}
            title='Thêm nhân viên'
            destroyOnClose={true}
            okText='Thêm'
            cancelText='Hủy'
            onCancel={props.closeModal}
            onOk={() => {
                form.validateFields()
                    .then(values => {
                        form.resetFields()
                        props.onCreate(values)
                    })
                    .catch(info => {
                        console.log('Validate Failed:', info)
                    })
            }}>
            <Form
                {...layout}
                form={form}
                name='nest-add'
                validateMessages={validateMessages}
                initialValues={{ role: 2 }}>
                <Form.Item
                    name='name'
                    label='Họ và tên'
                    rules={[
                        {
                            required: true
                        }
                    ]}>
                    <Input style={{ width: 250 }} />
                </Form.Item>
                <Form.Item label='Ngày sinh' name='birthday'>
                    <DatePicker format='DD/MM/YYYY' />
                </Form.Item>
                <Form.Item
                    name='email'
                    label='Email'
                    rules={[
                        {
                            required: true,
                            type: 'email'
                        }
                    ]}>
                    <Input style={{ width: 250 }} />
                </Form.Item>
                <Form.Item label='Số điện thoại' name='tel'>
                    <Input
                        style={{ width: 250 }}
                        maxLength={10}
                        onKeyPress={e => {
                            if (!/^[0-9]/.test(e.key)) event.preventDefault()
                        }}
                    />
                </Form.Item>
                <Form.Item label='Địa chỉ' name='address'>
                    <Input style={{ width: 250 }} />
                </Form.Item>
                <Form.Item
                    label='Tên đăng nhập'
                    name='userName'
                    rules={[
                        {
                            required: true
                        }
                    ]}>
                    <Input style={{ width: 250 }} />
                </Form.Item>
                <Form.Item
                    label='Mật khẩu'
                    name='password'
                    rules={[
                        {
                            required: true
                        }
                    ]}>
                    <Input.Password style={{ width: 250 }} />
                </Form.Item>
                <Form.Item label='Chức vụ' name='role'>
                    <Select style={{ width: 250 }}>
                        {state.roleList.map(item =>
                            item.level <= getCookie(UserInf).role ? (
                                <Select.Option value={item.level} key={item.level} disabled>
                                    {item.roleName}
                                </Select.Option>
                            ) : (
                                <Select.Option value={item.level} key={item.level}>
                                    {item.roleName}
                                </Select.Option>
                            )
                        )}
                    </Select>
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default Addpersonnal
