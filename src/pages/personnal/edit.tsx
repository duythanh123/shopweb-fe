import React, { Fragment, useState, useEffect } from 'react'
import { Layout, Form, Input, DatePicker, Select } from 'antd'
import { Language } from '../../models/language'
import { UserModel } from '../../models/user.model'
import userService from '../../services/user-service'
import { showNotification, getChangeItems, formatMoment, UserInf } from '../../common/common'
import moment from 'moment'
import userAction from '../../redux/actions/user'
import { useDispatch } from 'react-redux'
import { roleModel } from '../../models/role.model'
import roleService from '../../services/role-service'
import { getCookie } from '../../common/cookie'

const layout = {
    labelCol: {
        span: 8
    },
    wrapperCol: {
        span: 16
    }
}
export interface PersonnalProps {
    lang?: Language
    user?: UserModel
    checkUpdate?: boolean
    closeModal?: Function
    ableButton?: Function
    updateUser?: Function
}
interface PersonnalState {
    loading: boolean
    newProfile: UserModel
    roleList: roleModel[]
}
const EditPersonnal = (props: PersonnalProps) => {
    const [form] = Form.useForm()
    const { lang } = props
    const initalState: PersonnalState = {
        loading: true,
        newProfile: null,
        roleList: []
    }
    const dispatch = useDispatch()
    const [state, setState] = useState(initalState)
    const fetchData = async () => {
        try {
            const roleList = await roleService.getRoleList()
            setState(s => ({ ...s, roleList: roleList, newProfile: Object.assign({}, props.user), loading: false }))
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    useEffect(() => {
        fetchData()
    }, [])

    useEffect(() => {
        if (props.checkUpdate) {
            updateProfile()
        }
    }, [props.checkUpdate])

    const changeItem = (value: string | number, key: string) => {
        const { newProfile } = state
        newProfile[key] = value
        props.ableButton()
        setState(s => ({ ...s, newProfile: Object.assign({}, newProfile) }))
    }
    const updateProfile = async () => {
        const { newProfile } = state
        const param: UserModel = getChangeItems(props.user, newProfile)
        if (param.tel === '') param.tel = null
        if (!moment(param.birthday).isValid()) param.birthday = null
        try {
            const response = await userService.editUser(props.user?.id, param)
            if (response.status == 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.edit)
                props.closeModal()
                props.updateUser(response.data)
                dispatch({ type: userAction.updateUser, payload: response.data })
            } else {
                showNotification(lang.notification.error, response.data?.message)
            }
        } catch (error) {
            showNotification(lang.notification.error, lang.message_error.update_fail)
        }
    }
    const initForm = {
        name: props.user?.name,
        email: props.user?.email,
        birthday: props.user?.birthday ? moment(props.user?.birthday) : undefined,
        tel: props.user?.tel,
        address: props.user?.address,
        username: props.user?.userName,
        role: props.user?.role
    }

    const getSelectinValue = value => {
        changeItem(value, 'role')
    }

    return (
        <Fragment>
            <Form {...layout} name='nest-messages' form={form} onFinish={updateProfile} initialValues={initForm}>
                <Form.Item
                    name='name'
                    label='Họ tên'
                    rules={[
                        {
                            required: true
                        }
                    ]}>
                    <Input style={{ width: 250 }} onChange={e => changeItem(e.target.value, 'name')} />
                </Form.Item>
                <Form.Item label='Ngày sinh' name='birthday'>
                    <DatePicker onChange={date => changeItem(formatMoment(date), 'birthday')} format='DD/MM/YYYY' />
                </Form.Item>
                <Form.Item
                    name='email'
                    label='Email'
                    rules={[
                        {
                            required: true,
                            type: 'email'
                        }
                    ]}>
                    <Input style={{ width: 250 }} onChange={e => changeItem(e.target.value, 'email')} />
                </Form.Item>
                <Form.Item label='Số điện thoại' name='tel'>
                    <Input
                        style={{ width: 250 }}
                        onChange={e => changeItem(e.target.value, 'tel')}
                        maxLength={15}
                        onKeyPress={e => {
                            if (!/^[0-9]/.test(e.key)) event.preventDefault()
                        }}
                    />
                </Form.Item>
                <Form.Item label='Địa chỉ' name='address'>
                    <Input style={{ width: 250 }} onChange={e => changeItem(e.target.value, 'address')} />
                </Form.Item>
                <Form.Item label='Tên đăng nhập' name='username'>
                    <Input style={{ width: 250 }} disabled />
                </Form.Item>

                <Form.Item label='Chức vụ' name='role'>
                    <Select style={{ width: 250 }} onChange={getSelectinValue}>
                        {state.roleList?.map(item =>
                            item.level <= getCookie(UserInf).role ? (
                                <Select.Option value={item.level} key={item.level} disabled>
                                    {item.roleName}
                                </Select.Option>
                            ) : (
                                <Select.Option value={item.level} key={item.level}>
                                    {item.roleName}
                                </Select.Option>
                            )
                        )}
                    </Select>
                </Form.Item>
            </Form>
        </Fragment>
    )
}

export default EditPersonnal
