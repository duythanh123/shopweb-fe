import React, { useState, useEffect } from 'react'
import { Card, Form, Input, Button, Modal, InputNumber, Table } from 'antd'
import { Language } from '../../models/language'
import { showNotification, UserInf } from '../../common/common'
import { roleModel } from '../../models/role.model'
import roleService from '../../services/role-service'
import { DeleteOutlined, ExclamationCircleOutlined } from '@ant-design/icons'
import { getCookie } from '../../common/cookie'
const { confirm } = Modal
export interface RoleProps {
    lang?: Language
    visible?: boolean
    closeModal?: any
}
interface roleState {
    loading: boolean
    roleList: roleModel[]
    message: string
}
const Role = (props: RoleProps) => {
    const [form] = Form.useForm()
    const { lang } = props
    const initalState: roleState = {
        loading: true,
        roleList: [],
        message: 'Không được để trống'
    }
    const [state, setState] = useState(initalState)
    const fetchData = async () => {
        try {
            const roleList = await roleService.getRoleList()
            setState(s => ({ ...s, roleList: roleList, loading: false }))
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    useEffect(() => {
        if (state.loading) {
            fetchData()
        }
    }, [state])

    const colums = [
        {
            title: 'Chức vụ',
            dataIndex: 'roleName',
            key: 'roleName'
        },
        {
            title: 'level',
            dataIndex: 'level',
            key: 'level'
        },
        {
            title: 'Xóa',
            dataIndex: 'delete',
            key: 'delete',
            render: (_: string, record: roleModel): JSX.Element => {
                if (record.level <= getCookie(UserInf).role)
                    return <Button type='primary' icon={<DeleteOutlined />} disabled />
                else return <Button type='primary' icon={<DeleteOutlined />} onClick={() => Delete(record)} />
            }
        }
    ]
    const Delete = record => {
        confirm({
            title: 'Xóa chức vụ',
            icon: <ExclamationCircleOutlined />,
            content: 'Xác nhận xóa chức vụ này',
            okText: 'Xóa',
            cancelText: 'Hủy',
            async onOk() {
                try {
                    const result = await roleService.deleteRole(record.id)
                    if (result.status == 200) {
                        showNotification(lang.notification.success, '', lang.item.user, lang.action.delete)

                        setState(s => ({ ...s, loading: true }))
                    } else {
                        showNotification(lang.notification.error, result.data?.message)
                    }
                } catch (error) {
                    showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
                }
            },
            onCancel() {}
        })
    }
    const onFinish = async value => {
        let check1,
            check2: boolean = false
        state.roleList.map(item => {
            if (item.level === value.level) {
                check1 = true
            }
            if (item.roleName.toLocaleUpperCase() === value.roleName.toLocaleUpperCase()) {
                check2 = true
            }
        })
        if (check2) {
            Modal.error({
                title: 'Lỗi',
                content: 'Chức vụ đã tồn tại'
            })
        } else if (check1) {
            Modal.error({
                title: 'Lỗi',
                content: 'level đã tồn tại'
            })
        } else {
            try {
                const result = await roleService.createRole(value)
                if (result.status === 200) {
                    showNotification(lang.notification.success, '', lang.item.user, lang.action.create)
                    setState(s => ({ ...s, loading: true }))
                } else {
                    showNotification(lang.notification.error, result.data?.message)
                }
            } catch (error) {
                showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
            }
        }
    }

    return (
        <Modal
            visible={props.visible}
            title='Quản lý chúc vụ'
            destroyOnClose={true}
            onCancel={props.closeModal}
            footer={
                <Button key='back' onClick={props.closeModal}>
                    Đóng
                </Button>
            }>
            <Card bordered={false} style={{ height: 80 }}>
                <Form layout='inline' form={form} name='control-hooks' initialValues={null} onFinish={onFinish}>
                    <Form.Item
                        name='roleName'
                        rules={[
                            {
                                required: true,
                                message: state.message
                            }
                        ]}>
                        <Input placeholder='Tên chức vụ' />
                    </Form.Item>
                    <Form.Item name='level' style={{ width: 120 }} rules={[{ required: true, message: state.message }]}>
                        <InputNumber
                            placeholder='level'
                            min={1}
                            onKeyPress={e => {
                                if (!/^[0-9]/.test(e.key)) event.preventDefault()
                            }}
                        />
                    </Form.Item>
                    <Form.Item>
                        <Button type='primary' htmlType='submit'>
                            Thêm
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
            <br />
            <Table
                dataSource={state.roleList}
                rowKey='level'
                columns={colums}
                pagination={false}
                title={() => 'Danh sách chức vụ'}
                size='small'
                scroll={{ y: 350 }}
            />
        </Modal>
    )
}

export default Role
