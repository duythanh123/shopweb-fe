import React, { Fragment, useState, useEffect } from 'react'
import { Layout, Card, Button, Table, Row, Col, Input, Modal, Space, Breadcrumb } from 'antd'
import '../../styles/persons.scss'
import { Language } from '../../models/language'
import { UserModel } from '../../models/user.model'
import userService from '../../services/user-service'
import { showNotification, UserInf } from '../../common/common'
import Edit from './edit'
import ROLE from './role'

import Add from './add'
import HeaderDom from '../../components/manager/header'
import { LoadingPage } from '../../jsx/loading'
import { getCookie } from '../../common/cookie'
import { formatDate } from '../../common/date-util'
import { SearchOutlined, UserAddOutlined } from '@ant-design/icons'
import BreadcrumbItem from 'antd/lib/breadcrumb/BreadcrumbItem'
import { pagination } from '../../common/constant'
import roleService from '../../services/role-service'
import { roleModel } from '../../models/role.model'

interface PersonnalProps {
    lang: Language
}
interface PersonnalState {
    userList: UserModel[]
    loading: boolean
    user: UserModel
    roleList: roleModel[]
    modalShowing?: string
    checkUpdate?: boolean
    disabled?: boolean
    searchText?: string
    searchedColumn?: string
    current: number
    pageSize: number
    total: number
    loadTable: boolean
}

const PersonnalPage = (props: PersonnalProps) => {
    const { lang } = props
    const { Content } = Layout
    const initalState: PersonnalState = {
        loading: true,
        userList: [],
        roleList: [],
        modalShowing: '',
        checkUpdate: false,
        disabled: true,
        user: null,
        searchText: '',
        searchedColumn: '',
        current: pagination.current,
        pageSize: pagination.pageSize,
        total: pagination.total,
        loadTable: true
    }

    const [state, setState] = useState(initalState)
    const fetchData = async () => {
        if (state.searchText === '') {
            try {
                const roleList = await roleService.getRoleList()
                const data = await userService.getUserList(state.current - 1, state.pageSize)
                setState(s => ({
                    ...s,
                    userList: data?.result,
                    roleList: roleList,
                    loading: false,
                    loadTable: false,
                    total: data?.count
                }))
            } catch (error) {
                showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
            }
        } else {
            try {
                const result = await userService.getUserDetailWithName(
                    state.searchText,
                    state.current - 1,
                    state.pageSize
                )
                setState(s => ({ ...s, userList: result.result, total: result.count, loadTable: false }))
            } catch (error) {
                showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
            }
        }
    }
    useEffect(() => {
        if (state.loadTable) {
            fetchData()
        }
    }, [state])

    const handleChange = async pagination => {
        setState(s => ({
            ...s,
            current: pagination.current,
            pageSize: pagination.pageSize,
            total: pagination.total,
            loadTable: true
        }))
    }

    const closeModal = () => {
        setState(s => ({ ...s, modalShowing: '', disabled: true, checkUpdate: false }))
    }
    const showModal = (type: string, user?: UserModel) => {
        setState(s => ({ ...s, modalShowing: type, user: user }))
    }
    const isUpdate = () => {
        setState(s => ({ ...s, checkUpdate: true }))
    }
    const updateUser = (user: UserModel) => {
        let { userList } = state
        userList.forEach((item, key) => {
            if (item.id === user.id) {
                userList.splice(key, 1, user)
            }
        })
        userList = userList.slice()
        setState(s => ({ ...s, userList }))
    }
    const ableButton = () => {
        setState(s => ({ ...s, disabled: false }))
    }

    const deleteUser = async (user: UserModel) => {
        try {
            const result = await userService.deleteUser(user.id)
            if (result.status == 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.delete)
                setState(s => ({ ...s, modalShowing: '', loadTable: true }))
            } else {
                showNotification(lang.notification.error, result.data?.message)
            }
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    const onCreate = async (values: UserModel) => {
        try {
            const result = await userService.createUser(values)
            if (result.status === 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.create)
                setState(s => ({ ...s, loadTable: true, modalShowing: '' }))
            } else {
                showNotification(lang.notification.error, result.data?.message)
            }
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    //search
    const getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        this.searchInput = node
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Space>
                    <Button
                        type='primary'
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size='small'
                        style={{ width: 90 }}>
                        Search
                    </Button>
                    <Button onClick={() => handleReset(clearFilters)} size='small' style={{ width: 90 }}>
                        Reset
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) =>
            record[dataIndex]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select())
            }
        }
    })

    const handleSearch = async (selectedKeys, confirm, dataIndex) => {
        confirm()

        setState(s => ({
            ...s,
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
            current: pagination.current,
            pageSize: pagination.pageSize,
            loadTable: true
        }))
    }

    const handleReset = clearFilters => {
        clearFilters()
        setState(s => ({ ...s, searchText: '', loadTable: true }))
    }

    const columns = [
        {
            title: 'Họ và Tên',
            dataIndex: 'name',
            key: 'name',
            ...getColumnSearchProps('name')
        },
        {
            title: 'Ngày sinh',
            dataIndex: 'birthday',
            key: 'birthday',
            render: (_: string, record: UserModel): JSX.Element => {
                if (record.birthday !== null) return <div> {formatDate(record.birthday)}</div>
            }
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email'
        },
        {
            title: 'Điện thoại',
            dataIndex: 'tel',
            key: 'tel'
        },
        {
            title: 'Địa chỉ',
            dataIndex: 'address',
            key: 'address'
        },
        {
            title: 'Chức vụ',
            dataIndex: 'pos',
            key: 'pos',
            render: (_: string, record: UserModel): JSX.Element => {
                let roleName = ''
                state.roleList?.map(item => {
                    if (item.level === record.role) roleName = item.roleName
                })
                return <div>{roleName}</div>
            }
        },
        {
            title: 'Cập nhật',
            dataIndex: 'update',
            key: 'update',
            render: (_: string, record: UserModel): JSX.Element => {
                if (record.role <= getCookie(UserInf).role)
                    return (
                        <Button type='primary' disabled>
                            Cập nhật
                        </Button>
                    )
                else
                    return (
                        <div>
                            <Button type='primary' onClick={() => showModal('edit-user', record)}>
                                Cập nhật
                            </Button>
                        </div>
                    )
            }
        },
        {
            title: 'Xóa',
            dataIndex: 'delete',
            key: 'delete',
            render: (_: string, record: UserModel): JSX.Element => {
                if (record.role <= getCookie(UserInf).role)
                    return (
                        <Button type='primary' disabled>
                            Xóa
                        </Button>
                    )
                else
                    return (
                        <Button type='primary' onClick={() => showModal('delete-user', record)}>
                            Xóa
                        </Button>
                    )
            }
        }
    ]
    return (
        <Fragment>
            <HeaderDom itemMenu={2} />
            <Content
                className='site-layout-background'
                style={{
                    padding: 24,
                    margin: 0,
                    minHeight: 280
                }}>
                <div>
                    {state.loading ? (
                        <LoadingPage />
                    ) : (
                        <Fragment>
                            <div>
                                <Breadcrumb>
                                    <BreadcrumbItem>Quản lý nhân viên</BreadcrumbItem>
                                    <BreadcrumbItem>Danh sách nhân viên</BreadcrumbItem>
                                </Breadcrumb>
                                <Row gutter={{ xs: 4, sm: 8, md: 12, lg: 24 }}>
                                    <Col
                                        className='gutter-row'
                                        xs={24}
                                        sm={24}
                                        md={24}
                                        lg={24}
                                        style={{ textAlign: 'right' }}>
                                        <Button
                                            type='default'
                                            icon={<UserAddOutlined />}
                                            size='large'
                                            onClick={() => showModal('add-user')}>
                                            Thêm
                                        </Button>
                                        <Button
                                            type='default'
                                            icon={<UserAddOutlined />}
                                            size='large'
                                            onClick={() => showModal('role')}>
                                            Quản lý chức vụ
                                        </Button>
                                    </Col>
                                </Row>
                            </div>
                            <div>
                                <Card
                                    title='Danh sách nhân viên'
                                    headStyle={{ background: '#70bf70', fontSize: 16, fontWeight: 'bold' }}>
                                    <Table
                                        columns={columns}
                                        rowKey='id'
                                        dataSource={state.userList}
                                        pagination={{
                                            pageSize: state.pageSize,
                                            current: state.current,
                                            total: state.total
                                        }}
                                        loading={state.loadTable}
                                        onChange={handleChange}
                                        size='small'
                                    />
                                </Card>
                            </div>
                            <Modal
                                title='Cập nhật thông tin nhân viên'
                                visible={state.modalShowing === 'edit-user'}
                                destroyOnClose={true}
                                onCancel={() => closeModal()}
                                onOk={() => isUpdate()}
                                okText='Cập nhật'
                                cancelText='Hủy'
                                okButtonProps={{ disabled: state.disabled }}>
                                <Edit
                                    user={state.user}
                                    lang={props.lang}
                                    checkUpdate={state.checkUpdate}
                                    closeModal={closeModal}
                                    ableButton={ableButton}
                                    updateUser={updateUser}
                                />
                            </Modal>
                            <Add
                                lang={props.lang}
                                visible={state.modalShowing === 'add-user'}
                                closeModal={closeModal}
                                onCreate={onCreate}
                            />
                            <ROLE lang={props.lang} visible={state.modalShowing === 'role'} closeModal={closeModal} />
                            <Modal
                                title='Xóa nhân viên'
                                visible={state.modalShowing === 'delete-user'}
                                destroyOnClose={true}
                                onCancel={() => closeModal()}
                                onOk={() => deleteUser(state.user)}
                                okText='Xóa'
                                cancelText='Hủy'>
                                Xác nhận xóa tài khoản này?
                            </Modal>
                        </Fragment>
                    )}
                </div>
            </Content>
        </Fragment>
    )
}

export default PersonnalPage
