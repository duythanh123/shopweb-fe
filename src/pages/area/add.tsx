import React from 'react'
import { Form, Input, Modal } from 'antd'
import { Language } from '../../models/language'

const layout = {
    labelCol: {
        span: 8
    },
    wrapperCol: {
        span: 16
    }
}
const validateMessages = {
    required: '${label} không được trống!',
    types: {
        email: '${label} không phải là địa chỉ email!'
    }
}
export interface AddProps {
    lang?: Language
    visible?: boolean
    closeModal?: any
    onCreate?: Function
}

const AddArea = (props: AddProps) => {
    const [form] = Form.useForm()

    return (
        <Modal
            visible={props.visible}
            title='Thêm khu vực'
            destroyOnClose={true}
            okText='Thêm'
            cancelText='Hủy'
            onCancel={props.closeModal}
            onOk={() => {
                form.validateFields()
                    .then(values => {
                        form.resetFields()
                        props.onCreate(values)
                    })
                    .catch(info => {
                        console.log('Validate Failed:', info)
                    })
            }}>
            <Form {...layout} form={form} name='nest-add' validateMessages={validateMessages}>
                <Form.Item
                    name='name'
                    label='Khu vực'
                    rules={[
                        {
                            required: true
                        }
                    ]}>
                    <Input style={{ width: 250 }} />
                </Form.Item>

                <Form.Item label='Mô tả' name='status'>
                    <Input style={{ width: 250 }} />
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default AddArea
