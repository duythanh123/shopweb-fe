import React, { Fragment, useState, useEffect } from 'react'
import { Layout, Card, Button, Table, Row, Col, Input, Modal, Breadcrumb } from 'antd'
import '../../styles/persons.scss'
import { Language } from '../../models/language'
import { AreaModel } from '../../models/area.model'
import areaService from '../../services/area-service'
import { showNotification, UserInf } from '../../common/common'
import Edit from './edit'
import Add from './add'
import HeaderDom from '../../components/manager/header'
import { LoadingPage } from '../../jsx/loading'
import { getCookie } from '../../common/cookie'
import BreadcrumbItem from 'antd/lib/breadcrumb/BreadcrumbItem'

interface AreaProps {
    lang: Language
}
interface AreaState {
    areaList: AreaModel[]
    loading: boolean
    area: AreaModel
    modalShowing?: string
    checkUpdate?: boolean
    disabled?: boolean
    loadTable: boolean
}

const AreaPage = (props: AreaProps) => {
    const { lang } = props
    const { Content } = Layout
    const initalState: AreaState = {
        loading: true,
        areaList: [],
        modalShowing: '',
        checkUpdate: false,
        disabled: true,
        area: null,
        loadTable: true
    }

    const [state, setState] = useState(initalState)
    const fetchData = async () => {
        let areaList: AreaModel[] = []
        try {
            areaList = await areaService.getAreaList()
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
        setState(s => ({ ...s, areaList, loading: false, loadTable: false }))
    }
    useEffect(() => {
        if (state.loadTable) {
            fetchData()
        }
    }, [state])
    const closeModal = () => {
        setState(s => ({ ...s, modalShowing: '', disabled: true, checkUpdate: false }))
    }
    const showModal = (type: string, area?: AreaModel) => {
        setState(s => ({ ...s, modalShowing: type, area: area }))
    }
    const isUpdate = () => {
        setState(s => ({ ...s, checkUpdate: true }))
    }
    const updateArea = () => {
        setState(s => ({ ...s, loadTable: true }))
    }
    const ableButton = () => {
        setState(s => ({ ...s, disabled: false }))
    }

    const deleteArea = async (area: AreaModel) => {
        try {
            const result = await areaService.deleteArea(area.id)
            if (result.status === 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.delete)
                setState(s => ({ ...s, modalShowing: '', loadTable: true }))
            } else {
                showNotification(lang.notification.error, result.data?.message)
            }
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    const onCreate = async (values: AreaModel) => {
        try {
            const result = await areaService.createArea(values)
            if (result.status == 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.create)
                setState(s => ({ ...s, loadTable: true, modalShowing: '' }))
            } else {
                showNotification(lang.notification.error, result.data?.message)
            }
        } catch (error) {
            showNotification(props.lang.notification.error, props.lang.item.data, props.lang.action.load)
        }
    }
    const columns = [
        {
            title: 'Khu vực',
            dataIndex: 'name',
            key: 'name'
        },
        {
            title: 'Mô tả',
            dataIndex: 'status',
            key: 'status'
        },

        {
            title: 'Cập nhật',
            dataIndex: 'update',
            key: 'update',
            render: (_: string, record: AreaModel): JSX.Element => {
                return (
                    <div>
                        <Button type='primary' onClick={() => showModal('edit-area', record)}>
                            Cập nhật
                        </Button>
                    </div>
                )
            }
        },
        {
            title: 'Xóa',
            dataIndex: 'delete',
            key: 'delete',
            render: (_: string, record: AreaModel): JSX.Element => {
                if (record.id === getCookie(UserInf).id)
                    return (
                        <Button type='primary' disabled>
                            Xóa
                        </Button>
                    )
                else
                    return (
                        <Button type='primary' onClick={() => showModal('delete-area', record)}>
                            Xóa
                        </Button>
                    )
            }
        }
    ]
    return (
        <Fragment>
            <HeaderDom itemMenu={5} />
            <Content
                className='site-layout-background'
                style={{
                    padding: 24,
                    margin: 0,
                    minHeight: 280
                }}>
                <div>
                    {state.loading ? (
                        <LoadingPage />
                    ) : (
                        <Fragment>
                            <div>
                                <Breadcrumb>
                                    <BreadcrumbItem>Quản lý khu vực</BreadcrumbItem>
                                    <BreadcrumbItem>Khu vực</BreadcrumbItem>
                                </Breadcrumb>
                                <Row gutter={{ xs: 4, sm: 8, md: 12, lg: 16 }}>
                                    <Col
                                        className='gutter-row'
                                        xs={24}
                                        sm={24}
                                        md={24}
                                        lg={24}
                                        style={{ textAlign: 'right' }}>
                                        <Button type='primary' onClick={() => showModal('add-area')}>
                                            Thêm khu vực
                                        </Button>
                                    </Col>
                                </Row>
                            </div>
                            <div>
                                <Card
                                    title='Danh khu vực'
                                    headStyle={{ background: '#70bf70', fontSize: 20, fontWeight: 'bold' }}>
                                    <Table
                                        columns={columns}
                                        rowKey='id'
                                        dataSource={state.areaList}
                                        loading={state.loadTable}
                                        size='small'
                                        pagination={false}
                                    />
                                </Card>
                            </div>
                            <Modal
                                title='Cập nhật khu vực'
                                visible={state.modalShowing === 'edit-area'}
                                destroyOnClose={true}
                                onCancel={() => closeModal()}
                                onOk={() => isUpdate()}
                                okText='Cập nhật'
                                cancelText='Hủy'
                                okButtonProps={{ disabled: state.disabled }}>
                                <Edit
                                    area={state.area}
                                    lang={props.lang}
                                    checkUpdate={state.checkUpdate}
                                    closeModal={closeModal}
                                    ableButton={ableButton}
                                    updateArea={updateArea}
                                />
                            </Modal>
                            <Add
                                lang={props.lang}
                                visible={state.modalShowing === 'add-area'}
                                closeModal={closeModal}
                                onCreate={onCreate}
                            />
                            <Modal
                                title='Xóa khu vực'
                                visible={state.modalShowing === 'delete-area'}
                                destroyOnClose={true}
                                onCancel={() => closeModal()}
                                onOk={() => deleteArea(state.area)}
                                okText='Xóa'
                                cancelText='Hủy'>
                                Xác khu vực này?
                            </Modal>
                        </Fragment>
                    )}
                </div>
            </Content>
        </Fragment>
    )
}

export default AreaPage
