import React, { Fragment, useState, useEffect } from 'react'
import { Layout, Form, Input } from 'antd'
import { Language } from '../../models/language'
import { AreaModel } from '../../models/area.model'
import areaService from '../../services/area-service'
import { showNotification, getChangeItems } from '../../common/common'
import userAction from '../../redux/actions/user'
import { useDispatch } from 'react-redux'

const layout = {
    labelCol: {
        span: 8
    },
    wrapperCol: {
        span: 16
    }
}
export interface AreaProps {
    lang?: Language
    area?: AreaModel
    checkUpdate?: boolean
    closeModal?: Function
    ableButton?: Function
    updateArea?: Function
}
interface AreaState {
    loading: boolean
    newArea: AreaModel
}
const EditArea = (props: AreaProps) => {
    const [form] = Form.useForm()
    const { lang } = props
    const initalState: AreaState = {
        loading: true,
        newArea: null
    }
    const dispatch = useDispatch()
    const [state, setState] = useState(initalState)

    useEffect(() => {
        setState(s => ({ ...s, newArea: Object.assign({}, props.area), loading: false }))
    }, [])

    useEffect(() => {
        if (props.checkUpdate) {
            updateArea()
        }
    }, [props.checkUpdate])

    const changeItem = (value: string | number, key: string) => {
        const { newArea } = state
        newArea[key] = value
        props.ableButton()
        setState(s => ({ ...s, newArea: Object.assign({}, newArea) }))
    }

    const updateArea = async () => {
        const { newArea } = state
        const param: AreaModel = getChangeItems(props.area, newArea)

        try {
            const response = await areaService.editArea(props.area?.id, param)
            if (response.status == 200) {
                showNotification(lang.notification.success, '', lang.item.user, lang.action.edit)
                props.closeModal()
                props.updateArea(response.data)
                dispatch({ type: userAction.updateUser, payload: response.data })
            } else {
                showNotification(lang.notification.error, response.data?.message)
            }
        } catch (error) {
            showNotification(lang.notification.error, lang.message_error.update_fail)
        }
    }

    const initForm = {
        name: props.area.name,
        status: props.area.status
    }

    return (
        <Fragment>
            <Form {...layout} name='nest-messages' form={form} onFinish={updateArea} initialValues={initForm}>
                <Form.Item
                    name='name'
                    label='Khu vực'
                    rules={[
                        {
                            required: true
                        }
                    ]}>
                    <Input style={{ width: 250 }} onChange={e => changeItem(e.target.value, 'name')} />
                </Form.Item>

                <Form.Item name='status' label='Mô tả'>
                    <Input style={{ width: 250 }} onChange={e => changeItem(e.target.value, 'status')} />
                </Form.Item>
            </Form>
        </Fragment>
    )
}

export default EditArea
