export interface CategoryModel {
    id?: string
    name?: string
    status?: string
}
