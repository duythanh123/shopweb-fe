export interface AreaModel {
    id?: string
    name?: string
    status?: string
}
