export interface WareHouseModel {
    id?: string
    name?: string
    code?: string
    description?: string
    price?: number
}
