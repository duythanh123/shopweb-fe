import { WareHouseModel } from './warehouse.model'

export interface WareHouseInOutModel {
    id?: string
    name?: string
    code?: string
    description?: string
    type?: number
    price?: number
    supplier?: string
    items?: [
        {
            id?: string
            name?: string
            description?: string
            code?: string
            price?: number
            amount?: number
        }
    ]
    createDate?: string
}
