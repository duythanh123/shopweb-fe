import { TableModel } from './table.model'
import { AreaModel } from './area.model'
import { productModel } from './product.model'
export interface billModel {
    id?: string
    totalPrice?: number
    tableId?: string
    table?: TableModel
    createDate?: string
    area?: AreaModel
    product?: productModel
}
