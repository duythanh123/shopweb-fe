export interface Menu {
    user: string
    account: string
    sign_out: string
}

export interface Button {
    close: string
    next: string
    login: string
    back: string
    cancel: string
    update: string
    submit: string
    search: string
    previous: string
    register: string
    ok: string
    delete: string
    edit: string
    reset: string
    add: string
    create: string
    save: string
}

export interface Symboll {
    no: string
}

export interface Notification {
    success: string
    error: string
}

export interface Login {
    user_name: string
    pass_word: string
}

export interface Header {
    logout: string
}

export interface Item {
    data: string
    user: string
    store: string
}

export interface Action {
    add: string
    delete: string
    edit: string
    load: string
    create: string
    remove: string
    leave_group: string
}

export interface User {
    title_list: string
    title_profile_detail: string
    title_profile_edit: string
    name: string
    user_name: string
    email: string
    birthday: string
    tel: string
    address: string
    create_date: string
    role: string
}
export interface Store {
    name: string
    tel: string
    email: string
    create_date: string
    address: string
    description: string
    avatar: string
    timeOpen: string
    timeClose: string
}

export interface TabNotification {
    title: string
    describe: string
}

export interface TabContact {
    title: string
    describe: string
}

export interface MessageError {
    upload_fail: string
    add_fail: string
    update_fail: string
    delete_fail: string
    email: string
    tel: string
    file: string
    name_project: string
    error_role: string
}

export interface Footer {
    copy_right: string
}
export interface Language {
    menu: Menu
    button: Button
    symbol: Symboll
    notification: Notification
    login: Login
    header: Header
    action: Action
    user: User
    message_error: MessageError
    footer: Footer
    store: Store
    item: Item
}
