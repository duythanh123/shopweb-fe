export interface StoreModel {
    id?: string
    name?: string
    tel?: string
    address?: string
    description?: string
    timeOpen?: number
    timeClose?: number
    avatar?: string
}
