import { AreaModel } from './area.model'
import { cartModel } from './cart.model'

export interface TableModel {
    id?: string
    name?: string
    status?: boolean
    code?: string
    area?: AreaModel
    carts?: cartModel[]
}
