export interface productModel {
    id?: string
    name?: string
    picture?: string
    categoryId?: string
    price?: number
    description?: string
}
