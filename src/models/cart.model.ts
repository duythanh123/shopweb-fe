import { productModel } from './product.model'

export interface cartModel {
    product?: productModel
    amount?: number
    note?: string
    id?: string
    productId?: string
}
