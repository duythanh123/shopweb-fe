export interface UserModel {
    id?: string
    name?: string
    userName?: string
    password?: string
    role?: number
    tel?: string
    email?: string
    birthday?: string
    address?: string
}
