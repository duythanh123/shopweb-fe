import apiHandler from '../common/api-handler'
import apiConstant from '../configurations/api-constants'
const idExpr = '{id}'
const BillNameExpr = '{Bill-name}'
const pageExpr = '{page}'
const limitExpr = '{limit}'

const BillService = {
    /**
     * Get Bill List
     */
    getBillList: async (page?: number, limit?: number) => {
        const url = apiConstant.BillURI.replace(pageExpr, page?.toString()).replace(limitExpr, limit?.toString())
        const resp = await apiHandler.get(url)
        return resp.data
    },
    /**
     * Get Bill List by date
     */
    getBillListByDate: async (date: number[], page?: number, limit?: number) => {
        const url = apiConstant.BillByDateURI.replace('{date}', `${date[0]}-${date[1]}`)
            .replace(pageExpr, page?.toString())
            .replace(limitExpr, limit?.toString())
        const resp = await apiHandler.get(url)
        return resp.data
    },
    /**
     * Get user List
     */
    getContactList: async () => {
        const resp = await apiHandler.get(apiConstant.ContactURI)
        return resp.data
    },
    /**
     * Get user Detail
     * @param {*} userId
     */

    /**
     * Get Issue Detail with userName
     * @param {*} name
     */

    /**
     * Delete table
     * @param {*} userId
     */
    deleteBill: async billId => {
        const url = apiConstant.BillDetailURI.replace(idExpr, billId)
        const resp = await apiHandler.delete(url)
        return resp
    },
    /**
     * Edit Bill
     * @param {*} BillId
     */
    editBill: async (BillId: string, body: any) => {
        const url = apiConstant.BillDetailURI.replace(idExpr, BillId)
        const resp = await apiHandler.update(url, body)
        return resp
    },
    /**
     * Create Bill
     * @param {*} body
     *
     */
    createBill: async body => {
        const url = apiConstant.BillCreateURI
        const resp = await apiHandler.create(url, body)
        return resp
    }
    /**
     * Check BillName exist or not
     * @param {*} BillName
     *
     */
}

export default BillService
