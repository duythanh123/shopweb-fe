import apiHandler from '../common/api-handler'
import apiConstant from '../configurations/api-constants'
const userIdExpr = '{id}'
const userNameExpr = '{user-name}'
const pageExpr = '{page}'
const limitExpr = '{limit}'

const warehouseInOutService = {
    /**
     * Get user List
     */
    getWareHouseInOutList: async (page?: number, limit?: number) => {
        const url = apiConstant.WareHouseInOutURI.replace(pageExpr, page?.toString()).replace(
            limitExpr,
            limit?.toString()
        )
        const resp = await apiHandler.get(url)
        return resp.data
    },
    getWarehouseListByDate: async (date: number[], page?: number, limit?: number) => {
        const url = apiConstant.WareHouseInOutByDateURI.replace('{date}', `${date[0]}-${date[1]}`)
            .replace(pageExpr, page?.toString())
            .replace(limitExpr, limit?.toString())
        const resp = await apiHandler.get(url)
        return resp.data
    },
    /**
     * Delete User
     * @param {*} warehouseId
     */
    deleteWareHouse: async warehouseId => {
        const url = apiConstant.WareHouseInOutDeleteURI.replace(userIdExpr, warehouseId)
        const resp = await apiHandler.delete(url)
        return resp
    },
    /**
     * Edit User
     * @param {*} warehouseId
     */
    editWareHouse: async (warehouseId: string, body: any) => {
        const url = apiConstant.WareHouseDetailURI.replace(userIdExpr, warehouseId)
        const resp = await apiHandler.update(url, body)
        return resp
    },
    /**
     * Create user
     * @param {*} body
     *
     */
    createWareHouseInOut: async body => {
        const url = apiConstant.WareHouseInOutCreateURI
        const resp = await apiHandler.create(url, body)
        return resp
    }
    /**
     * Check userName exist or not
     * @param {*} userName
     *
     */
}

export default warehouseInOutService
