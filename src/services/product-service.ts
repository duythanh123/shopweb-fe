import apiHandler from '../common/api-handler'
import apiConstant from '../configurations/api-constants'
const productIdExpr = '{id}'
const productNameExpr = '{name}'
const limitExpr = '{limit}'
const pageExpr = '{page}'
const areaIdExpr = '{areaid}'

const productService = {
    /**
     * Get product List
     */
    getProductList: async (page?: number, limit?: number) => {
        const url = apiConstant.ProductURI.replace(pageExpr, page?.toString()).replace(limitExpr, limit?.toString())
        const resp = await apiHandler.get(url)
        return resp.data
    },
    /**
     * Get Product Detail with name
     * @param {*} name
     */
    getProductDetailWithName: async (name, page?: number, limit?: number) => {
        const url = apiConstant.ProductWithNameURI.replace(productNameExpr, name).replace(limitExpr, limit?.toString())
        const resp = await apiHandler.get(url.replace(pageExpr, page?.toString()))
        return resp.data
    },
    getProductDetailWithArea: async (areaId, page?: number, limit?: number) => {
        const url = apiConstant.ProductWithAreaURI.replace(areaIdExpr, areaId).replace(limitExpr, limit?.toString())
        const resp = await apiHandler.get(url.replace(pageExpr, page?.toString()))
        return resp.data
    },
    /**
     * Delete product
     * @param {*} productId
     */
    deleteProduct: async productId => {
        const url = apiConstant.ProductDetailURI.replace(productIdExpr, productId)
        const resp = await apiHandler.delete(url)
        return resp
    },
    /**
     * Edit product
     * @param {*} productId
     */
    editProduct: async (productId: string, body: any) => {
        const url = apiConstant.ProductDetailURI.replace(productIdExpr, productId)
        const resp = await apiHandler.update(url, body)
        return resp
    },
    /**
     * Create product
     * @param {*} body
     *
     */
    createProduct: async body => {
        const url = apiConstant.ProductCreateURI
        const resp = await apiHandler.create(url, body)
        return resp
    }
}

export default productService
