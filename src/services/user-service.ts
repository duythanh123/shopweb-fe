import apiHandler from '../common/api-handler'
import apiConstant from '../configurations/api-constants'
const userIdExpr = '{id}'
const userNameExpr = '{name}'
export const pageExpr = '{page}'
export const limitExpr = '{limit}'

const userService = {
    /**
     * Get user List
     */
    getUserList: async (page?: number, limit?: number) => {
        const url = apiConstant.UsersURI.replace(pageExpr, page?.toString()).replace(limitExpr, limit?.toString())
        const resp = await apiHandler.get(url)
        return resp.data
    },
    /**
     * Get user List
     */
    getContactList: async () => {
        const resp = await apiHandler.get(apiConstant.ContactURI)
        return resp.data
    },
    /**
     * Get user Detail
     * @param {*} userId
     */
    getUserDetail: async userId => {
        const url = apiConstant.UserDetailURI.replace(userIdExpr, userId)
        const resp = await apiHandler.get(url)
        return resp.data
    },
    /**
     * Get Issue Detail with userName
     * @param {*} name
     */
    getUserDetailWithName: async (name, page?: number, limit?: number) => {
        const url = apiConstant.UserDetailWithNameURI.replace(userNameExpr, name).replace(limitExpr, limit?.toString())
        const resp = await apiHandler.get(url.replace(pageExpr, page?.toString()))
        return resp.data
    },
    /**
     * Delete User
     * @param {*} userId
     */
    deleteUser: async userId => {
        const url = apiConstant.UserDetailURI.replace(userIdExpr, userId)
        const resp = await apiHandler.delete(url)
        return resp
    },
    /**
     * Edit User
     * @param {*} userId
     */
    editUser: async (userId: string, body: any) => {
        const url = apiConstant.UserDetailURI.replace(userIdExpr, userId)
        const resp = await apiHandler.update(url, body)
        return resp
    },
    /**
     * Create user
     * @param {*} body
     *
     */
    createUser: async body => {
        const url = apiConstant.UserCreateURI
        const resp = await apiHandler.create(url, body)
        return resp
    },
    /**
     * Check userName exist or not
     * @param {*} userName
     *
     */
    checkUserName: async (userName: string) => {
        const url = apiConstant.UserCheckExistURI.replace('{userName}', userName)
        const resp = await apiHandler.get(url)
        return resp
    }
}

export default userService
