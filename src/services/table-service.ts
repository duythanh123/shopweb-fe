import apiHandler from '../common/api-handler'
import apiConstant from '../configurations/api-constants'
const idExpr = '{id}'
const userNameExpr = '{user-name}'

const tableService = {
    /**
     * Get user List
     */
    getTableList: async () => {
        const resp = await apiHandler.get(apiConstant.TableURI)
        return resp.data
    },
    /**
     * Get user Detail
     * @param {*} tableId
     */
    getTableDetail: async tableId => {
        const url = apiConstant.TableDetailURI.replace(idExpr, tableId)
        const resp = await apiHandler.get(url)
        return resp.data
    },
    /**
     * Get user List
     *  @param {*} areaId
     */
    getTableByIdArea: async (areaId: string) => {
        const url = apiConstant.TableSearchURI.replace(idExpr, areaId)
        const resp = await apiHandler.get(url)
        return resp.data
    },

    /**
     * Delete User
     * @param {*} tableId
     */
    deleteTable: async tableId => {
        const url = apiConstant.TableDetailURI.replace(idExpr, tableId)
        const resp = await apiHandler.delete(url)
        return resp
    },
    /**
     * Edit User
     * @param {*} tableId
     */
    editTable: async (tableId: string, body: any) => {
        const url = apiConstant.TableDetailURI.replace(idExpr, tableId)
        const resp = await apiHandler.update(url, body)
        return resp
    },
    /**
     * Create user
     * @param {*} body
     *
     */
    createTable: async body => {
        const url = apiConstant.TableCreateURI
        const resp = await apiHandler.create(url, body)
        return resp
    }
    /**
     * Check userName exist or not
     * @param {*} userName
     *
     */
}

export default tableService
