import apiHandler from '../common/api-handler'
import apiConstant from '../configurations/api-constants'
const userIdExpr = '{id}'
const userNameExpr = '{user-name}'

const storeService = {
    /**
     * Get user List
     */
    getStoreDetail: async () => {
        const resp = await apiHandler.get(apiConstant.StoreURI)
        return resp.data
    },

    /**
     * Edit User
     * @param {*} userId
     */
    editStore: async (userId: string, body: any) => {
        const url = apiConstant.StoreDetailURI.replace(userIdExpr, userId)
        const resp = await apiHandler.update(url, body)
        return resp
    }
}

export default storeService
