import apiHandler from '../common/api-handler'
import apiConstant from '../configurations/api-constants'
const roleIdExpr = '{id}'

const roleService = {
    /**
     * Get role List
     */
    getRoleList: async () => {
        const resp = await apiHandler.get(apiConstant.RoleURI)
        return resp.data
    },

    /**
     * Delete role
     * @param {*} roleId
     */
    deleteRole: async roleId => {
        const url = apiConstant.RoleDetailURI.replace(roleIdExpr, roleId)
        const resp = await apiHandler.delete(url)
        return resp
    },
    /**
     * Create role
     * @param {*} body
     *
     */
    createRole: async body => {
        const url = apiConstant.RoleCreateURI
        const resp = await apiHandler.create(url, body)
        return resp
    }
}

export default roleService
