import apiHandler from '../common/api-handler'
import apiConstant from '../configurations/api-constants'

const loginService = {
    /**
     *
     * @param {*} params
     */
    login: async (params: any) => {
        const resp = await apiHandler.login(apiConstant.LoginURI, params)
        return resp
    },

    /**
     * @param {*} params
     */
    changePassword: async (params: any) => {
        const resp = await apiHandler.update(apiConstant.ChangePasswordURI, params)
        return resp
    }
}

export default loginService
