import apiHandler from '../common/api-handler'
import apiConstant from '../configurations/api-constants'
const userIdExpr = '{id}'
const userNameExpr = '{user-name}'

const categoryService = {
    /**
     * Get user List
     */
    getCategoryList: async () => {
        const resp = await apiHandler.get(apiConstant.CategoryURI)
        return resp.data
    },

    /**
     * Delete User
     * @param {*} userId
     */
    deleteCat: async caId => {
        const url = apiConstant.CaDetailURI.replace(userIdExpr, caId)
        const resp = await apiHandler.delete(url)
        return resp
    },
    /**
     * Edit User
     * @param {*} userId
     */
    editCat: async (userId: string, body: any) => {
        const url = apiConstant.CaDetailURI.replace(userIdExpr, userId)
        const resp = await apiHandler.update(url, body)
        return resp
    },
    /**
     * Create user
     * @param {*} body
     *
     */
    createCat: async body => {
        const url = apiConstant.CaCreateURI
        const resp = await apiHandler.create(url, body)
        return resp
    }
    /**
     * Check userName exist or not
     * @param {*} userName
     *
     */
}

export default categoryService
