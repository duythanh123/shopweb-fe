import apiHandler from '../common/api-handler'
import apiConstant from '../configurations/api-constants'
const userIdExpr = '{id}'
const userNameExpr = '{user-name}'

const warehouseService = {
    /**
     * Get user List
     */
    getWareHouseList: async () => {
        const resp = await apiHandler.get(apiConstant.WareHouseURI)
        return resp.data
    },

    /**
     * Delete User
     * @param {*} warehouseId
     */
    deleteWareHouse: async warehouseId => {
        const url = apiConstant.WareHouseDetailURI.replace(userIdExpr, warehouseId)
        const resp = await apiHandler.delete(url)
        return resp
    },
    /**
     * Edit User
     * @param {*} warehouseId
     */
    editWareHouse: async (warehouseId: string, body: any) => {
        const url = apiConstant.WareHouseDetailURI.replace(userIdExpr, warehouseId)
        const resp = await apiHandler.update(url, body)
        return resp
    },
    /**
     * Create user
     * @param {*} body
     *
     */
    createWareHouse: async body => {
        const url = apiConstant.WareHouseCreateURI
        const resp = await apiHandler.create(url, body)
        return resp
    }
    /**
     * Check userName exist or not
     * @param {*} userName
     *
     */
}

export default warehouseService
