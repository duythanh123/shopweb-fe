import apiHandler from '../common/api-handler'
import apiConstant from '../configurations/api-constants'
const userIdExpr = '{id}'
const userNameExpr = '{user-name}'

const areaService = {
    /**
     * Get user List
     */
    getAreaList: async () => {
        const resp = await apiHandler.get(apiConstant.AreaURI)
        return resp.data
    },

    /**
     * Delete User
     * @param {*} userId
     */
    deleteArea: async areaId => {
        const url = apiConstant.AreaDeleteURI.replace(userIdExpr, areaId)
        const resp = await apiHandler.delete(url)
        return resp
    },
    /**
     * Edit User
     * @param {*} userId
     */
    editArea: async (areaId: string, body: any) => {
        const url = apiConstant.AreaDetailURI.replace(userIdExpr, areaId)
        const resp = await apiHandler.update(url, body)
        return resp
    },
    /**
     * Create user
     * @param {*} body
     *
     */
    createArea: async body => {
        const url = apiConstant.AreaCreateURI
        const resp = await apiHandler.create(url, body)
        return resp
    }
    /**
     * Check userName exist or not
     * @param {*} userName
     *
     */
}

export default areaService
