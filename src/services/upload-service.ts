import apiHandler from '../common/api-handler'
import apiConstant from '../configurations/api-constants'

const uploadService = {
    uploadFile: async (e: any) => {
        const formData = new FormData()
        formData.append('file', e)
        const url = apiConstant.UploadFileURI
        const resp = await apiHandler.create(url, formData)
        return resp
    }
}

export default uploadService
