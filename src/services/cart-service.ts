import apiHandler from '../common/api-handler'
import apiConstant from '../configurations/api-constants'
const idExpr = '{id}'
const cardIdExpr = '{id}'
const idTable = '{tableId}'
const CartNameExpr = '{Cart-name}'

const CartService = {
    /**
     * Get Cart List
     */
    getCartList: async () => {
        const resp = await apiHandler.get(apiConstant.CartURI)
        return resp.data
    },

    /**
     * Delete Cart
     * @param {*} CartId
     */
    deleteCart: async (tableId: string, CartId: string) => {
        const url = apiConstant.CartEditURI.replace(idTable, tableId).replace(cardIdExpr, CartId)
        const resp = await apiHandler.delete(url)
        return resp
    },
    deleteAllCart: async (tableId: string) => {
        const url = apiConstant.CartDeleteURI.replace(idTable, tableId)
        const resp = await apiHandler.delete(url)
        return resp
    },
    /**
     * Edit Cart
     * @param {*} CartId
     */
    editCart: async (tableId: string, CartId: string, body: any) => {
        const url = apiConstant.CartEditURI.replace(idTable, tableId).replace(cardIdExpr, CartId)
        const resp = await apiHandler.update(url, body)
        return resp
    },
    /**
     * Create Cart
     * @param {*} body
     *
     */
    createCart: async (tableId: string, body: any) => {
        const url = apiConstant.CartCreateURI.replace(idTable, tableId)
        const resp = await apiHandler.create(url, body)
        return resp
    }
    /**
     * Check CartName exist or not
     * @param {*} CartName
     *
     */
}

export default CartService
