import React from 'react'
import { Route, Switch } from 'react-router'
import { Language } from '../models/language'
import LoginPage from '../pages/login'
import PersonnalPage from '../pages/personnal/detail'
import ProfilePage from '../pages/profile/detail'
import StorePage from '../pages/store/detail'
import TableRoomPage from '../pages/tableroom/detail'
import BillPage from '../pages/bill/detail'
import ProductPage from '../pages/product/detail'
import CatPage from '../pages/category/detail'
import AreaPage from '../pages/area/deatail'
import PublicPage from '../pages/public/public'
import WareHouseCategoryPage from '../pages/warehouseCategory/detail'
import WareHouseInOutPage from '../pages/warehouseInOut/detail'
import Overview from '../pages/overview'

const Routing = (props: RoutingProps) => (
    <Switch>
        <Route path='/' exact component={() => <Overview />} />
        <Route path='/login' exact component={() => <LoginPage />} />
        <Route path='/profile' exact component={() => <ProfilePage lang={props.lang} />} />
        <Route path='/personnal' exact component={() => <PersonnalPage lang={props.lang} />} />
        <Route path='/product' exact component={() => <ProductPage lang={props.lang} />} />
        <Route path='/category/' exact component={() => <CatPage lang={props.lang} />} />
        <Route path='/store' exact component={() => <StorePage lang={props.lang} />} />
        <Route path='/bill' exact component={() => <BillPage lang={props.lang} />} />
        <Route path='/tableroom' exact component={() => <TableRoomPage lang={props.lang} />} />
        <Route path='/area' exact component={() => <AreaPage lang={props.lang} />} />
        <Route path='/public' exact component={() => <PublicPage lang={props.lang} />} />
        <Route path='/overview' exact component={() => <Overview />} />
        <Route path='/warehouse' exact component={() => <WareHouseCategoryPage lang={props.lang} />} />
        <Route path='/warehouseInout' exact component={() => <WareHouseInOutPage lang={props.lang} />} />
    </Switch>
)

export interface RoutingProps {
    lang: Language
}

export default Routing
