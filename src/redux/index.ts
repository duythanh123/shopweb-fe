import { combineReducers } from 'redux'
import { appReducer } from './reducers/app-reducer'
import { RootState } from './state'
import { userReducer } from './reducers/user-reducer'

export { RootState }

const rootReducer = combineReducers<RootState>({
    app: appReducer,
    user: userReducer
})

export default rootReducer
