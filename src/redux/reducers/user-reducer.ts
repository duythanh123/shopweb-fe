import { RootState } from '..'
import userAction from '../actions/user'

const initialState: RootState.User = {
    loading: false
}

export function userReducer(state = initialState, action: RootState.Action): RootState.User {
    switch (action.type) {
        case userAction.updateUser:
            state.user = action.payload
            state.loading = true
            return Object.assign({}, state)
        default:
            return state
    }
}
