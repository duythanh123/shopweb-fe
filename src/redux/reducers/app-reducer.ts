import { RootState } from '..'
import appAction from '../actions/app'

const initialState: RootState.App = {
    menuToggle: false
}

export function appReducer(state = initialState, action: RootState.Action): RootState.App {
    switch (action.type) {
        case appAction.menuToggle:
            state.menuToggle = !state.menuToggle
            return Object.assign({}, state)
        default:
            return state
    }
}
