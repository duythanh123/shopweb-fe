const appAction = {
    menuToggle: 'TOGGLE',
    login: 'LOGIN',
    logout: 'LOGOUT'
}
export default appAction
