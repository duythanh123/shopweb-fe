import { UserModel } from '../models/user.model'
export interface RootState {
    app: RootState.App
    user: RootState.User
}

export namespace RootState {
    export type App = AppState
    export type Action = AppAction
    export type User = UserAction
}

export interface AppState {
    menuToggle: boolean
}

export interface AppAction {
    type: string
    payload?: any
    id?: string
}

export interface UserAction {
    user?: UserModel
    loading?: boolean
}
