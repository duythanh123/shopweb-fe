import { createStore, applyMiddleware, Store } from 'redux'
import { logger } from '../middleware/logger'
import { RootState } from './state'
import rootReducer from '.'

export function configureStore(preloadState?: RootState): Store<RootState> {
    const middleware = applyMiddleware(logger)
    const store = createStore(rootReducer as any, preloadState, middleware)
    return store
}
