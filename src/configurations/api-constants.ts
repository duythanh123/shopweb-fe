const RootAPI = process.env.ROOT_API || 'https://coffeeshoping.herokuapp.com/shopping'
const RootContentURI = process.env.ROOT_CONTENT || 'https://coffeeshoping.herokuapp.com'

// const RootAPI = process.env.ROOT_API || 'http://localhost:30000/shopping'
// const RootContentURI = process.env.ROOT_CONTENT || 'http://localhost:30000'
const apiConstant = Object.freeze({
    // Login
    LoginURI: `${RootAPI}/auth/login`,
    ChangePasswordURI: `${RootAPI}/auth/changepassword`,

    // Users
    UsersURI: `${RootAPI}/users?page={page}&limit={limit}`,
    ContactURI: `${RootAPI}/users?item=contact`,
    UserDetailURI: `${RootAPI}/users/{id}`,
    UserDetailWithNameURI: `${RootAPI}/users/{name}?item=name&page={page}&limit={limit}`,
    UserCreateURI: `${RootAPI}/users`,
    UserCheckExistURI: `${RootAPI}/users/checkUserName/{userName}`,
    //role
    RoleURI: `${RootAPI}/role`,
    RoleDetailURI: `${RootAPI}/role/{id}`,
    RoleCreateURI: `${RootAPI}/role`,
    //store
    StoreURI: `${RootAPI}/store`,
    StoreDetailURI: `${RootAPI}/store/{id}`,
    //upload
    UploadFileURI: `${RootAPI}/upload`,
    //product
    ProductURI: `${RootAPI}/product?page={page}&limit={limit}`,
    ProductDetailURI: `${RootAPI}/product/{id}`,
    ProductWithNameURI: `${RootAPI}/product/{name}?item=name&page={page}&limit={limit}`,
    ProductCreateURI: `${RootAPI}/product`,
    ProductWithAreaURI: `${RootAPI}/product/{areaid}?item=category&page={page}&limit={limit}`,

    //category
    CategoryURI: `${RootAPI}/category`,
    CaDetailURI: `${RootAPI}/category/{id}`,
    CaCreateURI: `${RootAPI}/category`,

    //area
    AreaURI: `${RootAPI}/area`,
    AreaDetailURI: `${RootAPI}/area/{id}`,
    AreaDeleteURI: `${RootAPI}/area/{id}`,
    AreaCreateURI: `${RootAPI}/area`,
    //table
    TableURI: `${RootAPI}/table`,
    TableDetailURI: `${RootAPI}/table/{id}`,
    TableCreateURI: `${RootAPI}/table`,
    TableSearchURI: `${RootAPI}/table/{id}/area`,
    //cart
    CartURI: `${RootAPI}/table/{tableId}/cart`,
    CartDeleteURI: `${RootAPI}/table/{tableId}/cart`,
    CartEditURI: `${RootAPI}/table/{tableId}/cart/{id}`,
    CartDetailURI: `${RootAPI}/table/{tableId}/cart/{id}`,
    CartCreateURI: `${RootAPI}/table/{tableId}/cart`,
    //CartSearchURI: `${RootAPI}/cart/{id}/area`,
    //Bill
    BillURI: `${RootAPI}/bill?page={page}&limit={limit}`,
    BillByDateURI: `${RootAPI}/bill/{date}?item=date&page={page}&limit={limit}`,
    BillDetailURI: `${RootAPI}/bill/{id}`,
    BillCreateURI: `${RootAPI}/bill`,
    //warehouse
    WareHouseURI: `${RootAPI}/items`,
    WareHouseDetailURI: `${RootAPI}/items/{id}`,
    WareHouseCreateURI: `${RootAPI}/items`,
    // content
    WareHouseInOutURI: `${RootAPI}/warehouses?page={page}&limit={limit}`,
    WareHouseInOutCreateURI: `${RootAPI}/warehouses`,
    WareHouseInOutDeleteURI: `${RootAPI}/warehouses/{id}`,
    WareHouseInOutByDateURI: `${RootAPI}/warehouses/{date}?item=date&page={page}&limit={limit}`
})

export default apiConstant
