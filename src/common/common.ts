/*eslint no-undef: 0*/
import { notification } from 'antd'
import uuid from 'uuid/v4'
import moment from 'moment'

//auth
export const Token = 'frtk'
export const ExpireKey = 'eprtk'
export const UserKey = 'usky'
export const UserInf = 'usif'
export const ExpireTime = 24

export const numberWithCommas = (x: any) => {
    const parts = x.toString().split('.')
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',')
    return parts.join('.')
}

export const distinctArray = (x: any) => {
    return Array.from(new Set(x.map((s: any) => s.value))).map(val => {
        return {
            value: val,
            key: x.find((i: any) => i.value === val).key
        }
    })
}

export const isFilterSelected = (filter: any) => {
    return filter && filter.key !== '-1'
}

export const isFilterNotSelected = (filter: any) => {
    return !filter || filter.key === '-1'
}

export const isObjectEmpty = (obj: any) => {
    return obj === undefined || obj === null || Object.keys(obj).length === 0
}

export const isArrayEmpty = (arr: Array<any>) => {
    return !arr || arr.length === 0 || !Array.isArray(arr)
}

export const isStrEmpty = (str: string) => {
    return str === undefined || str === null || str.length === 0 || str.trim().length === 0
}

export const getCoordinateOfElement = (id: string) => {
    const element = document.getElementById(id)
    if (isObjectEmpty(element)) {
        return null
    }
    const obj = {
        top: element.getBoundingClientRect().top,
        bottom: element.getBoundingClientRect().bottom,
        width: element.getBoundingClientRect().width,
        height: element.getBoundingClientRect().height,
        left: element.getBoundingClientRect().left,
        right: element.getBoundingClientRect().right,
        space: parseInt(window.getComputedStyle(element.parentElement).marginBottom, 10),
        scrollLeft: element.scrollLeft,
        scrollTop: element.scrollTop
    }
    return obj
}

export const countCharInStr = (str: string, char: string) => {
    const found = []
    for (let i = 0; i < str.length; i++) {
        if (char === str[i]) found.push(i)
    }
    return found.length
}

export const isMobileDevice = () => {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
}

export const showNotification = (type: string, message?: string, item?: string, action?: string, cb?: Function) => {
    notification[type]({
        message: type === 'success' ? 'Notification' : 'Error',
        description: type === 'success' ? `${item} has been ${action}  successfully.` : message
    })
    //callback trigger
    if (cb) cb()
}

export const enablePending = () => {
    const element = document.getElementById('app-pending')
    if (element) element.style.display = 'block'
}

export const disablePending = () => {
    const element = document.getElementById('app-pending')
    if (element) element.style.display = 'none'
}

export const getUrlParameter = (name: string) => {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]') //eslint-disable-line
    const regex = new RegExp('[\\?&]' + name + '=([^&#]*)')
    const results = regex.exec(location.search)
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '))
}

export const openWindow = (path: string) => {
    const url = `${location.origin}/${path}&popup=true`
    window.open(
        url,
        `newwindow${uuid()}`,
        'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=1024,height=750'
    )
}

/**
 * Convert funtion just be used for ant table. It converts id->value, name->text
 * @param data
 */
export const convertDataForAnt = (data: Array<any>) => {
    data?.forEach(elem => {
        elem.text = elem.name
        elem.value = elem.id
    })
}

export const capitalize = s => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

/**
 * Convert funtion just be used for ant table. It converts object to array(text,value)
 * @param data
 */

export const objectToArray = (data: any) => {
    const filterArr = []
    for (const key in data) {
        filterArr.push({
            text: data[key],
            value: key
        })
    }
    return filterArr
}

export const beforeUpload = file => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png'
    if (!isJpgOrPng) {
        showNotification('error', 'You can only upload JPG/PNG file!')
    }
    const isLt2M = file.size / 1024 / 1024 < 2
    if (!isLt2M) {
        showNotification('error', 'Image must smaller than 2MB!')
    }
    return isJpgOrPng && isLt2M
}

export const getChangeItems = (oldObj: object, newObj: object) => {
    const result = {}
    for (const item in newObj) {
        if (JSON.stringify(newObj[item]) != JSON.stringify(oldObj[item])) {
            result[item] = newObj[item]
        }
    }
    return result
}

export const formatMoment = (date: any) => {
    return date
        ? moment(date)
              .startOf('day')
              .valueOf()
        : null
}

export const includeStr = (str1: string, str2: string) => {
    return str1
        .toString()
        .toLowerCase()
        .includes(str2.toLowerCase())
}
export const formatter = new Intl.NumberFormat('de-DE', {})
