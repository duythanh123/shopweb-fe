import { isStrEmpty } from './common'

/**
 * Format the date to 'DD/MM/YYYY'
 * @param dateTime: the time (such as new Date().getTime())
 */
export const formatDate = (dateTime: any) => {
    if (dateTime === undefined || dateTime === null) return ''
    try {
        const date = new Date(dateTime)
        return (
            (date.getDate() < 10 ? '0' : '') +
            date.getDate() +
            '/' +
            (date.getMonth() < 9 ? '0' : '') +
            (date.getMonth() + 1) +
            '/' +
            date.getFullYear()
        )
    } catch {
        return ''
    }
}

export const compareDate = (dateString1: string, dateString2: string) => {
    if (dateString2 === undefined || dateString2 === null) return 1
    if (dateString1 === undefined || dateString1 === null) return -1
    const date1 = new Date(dateString1)
    const date2 = new Date(dateString2)
    return date1 >= date2 ? 1 : -1
}

/**
 * Get toISOString from the date with format 'DD/MM/YYYY'
 * @param dateTime: the date format 'DD/MM/YYYY'
 */
export const getTimeStamp = (dateString: string) => {
    if (isStrEmpty(dateString)) return null

    const dateParts = dateString.split('/')
    // month is 0-based, that's why we need dataParts[1] - 1
    const dateObject = new Date(+dateParts[2], parseInt(dateParts[1]) - 1, +dateParts[0])
    return dateObject.toISOString()
}

export const isSameDate = (dateTime1: any, dateTime2: any) => {
    if (dateTime1 === undefined || dateTime1 === null || dateTime2 === undefined || dateTime2 === null) return false

    try {
        const date1 = new Date(dateTime1).setHours(0, 0, 0, 0)
        const date2 = new Date(dateTime2).setHours(0, 0, 0, 0)
        return date1 === date2
    } catch {
        return false
    }
}

export const isPastDate = (dateTime: any) => {
    if (dateTime === undefined || dateTime === null) return false

    try {
        const date1 = new Date(dateTime).setHours(0, 0, 0, 0)
        const date2 = new Date().setHours(0, 0, 0, 0)
        return date1 < date2
    } catch {
        return false
    }
}

/**
 * Format the day to 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'
 * @param dateTime: the time (such as new Date().getTime())
 */
export const formatDay = (dateTime: any) => {
    if (dateTime === undefined || dateTime === null) return ''
    const date = new Date(dateTime)
    const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
    return days[date.getDay()]
}

/**
 * Format the time to HH:mm
 * @param dateTime: the time (such as new Date().getTime())
 */
export const formatTime = (dateTime: any) => {
    if (dateTime === undefined || dateTime === null) return ''
    const date = new Date(dateTime)
    return (
        (date.getHours() < 10 ? '0' : '') +
        date.getHours() +
        ':' +
        (date.getMinutes() < 10 ? '0' : '') +
        date.getMinutes()
    )
}

/**
 * Check the dateTime is today or not
 * TODO: need month/year
 * @param dateTime: the time (such as new Date().getTime())
 */
export const isInDay = (dateTime: any) => {
    return isSameDate(dateTime, new Date())
}

/**
 * Check the dateTime is in this week or not
 * TODO: need month/year
 * @param dateTime: the time (such as new Date().getTime())
 */
export const isInWeek = (dateTime: any) => {
    if (dateTime === undefined || dateTime === null) return false

    try {
        const date1 = addDays(new Date(dateTime), 7).setHours(0, 0, 0, 0)
        const date2 = new Date().setHours(0, 0, 0, 0)
        return date1 > date2
    } catch {
        return false
    }
}

export const addDays = (date: any, days: number) => {
    const copy = new Date(Number(date))
    copy.setDate(date.getDate() + days)
    return copy
}

/**
 * Show the time as type 'two days ago'
 * @param dateTime: the time (such as new Date().getTime())
 */
export const showTimeAgo = (dateTime: any) => {
    if (dateTime === undefined || dateTime === null) return ''

    const seconds = Math.floor((new Date().valueOf() - dateTime) / 1000)
    if (seconds < 30) return 'just now'

    let intervalType
    let interval = Math.floor(seconds / 31536000)

    if (interval >= 1) {
        intervalType = 'year'
    } else {
        interval = Math.floor(seconds / 2592000)
        if (interval >= 1) {
            intervalType = 'month'
        } else {
            interval = Math.floor(seconds / 86400)
            if (interval >= 1) {
                intervalType = 'day'
            } else {
                interval = Math.floor(seconds / 3600)
                if (interval >= 1) {
                    intervalType = 'hour'
                } else {
                    interval = Math.floor(seconds / 60)
                    if (interval >= 1) {
                        intervalType = 'minute'
                    } else {
                        interval = seconds
                        intervalType = 'second'
                    }
                }
            }
        }
    }

    if (interval > 1 || interval === 0) {
        intervalType += 's'
    }

    return interval + ' ' + intervalType
}
