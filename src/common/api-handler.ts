import axios from 'axios'
import { getCookie, removeCookie } from './cookie'
import { Token, UserKey, ExpireKey, UserInf, enablePending, disablePending } from './common'

axios.defaults.headers.common['Authorization'] = `Bearer ${getCookie(Token)}`
axios.defaults.timeout = 10000 // 10 seconds
// Add a request interceptor
axios.interceptors.request.use(
    function(config) {
        // Do something before request is sent
        if (config.method !== 'get') {
            enablePending()
        }
        return config
    },
    function(error) {
        // Do something with request error
        disablePending()

        return Promise.reject(error)
    }
)

// Add a response interceptor
axios.interceptors.response.use(
    function(response) {
        // Do something with response data
        disablePending()

        return response
    },
    function(error) {
        disablePending()
        const status = error?.response?.status
        if (status === 401 && !error.response.config.url.includes('/api/auth/login')) {
            removeCookie(UserKey)
            removeCookie(Token)
            removeCookie(ExpireKey)
            removeCookie(UserInf)
            window.location.href = `/login?url=${location.pathname}`
        }
        if (status === 403 || status === 404 || status === 409 || status === 500) {
            const { message } = error.response.data.error
            const customError = {
                message: `${message.summary} ${message.message}`
            }
            return Promise.reject(customError)
        }
        if (status === 400) {
            const customError = {
                message: JSON.stringify(error.response.data)
            }
            return Promise.reject(customError)
        }
        return Promise.reject(error)
    }
)

const apiHandler = {
    /**
     * Make Http Get Request
     * This method is used to fetch data
     * @param url
     * @param body
     */
    get: async (url: string, body?: any) => {
        return await axios.get(url, { params: body })
    },
    /**
     * Make Http Post Request
     * This method is used to create new item
     * @param url
     * @param body
     */
    create: async (url: string, body?: any) => {
        return axios.post(url, body)
    },
    /**
     * Make Http Put Request
     * This method is used to update item
     * @param url
     * @param body
     */
    update: async (url: string, body?: any) => {
        return axios.patch(url, body)
    },
    /**
     * Make Http Delete Request
     * This method is used to delete item
     * @param url
     */
    delete: async (url: string, body?: any) => {
        return await axios.delete(url, { data: body })
    },
    /**
     * Make Http Post Request (without Authentication params)
     * This method is used to login
     * @param url
     */
    login: async (url: string, body: any) => {
        delete axios.defaults.headers.common['Authorization']
        return await axios.post(url, body)
    },
    /**
     * Make Multiple Http Get Requests
     * @param urls
     */
    all: async (urls: Array<string>) => {
        const requests = []
        urls.forEach(url => {
            requests.push(axios.get(url))
        })
        return await axios.all(requests)
    },
    /**
     * Make Multiple Http Get Requests
     * @param urls
     */
    allDelete: async (urls: Array<string>) => {
        const requests = []
        urls.forEach(url => {
            requests.push(axios.delete(url))
        })
        return await axios.all(requests)
    }
}
export default apiHandler
