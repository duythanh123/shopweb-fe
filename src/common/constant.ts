export const UserRole = Object.freeze({
    Admin: 1,
    User: 2
})
export const pagination = Object.freeze({
    current: 1,
    pageSize: 8,
    total: 0
})
export const menuItem = Object.freeze({
    // using
    updateAccount: 'UPDATEACOUNT',
    changePass: 'CHANGEPASSWORD'
})

export const apiImage = '../../src/static/images'
