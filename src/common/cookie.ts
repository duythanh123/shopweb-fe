import cookie from 'js-cookie'
import { UserKey, Token, ExpireKey, UserInf, isObjectEmpty } from './common'

/**
 * @param   {[type]} key
 * @returns {[type]}
 */
const getCookieFromBrowser = (key: string) => {
    if (cookie.get(key)) {
        try {
            return cookie.getJSON(key)
        } catch (error) {
            console.log('getCookieFromBrowser', error)
        }
    } else {
        return null
    }
}
/**
 * @param   {[type]} key
 * @param   {[type]} req
 * @returns {[type]}
 */
const getCookieFromServer = (key: string, req?: any) => {
    if (!req || !req.headers.cookie) {
        return undefined
    }
    const rawCookie = req.headers.cookie.split(';').find((c: string) => c.trim().startsWith(`${key}=`))
    if (!rawCookie) {
        return undefined
    }
    return rawCookie.split('=')[1]
}

export const setCookie = (key: string, value: any, res?: any) => {
    if (isObjectEmpty(window)) {
        res.cookie(key, value)
    } else {
        cookie.set(key, value, { expires: 1, path: '/' })
    }
}
/**
 * @param   {[type]} key
 * @param   {[type]} res
 * @returns {[type]}
 */
export const removeCookie = (key: string, res?: any) => {
    if (isObjectEmpty(window)) {
        res.clearCookie(key)
    } else {
        cookie.remove(key, { expires: 1 })
    }
}
/**
 * @param   {[type]} key
 * @param   {[type]} req
 * @returns {[type]}
 */
export const getCookie = (key: string, req?: any) => {
    const cookie = isObjectEmpty(window) ? getCookieFromServer(key, req) : getCookieFromBrowser(key)
    const pathname = window.location.pathname
    if (!cookie && pathname !== '/login') {
        removeCookie(UserKey)
        removeCookie(Token)
        removeCookie(ExpireKey)
        removeCookie(UserInf)
        location.href = `/login?url=${pathname}`
    }
    return cookie
}
