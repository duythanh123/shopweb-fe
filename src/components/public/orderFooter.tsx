import { Col, Row, Button, Input } from 'antd'
import React, { useState, useEffect } from 'react'
import { Language } from '../../models/language'
import { cartModel } from '../../models/cart.model'
import { formatter } from '../../common/common'

interface OrderRightProps {
    lang?: Language
    cartList?: cartModel[]
    createBill?: Function
    close?: Function
    destroy?: Function
}
interface OrderState {
    total: number
    count: number
    returnM: number
    pay: number
}
const OrderRigth = (props: OrderRightProps) => {
    const initalState: OrderState = {
        total: 0,
        count: 0,
        returnM: 0,
        pay: 0
    }
    const [state, setState] = useState(initalState)
    const fetchData = async () => {
        let { total, count, returnM, pay } = state
        total = 0
        count = 0
        props.cartList.forEach(item => {
            total += item.amount * item.product.price
            count += item.amount
        })
        if (pay !== 0) returnM = pay - total
        else returnM = 0
        setState(s => ({ ...s, total, count, returnM }))
    }
    useEffect(() => {
        if (props.cartList) {
            fetchData()
        }
    }, [props])
    const ChangeAmount = e => {
        let { pay, returnM, total } = state
        if (e.target.value === '') pay = 0
        else pay = parseInt(e.target.value)
        returnM = pay - total
        setState(s => ({ ...s, pay, returnM }))
    }

    return (
        <div>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                <Col className='search-row' xs={24} sm={24} md={24} lg={24}>
                    <div className='bill-footer'>
                        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                            <Col className='search-row' xs={24} sm={12} md={12} lg={12}>
                                <div className='order-footer'>
                                    <span>Tổng tiền: </span>
                                    <Input
                                        value={formatter.format(state.total)}
                                        disabled
                                        style={{ textAlign: 'right' }}
                                    />
                                </div>
                            </Col>
                            <Col className='search-row' xs={24} sm={12} md={12} lg={12}>
                                <div className='order-footer'>
                                    <span>Số lượng: </span>
                                    <Input value={state.count} disabled style={{ textAlign: 'right' }} />
                                </div>
                            </Col>
                        </Row>
                        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                            <Col className='search-row' xs={24} sm={12} md={12} lg={12}>
                                <div className='order-footer'>
                                    <span>Khách đưa: </span>
                                    <Input
                                        style={{ textAlign: 'right' }}
                                        onKeyPress={e => {
                                            if (!/^[0-9]/.test(e.key)) event.preventDefault()
                                        }}
                                        defaultValue='0'
                                        value={state.pay}
                                        onChange={ChangeAmount}
                                    />
                                </div>
                            </Col>
                            <Col className='search-row' xs={24} sm={12} md={12} lg={12}>
                                <div className='order-footer'>
                                    <span>Thối lại: </span>
                                    <Input
                                        value={formatter.format(state.returnM)}
                                        disabled
                                        style={{ textAlign: 'right' }}
                                    />
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Col>
            </Row>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                <Col className='search-row' xs={12} sm={8} md={8} lg={8}>
                    <Button type='primary' onClick={() => props.close()} style={{ width: 100 }}>
                        Lưu
                    </Button>
                </Col>
                <Col className='search-row' xs={12} sm={8} md={8} lg={8}>
                    {state.pay >= state.total && state.total > 0 ? (
                        <Button type='primary' onClick={() => props.createBill(state.total)} style={{ width: 100 }}>
                            Thanh toán
                        </Button>
                    ) : (
                        <Button type='primary' disabled style={{ width: 100 }}>
                            Thanh toán
                        </Button>
                    )}
                </Col>
                <Col className='search-row' xs={12} sm={8} md={8} lg={8}>
                    <Button type='primary' onClick={() => props.destroy()} style={{ width: 100 }}>
                        Hủy bàn
                    </Button>
                </Col>
            </Row>
        </div>
    )
}
export default OrderRigth
