import React, { Fragment } from 'react'
import { Form, Input, Select, DatePicker, InputNumber, AutoComplete, Popover } from 'antd'
import Icon from '@ant-design/icons'
import CKEditor from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import { isArrayEmpty } from '../../common/common'
import 'antd/dist/antd.css'
import '../../styles/form.scss'

const { Option } = Select
const { TextArea } = Input

export interface FormItemProps {
    item: string
    tag: string
    rules?: Array<any>
    placeholder?: string
    label?: string
    initialValue?: any
    icon?: string
    type?: string
    sources?: Array<any>
    onChange?: any
    onBlur?: any
    onKeep?: any
    onKeyPress?: any
    disabled?: boolean
    name?: string
    valuePropName?: string
    maxLength?: number
    dataSource?: Array<any>
    minNumber?: number
    maxNumber?: number
    disabledDate?: any
    allowEnter?: boolean
    popoverContent?: any
    popoverVisible?: boolean
    className?: string
}
export const validateMessages = {
    required: 'This field is required!',
    types: {
        email: 'Not a validate email!',
        number: 'Not a validate number!'
    },
    number: {
        range: 'Must be between ${min} and ${max}'
    }
}

const renderPlaceholder = (tag: string, item: string) => {
    if (tag === 'input') {
        return `Input ${item}`
    } else if (tag === 'select') {
        return `Select ${item}`
    }
    return null
}

const renderElement = (i: FormItemProps) => {
    if (i.tag === 'input') {
        return (
            <Input
                prefix={i.icon && <Icon style={{ color: 'rgba(0,0,0,.25)' }} />}
                type={i.type}
                placeholder={`Input ${i.label || i.item}`}
                maxLength={i.maxLength}
                onChange={i.onChange}
                disabled={i.disabled}
                defaultValue={i.initialValue}
                onKeyPress={e => {
                    if (!i.allowEnter && e.key === 'Enter') event.preventDefault()
                }}
            />
        )
    }
    if (i.tag === 'inputNumber') {
        return (
            <Input
                prefix={i.icon && <Icon style={{ color: 'rgba(0,0,0,.25)' }} />}
                type={i.type}
                placeholder={`Input ${i.label || i.item}`}
                maxLength={i.maxLength}
                onChange={i.onChange}
                disabled={i.disabled}
                defaultValue={i.initialValue}
                onKeyPress={e => {
                    if (!/^[0-9]/.test(e.key)) event.preventDefault()
                }}
            />
        )
    }
    if (i.tag === 'inputNumberPopover') {
        return (
            <Popover
                placement='topLeft'
                style={{ width: 500 }}
                content={i.popoverContent}
                title=''
                trigger='focus'
                visible={i.popoverVisible}>
                <Input
                    prefix={i.icon && <Icon style={{ color: 'rgba(0,0,0,.25)' }} />}
                    type={i.type}
                    placeholder={`Input ${i.label || i.item}`}
                    maxLength={i.maxLength}
                    onChange={i.onChange}
                    onBlur={i.onBlur}
                    disabled={i.disabled}
                    defaultValue={i.initialValue}
                    onKeyPress={e => {
                        if (!/^[0-9]/.test(e.key)) event.preventDefault()
                    }}
                />
            </Popover>
        )
    }
    if (i.tag === 'select') {
        return (
            <Select
                defaultValue={i.initialValue}
                placeholder={`Select ${i.label}`}
                labelInValue
                onChange={i.onChange}
                disabled={i.disabled}>
                {!isArrayEmpty(i.sources) &&
                    i.sources.map((v: { id: string | number; name: React.ReactNode }) => (
                        <Option key={v.id} value={v.id}>
                            {v.name}
                        </Option>
                    ))}
            </Select>
        )
    }
    if (i.tag === 'area') {
        return <TextArea rows={4} placeholder={i.placeholder} onChange={i.onChange} value={i.initialValue} />
    }

    if (i.tag === 'datepicker') {
        return (
            <DatePicker
                disabledDate={i.disabledDate}
                onChange={i.onChange}
                format='DD/MM/YYYY'
                disabled={i.disabled}
                defaultValue={i.initialValue}
                value={i.initialValue}
            />
        )
    }
    if (i.tag === 'inputPercent') {
        return (
            <InputNumber
                min={i.minNumber}
                max={i.maxNumber}
                formatter={value => `${value}%`}
                parser={value => value.replace('%', '')}
                onChange={i.onChange}
                disabled={i.disabled}
            />
        )
    }
    if (i.tag === 'ckeditor') {
        return (
            <CKEditor
                editor={ClassicEditor}
                onChange={i.onChange}
                data={i.initialValue}
                disabled={i.disabled}
                name={i.item}
            />
        )
    }
    if (i.tag === 'autoComplete') {
        return (
            <AutoComplete
                defaultValue={i.initialValue}
                dataSource={i.dataSource}
                onChange={i.onChange}
                disabled={i.disabled}
            />
        )
    }
    return null
}

export const renderFormFields = (items: Array<FormItemProps>) => {
    if (!isArrayEmpty(items)) {
        return (
            <Fragment>
                {items.map((i: FormItemProps, index: number) => {
                    return (
                        <Form.Item
                            className={i.className}
                            key={index}
                            label={i.label}
                            rules={i.rules}
                            name={i.tag !== 'datepicker' ? i.item : null}>
                            {renderElement(i)}
                        </Form.Item>
                    )
                })}
            </Fragment>
        )
    }
}

export const renderFormField = (item: FormItemProps) => {
    return (
        <Form.Item label={item.label} rules={item.rules} name={item.tag !== 'datepicker' ? item.item : null}>
            {renderElement(item)}
        </Form.Item>
    )
}

export const disableSubmit = () => {
    const element = document.querySelector('button[type="submit"]')
    if (element) element.setAttribute('disable', 'true')
}

export const enableSubmit = () => {
    const element = document.querySelector('button[type="submit"]')
    if (element) element.removeAttribute('disable')
}

export const handleValidation = (e: any, form: any, callback: any) => {
    e.preventDefault()
    form.validateFieldsAndScroll((err: any, values: any) => {
        if (!err) {
            disableSubmit()
            callback(values)
        }
    })
}
