const constant = Object.freeze({
    userRole: {
        1: 'Admin',
        2: 'User'
    },
    memberRole: {
        1: 'Editor',
        2: 'Guest'
    },
    role: {
        admin: 1,
        author: 2,
        assignee: 3,
        editor: 4,
        guest: 5,
        notMember: 6
    },
    checkIssueAuthen: {
        editWithEditor: [2, 3, 4, 6],
        editWithAuthor: [3, 4],
        editWithAssignee: [4, 6]
    },
    itemMenu: {
        1: 'Quản lý tài khoản',
        2: 'Quản lý nhân viên',
        3: 'Quản lý doanh thu',
        4: 'Quản lý cửa hàng',
        5: 'Khu vực-Phòng bàn',
        6: 'Quản lý kho',
        7: 'Quản lý danh mục',
        8: 'Quản lý danh mục',
        9: 'Tổng Quan'
    }
})

export default constant
