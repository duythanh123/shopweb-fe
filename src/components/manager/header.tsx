import React from 'react'
import 'antd/dist/antd.css'
import { Layout } from 'antd'
import constant from '../common/constant'

interface HearderProps {
    itemMenu?: number
}
const HeaderDom = (props: HearderProps) => {
    const { Header } = Layout

    return (
        <Header className='site-layout-header' style={{ padding: 15, background: 'inherit' }}>
            <h3>{constant.itemMenu[props.itemMenu]}</h3>
        </Header>
    )
}

export default HeaderDom
