import React, { useState, useEffect, Fragment } from 'react'
import 'antd/dist/antd.css'
import { Layout, Menu } from 'antd'
import {
    UserOutlined,
    TeamOutlined,
    AppstoreOutlined,
    ShopOutlined,
    DropboxOutlined,
    BarChartOutlined
} from '@ant-design/icons'
import { SettingOutlined } from '@ant-design/icons'
import { useHistory, useLocation } from 'react-router-dom'
import { UserModel } from '../../models/user.model'
import userService from '../../services/user-service'
import { getCookie } from '../../common/cookie'
import { UserInf, showNotification } from '../../common/common'
import { Language } from '../../models/language'

const { Sider } = Layout
const { SubMenu } = Menu
interface SiderProp {
    lang?: Language
}
type LocationState = {
    menuKey: string[]
    parentMenuKey: string[]
    userDetail: UserModel
}

const SiderDom = (props: SiderProp) => {
    const history = useHistory()
    const location = useLocation()
    const { lang } = props

    const initialMenuState = {
        menuKey: ['1'],
        parentMenuKey: [''],
        currentMenuKey: [''],
        userDetail: null
    }

    const locationState: LocationState = (location && (location.state as LocationState)) || initialMenuState
    const [state, setState] = useState({
        currentMenuKey: locationState.menuKey,
        parentMenuKey: locationState.parentMenuKey,
        userDetail: null
    })
    const navigatePage = (menuKey: string[], parentMenuKey: string[], link: string): void => {
        history.push(link, { menuKey, parentMenuKey })
    }
    const fetchData = async () => {
        try {
            const userDetail2 = await userService.getUserDetail(getCookie(UserInf).id)
            setState(s => ({ ...s, userDetail: userDetail2 }))
        } catch (error) {
            showNotification(lang.notification.error, lang.item.data, lang.action.load)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])
    return (
        <Sider
            breakpoint='lg'
            collapsedWidth='0'
            onBreakpoint={broken => {
                console.log(broken)
            }}
            onCollapse={(collapsed, type) => {
                console.log(collapsed, type)
            }}>
            {/* <div className='logo' /> */}
            <div style={{ textAlign: 'center' }}>
                <h4>Milan coffee</h4>
            </div>
            <Menu
                theme='dark'
                defaultSelectedKeys={state.currentMenuKey}
                mode='inline'
                defaultOpenKeys={state.parentMenuKey}>
                <Menu.Item
                    key='1'
                    icon={<BarChartOutlined />}
                    onClick={(): void => navigatePage(['1'], [], '/overview')}>
                    Tổng quan
                </Menu.Item>
                {state.userDetail?.role < 3 ? (
                    <Menu.Item
                        key='2'
                        icon={<TeamOutlined />}
                        onClick={(): void => navigatePage(['2'], [], '/personnal')}>
                        Quản lý nhân viên
                    </Menu.Item>
                ) : null}
                {state.userDetail?.role < 3 ? (
                    <Menu.Item key='3' icon={<ShopOutlined />} onClick={(): void => navigatePage(['3'], [], '/store')}>
                        Quản lý cửa hàng
                    </Menu.Item>
                ) : null}
                {state.userDetail?.role < 3 ? (
                    <SubMenu key='sub1' icon={<SettingOutlined />} title={'Khu vực - Phòng bàn'}>
                        <Menu.Item key='41' onClick={(): void => navigatePage(['41'], ['sub1'], '/area')}>
                            Khu vực
                        </Menu.Item>
                        <Menu.Item key='42' onClick={(): void => navigatePage(['42'], ['sub1'], '/tableroom')}>
                            Phòng bàn
                        </Menu.Item>
                    </SubMenu>
                ) : null}
                {state.userDetail?.role < 3 ? (
                    <SubMenu key='sub2' icon={<AppstoreOutlined />} title='Danh mục'>
                        <Menu.Item key='8' onClick={(): void => navigatePage(['8'], ['sub2'], '/product')}>
                            Đồ uống món ăn
                        </Menu.Item>
                        <Menu.Item key='7' onClick={(): void => navigatePage(['7'], ['sub2'], '/category')}>
                            Nhóm món
                        </Menu.Item>
                    </SubMenu>
                ) : null}
                {state.userDetail?.role < 4 ? (
                    <SubMenu key='sub3' icon={<DropboxOutlined />} title='Quản lý kho'>
                        <Menu.Item key='5' onClick={(): void => navigatePage(['5'], ['sub3'], '/warehouse')}>
                            Danh mục kho
                        </Menu.Item>
                        <Menu.Item key='9' onClick={(): void => navigatePage(['9'], ['sub3'], '/warehouseInout')}>
                            Nhập - xuất kho
                        </Menu.Item>
                    </SubMenu>
                ) : null}
                {state.userDetail?.role < 4 ? (
                    <Menu.Item
                        key='6'
                        icon={<BarChartOutlined />}
                        onClick={(): void => navigatePage(['6'], [], '/bill')}>
                        Quản lý doanh thu
                    </Menu.Item>
                ) : null}
                <Menu.Item key='10' icon={<UserOutlined />} onClick={(): void => navigatePage(['10'], [], '/profile')}>
                    Quản lý tài khoản
                </Menu.Item>
            </Menu>
        </Sider>
    )
}

export default SiderDom
