import React, { useState, useEffect } from 'react'
import { Token, ExpireKey, UserKey, UserInf } from '../../common/common'
import { removeCookie, getCookie } from '../../common/cookie'
import { Language } from '../../models/language'
import { apiImage } from '../../common/constant'
import { Menu, Dropdown } from 'antd'
import { UserOutlined, CoffeeOutlined, LogoutOutlined } from '@ant-design/icons'
import { useSelector } from 'react-redux'
import { RootState } from '../../redux'

const AppHeader = (props: { lang: Language }) => {
    const [user, setUser] = useState(getCookie(UserInf))
    const userReducer = useSelector<RootState, RootState.User>(state => state.user)

    useEffect(() => {
        if (userReducer.loading) {
            setUser(userReducer.user)
        }
    }, [userReducer])
    const onLogoutClick = () => {
        const pathname = window.location.pathname
        removeCookie(UserKey)
        removeCookie(Token)
        removeCookie(ExpireKey)
        removeCookie(UserInf)
        location.href = `/login?url=${pathname}`
    }
    const onClickPublic = () => {
        location.href = `/public`
    }

    const menu = (
        <Menu>
            <Menu.Item key='1' icon={<CoffeeOutlined />} onClickCapture={onClickPublic}>
                Bán hàng
            </Menu.Item>

            <Menu.Item key='3' icon={<LogoutOutlined />} onClickCapture={onLogoutClick}>
                Đăng xuất
            </Menu.Item>
        </Menu>
    )
    return (
        <nav className='navbar'>
            <div id='top-menu'>
                <div className='left'>
                    <img
                        src={'http://res.cloudinary.com/ds5zmvcyj/image/upload/v1593349050/x85vvrfnjqjmuu5t7bp3.png'}
                        alt='logo'></img>
                </div>
                <div className='right'>
                    <Dropdown.Button overlay={menu} icon={<UserOutlined />}>
                        {user.name}
                    </Dropdown.Button>
                </div>
            </div>
        </nav>
    )
}

export default AppHeader
