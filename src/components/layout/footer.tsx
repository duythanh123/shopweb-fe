import React from 'react'
import { Language } from '../../models/language'

export interface FooterProps {
    lang: Language
}

const AppFooter = (props: FooterProps) => {
    return (
        <footer className='sticky-footer bg-white'>
            <div className='container my-auto'>
                <div className='copyright text-center my-auto'>
                    <span>{props.lang.footer.copy_right}</span>
                </div>
            </div>
        </footer>
    )
}

export default AppFooter
