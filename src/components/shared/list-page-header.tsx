import React, { Fragment } from 'react'
import Title from './title'

const ListPageHeader = (props: ListPageHeaderProps) => {
    return (
        <Fragment>
            <Title title={props.titlePage} />
            <p className='sub-text'>{props.description}</p>
        </Fragment>
    )
}

export interface ListPageHeaderProps {
    titlePage: string
    description?: string
}

export default ListPageHeader
