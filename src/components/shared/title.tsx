import React from 'react'

export interface TitleProps {
    title: string
}
const Title = (props: TitleProps) => {
    return <h1 className='h3 mb-0 text-gray-800'>{props.title}</h1>
}

export default Title
