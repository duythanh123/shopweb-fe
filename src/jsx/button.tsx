import React from 'react'
import { Button } from 'antd'
import '../styles/button.scss'

export interface ButtonProps {
    className?: string
    icon?: string
    text?: string
    htmlType?: 'button' | 'reset' | 'submit'
    onClick?: any
    disabled?: boolean
    loading?: boolean
    type?: 'default' | 'primary' | 'ghost' | 'dashed' | 'link'
}

const ButtonCommon = (props: ButtonProps) => {
    return (
        <Button
            className={`button ${props.className}`}
            htmlType={props.htmlType || null}
            type={props.type || null}
            onClick={props.onClick}>
            {props.text}
        </Button>
    )
}

export const ButtonMain = (props: ButtonProps) => <ButtonCommon {...props} className='blue' />

export const ButtonSubmit = (props: ButtonProps) => <ButtonCommon {...props} className='blue' htmlType='submit' />

export const ButtonSave = (props: ButtonProps) => (
    <ButtonCommon {...props} text='Save' className='blue' htmlType='submit' />
)

export const ButtonUpdate = (props: ButtonProps) => (
    <ButtonCommon {...props} text='Update' className='green' htmlType='submit' />
)

export const ButtonCancel = (props: ButtonProps) => (
    <ButtonCommon {...props} text='Cancel' className='gray' htmlType='button' />
)

export const ButtonIcon = (props: ButtonProps) => <ButtonCommon {...props} className='blue' />

export const ButtonAttach = (props: ButtonProps) => <ButtonCommon {...props} className='blue' />

export const ButtonPrevious = (props: ButtonProps) => <ButtonCommon {...props} className='blue' />

export const ButtonDelete = (props: ButtonProps) => <ButtonCommon {...props} className='gray' />

export const ButtonEdit = (props: ButtonProps) => <ButtonCommon {...props} className='green' />

export const ButtonBack = (props: ButtonProps) => <ButtonCommon {...props} className='white' />

export const ButtonDisabled = (props: ButtonProps) => <ButtonCommon {...props} className='gray' />

export const ButtonLogin = (props: ButtonProps) => (
    <ButtonCommon {...props} className='login-form-button' type='primary' htmlType='submit' text='Login' />
)
export const ButtonLoginAdmin = (props: ButtonProps) => (
    <ButtonCommon {...props} className='login-form-button' type='primary' htmlType='submit' text='Admin' />
)
export const ButtonLoginSale = (props: ButtonProps) => (
    <ButtonCommon {...props} className='login-form-button' type='primary' htmlType='submit' text='Sale' />
)
export const ButtonChange = (props: ButtonProps) => (
    <ButtonCommon {...props} className='blue' type='primary' htmlType='submit' text='Change' />
)

export const ButtonSearch = (props: ButtonProps) => (
    <ButtonCommon {...props} className='search-columns' type='primary' />
)

export const ButtonReset = (props: ButtonProps) => <ButtonCommon {...props} className='btn-reset' />
