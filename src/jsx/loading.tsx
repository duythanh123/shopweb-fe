import React from 'react'
import '../styles/loading.scss'

const Loading = () => (
    <div className='loading'>
        <div className='lds-roller'>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
)

export const LoadingPage = () => {
    return <Loading />
}

export const LoadingDetail = () => (
    <div className='loading-detail'>
        <Loading />
    </div>
)

export const LoadingModal = () => (
    <div className='loading-modal'>
        <Loading />
    </div>
)
