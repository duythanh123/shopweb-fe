import { Middleware } from 'redux'

export const logger: Middleware = () => next => action => {
    return next(action)
}
